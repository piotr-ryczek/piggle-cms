import {
	COUNT_BRAND_FAILED,
	COUNT_BRAND_SUCCED,
	COUNT_BRAND_PROCESSING,
	FETCH_BRAND_FAILED,
	FETCH_BRAND_SUCCED,
	FETCH_BRAND_PROCESSING,
	GET_BRAND_FAILED,
	GET_BRAND_SUCCED,
	GET_BRAND_PROCESSING,
	SAVE_BRAND_FAILED,
	SAVE_BRAND_SUCCED,
	SAVE_BRAND_PROCESSING,
	DELETE_BRAND_FAILED,
	DELETE_BRAND_SUCCED,
	DELETE_BRAND_PROCESSING,
	HIDE_ALERT,
	ADD_NEW,
	FETCH_STORES_FAILED,
	FETCH_STORES_SUCCED,
	FETCH_STORES_PROCESSING,
	UPDATE_SEARCH_TEXT
} from '../constants'

const initialState = {
	fetching: false,
	fetched: false,
	counting: false,
	counted: false,
	saving: false,
	saved: false,
	deleting: false,
	deleted: false,
	errorMessage: null,
	list: [],
	total: 0,
	record: null,
	stores: [],
	page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
	order: 'asc'
}

const brand = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_BRAND_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_BRAND_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_BRAND_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				list: action.payload.list,
				page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
			}
		case GET_BRAND_FAILED:
			return {
				...state,
				fetching: false,
				fetched: true,
				errorMessage: action.payload.error
			}
		case GET_BRAND_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case GET_BRAND_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				record: action.payload.record
			}
		case COUNT_BRAND_FAILED:
			return {
				...state,
				counting: false,
				counted: false,
				errorMessage: action.payload.error
			}
		case COUNT_BRAND_PROCESSING:
			return {
				...state,
				counting: true,
				counted: false
			}
		case COUNT_BRAND_SUCCED:
			return {
				...state,
				counting: false,
				counted: true,
				total: action.payload.total
			}
		case SAVE_BRAND_FAILED:
			return {
				...state,
				saving: false,
				saved: true,
				errorMessage: action.payload.error
			}
		case SAVE_BRAND_PROCESSING:
			return {
				...state,
				saving: true,
				saved: false
			}
		case SAVE_BRAND_SUCCED:
			return {
				...state,
				saving: false,
				saved: true
			}
		case DELETE_BRAND_FAILED:
			return {
				...state,
				deleting: false,
				deleted: false,
				errorMessage: action.payload.error
			}
		case DELETE_BRAND_PROCESSING:
			return {
				...state,
				deleting: true,
				deleted: false
			}
		case DELETE_BRAND_SUCCED:
			return {
				...state,
				deleting: false,
				deleted: true
			}
		case ADD_NEW:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false,
				record: null
			}
		case FETCH_STORES_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_STORES_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_STORES_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				stores: action.payload.list
			}
		case HIDE_ALERT:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false
			}
			case UPDATE_SEARCH_TEXT:
			return {
			  ...state,
			  searchText: action.payload.searchText,
			}     
		default:
			return state
	}
}

export default brand
