import {
  COUNT_SPONSOR_FAILED,
  COUNT_SPONSOR_SUCCED,
  COUNT_SPONSOR_PROCESSING,
  FETCH_SPONSOR_FAILED,
  FETCH_SPONSOR_SUCCED,
  FETCH_SPONSOR_PROCESSING,
  GET_SPONSOR_FAILED,
  GET_SPONSOR_SUCCED,
  GET_SPONSOR_PROCESSING,
  SAVE_SPONSOR_FAILED,
  SAVE_SPONSOR_SUCCED,
  SAVE_SPONSOR_PROCESSING,
  DELETE_SPONSOR_FAILED,
  DELETE_SPONSOR_SUCCED,
  DELETE_SPONSOR_PROCESSING,
  HIDE_ALERT,
  ADD_NEW,
  UPDATE_SEARCH_TEXT
} from '../constants'

const initialState = {
  fetching: false,
  fetched: false,
  counting: false,
  counted: false,
  saving: false,
  saved: false,
  deleting: false,
  deleted: false,
  errorMessage: null,
  list: [],
  total: 0,
  record: null,
  page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
	order: 'asc'
}

const sponsor = (state = initialState, action) => {
  switch (action.type) {

    case FETCH_SPONSOR_FAILED:
      return {
        ...state,
        fetching: false,
        fetched: false,
        errorMessage: action.payload.error,
      }
    case FETCH_SPONSOR_PROCESSING:
      return {
        ...state,
        fetching: true,
        fetched: false,
      }
    case FETCH_SPONSOR_SUCCED:
      return {
        ...state,
        fetching: false,
        fetched: true,
        list: action.payload.list,
        page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
      }
    case GET_SPONSOR_FAILED:
      return {
        ...state,
        fetching: false,
        fetched: true,
        errorMessage: action.payload.error,
      }
    case GET_SPONSOR_PROCESSING:
      return {
        ...state,
        fetching: true,
        fetched: false,
      }
    case GET_SPONSOR_SUCCED:
      return {
        ...state,
        fetching: false,
        fetched: true,
        record: action.payload.record,
      }
    case COUNT_SPONSOR_FAILED:
      return {
        ...state,
        counting: false,
        counted: false,
        errorMessage: action.payload.error,
      }
    case COUNT_SPONSOR_PROCESSING:
      return {
        ...state,
        counting: true,
        counted: false,
      }
    case COUNT_SPONSOR_SUCCED:
      return {
        ...state,
        counting: false,
        counted: true,
        total: action.payload.total,
      }
    case SAVE_SPONSOR_FAILED:
      return {
        ...state,
        saving: false,
        saved: true,
        errorMessage: action.payload.error,
      }
    case SAVE_SPONSOR_PROCESSING:
      return {
        ...state,
        saving: true,
        saved: false,
      }
    case SAVE_SPONSOR_SUCCED:
      return {
        ...state,
        saving: false,
        saved: true,
      }
    case DELETE_SPONSOR_FAILED:
      return {
        ...state,
        deleting: false,
        deleted: false,
        errorMessage: action.payload.error,
      }
    case DELETE_SPONSOR_PROCESSING:
      return {
        ...state,
        deleting: true,
        deleted: false,
      }
    case DELETE_SPONSOR_SUCCED:
      return {
        ...state,
        deleting: false,
        deleted: true,
      }
    case ADD_NEW:
      return {
        ...state,
        fetched: false,
        saved: false,
        errorMessage: null,
        deleted: false,
        record: null
      }     
    case HIDE_ALERT:
      return {
        ...state,
        fetched: false,
        saved: false,
        errorMessage: null,
        deleted: false,
      }
      case UPDATE_SEARCH_TEXT:
      return {
        ...state,
        searchText: action.payload.searchText,
      }     
    default:
      return state
  }
}

export default sponsor