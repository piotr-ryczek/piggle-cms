import {
	COUNT_STORE_FAILED,
	COUNT_STORE_SUCCED,
	COUNT_STORE_PROCESSING,
	FETCH_STORE_FAILED,
	FETCH_STORE_SUCCED,
	FETCH_STORE_PROCESSING,
	GET_STORE_FAILED,
	GET_STORE_SUCCED,
	GET_STORE_PROCESSING,
	SAVE_STORE_FAILED,
	SAVE_STORE_SUCCED,
	SAVE_STORE_PROCESSING,
	DELETE_STORE_FAILED,
	DELETE_STORE_SUCCED,
	DELETE_STORE_PROCESSING,
	UPDATE_LOCATION,
	HIDE_ALERT,
	ADD_NEW,
	FETCH_BRANDS_FAILED,
	FETCH_BRANDS_SUCCED,
	FETCH_BRANDS_PROCESSING,
	UPDATE_SEARCH_TEXT
} from '../constants'

const initialState = {
	fetching: false,
	fetched: false,
	counting: false,
	counted: false,
	saving: false,
	saved: false,
	deleting: false,
	deleted: false,
	errorMessage: null,
	list: [],
	total: 0,
	record: null,
	lat: 51.5073509,
	lng: -0.12775829999998223,
	brands: [],
	selectedBrand: null,
	page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
	order: 'asc'
}

const store = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_STORE_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_STORE_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_STORE_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				list: action.payload.list,
				page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
			}
		case GET_STORE_FAILED:
			return {
				...state,
				fetching: false,
				fetched: true,
				errorMessage: action.payload.error
			}
		case GET_STORE_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case GET_STORE_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				record: action.payload.record,
				selectedBrand: action.payload.selectedBrand,
				lat:action.payload.lat,
				lng:action.payload.lng
			}
		case COUNT_STORE_FAILED:
			return {
				...state,
				counting: false,
				counted: false,
				errorMessage: action.payload.error
			}
		case COUNT_STORE_PROCESSING:
			return {
				...state,
				counting: true,
				counted: false
			}
		case COUNT_STORE_SUCCED:
			return {
				...state,
				counting: false,
				counted: true,
				total: action.payload.total
			}
		case SAVE_STORE_FAILED:
			return {
				...state,
				saving: false,
				saved: true,
				errorMessage: action.payload.error
			}
		case SAVE_STORE_PROCESSING:
			return {
				...state,
				saving: true,
				saved: false
			}
		case SAVE_STORE_SUCCED:
			return {
				...state,
				saving: false,
				saved: true
			}
		case DELETE_STORE_FAILED:
			return {
				...state,
				deleting: false,
				deleted: false,
				errorMessage: action.payload.error
			}
		case DELETE_STORE_PROCESSING:
			return {
				...state,
				deleting: true,
				deleted: false
			}
		case DELETE_STORE_SUCCED:
			return {
				...state,
				deleting: false,
				deleted: true
			}
		case ADD_NEW:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false,
				record: null
			}
		case UPDATE_LOCATION:
			return {
				...state,
				record: action.payload.record,
				lat: action.payload.lat,
				lng: action.payload.lng
			}
		case FETCH_BRANDS_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_BRANDS_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_BRANDS_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				brands: action.payload.brands
			}
		case HIDE_ALERT:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false
			}
			case UPDATE_SEARCH_TEXT:
			return {
			  ...state,
			  searchText: action.payload.searchText,
			}     
		default:
			return state
	}
}

export default store
