import {
  COUNT_OFFER_FAILED,
  COUNT_OFFER_SUCCED,
  COUNT_OFFER_PROCESSING,
  FETCH_OFFER_FAILED,
  FETCH_OFFER_SUCCED,
  FETCH_OFFER_PROCESSING,
  GET_OFFER_FAILED,
  GET_OFFER_SUCCED,
  GET_OFFER_PROCESSING,
  SAVE_OFFER_FAILED,
  SAVE_OFFER_SUCCED,
  SAVE_OFFER_PROCESSING,
  DELETE_OFFER_FAILED,
  DELETE_OFFER_SUCCED,
  DELETE_OFFER_PROCESSING,
  FETCH_STORES_FAILED,
  FETCH_STORES_SUCCED,
  FETCH_STORES_PROCESSING,  
  FETCH_BRANDS_FAILED,
  FETCH_BRANDS_SUCCED,
  FETCH_BRANDS_PROCESSING,   
  FETCH_FACILITY_FAILED,
  FETCH_FACILITY_SUCCED,
  FETCH_FACILITY_PROCESSING,  
  FETCH_SUBCATEGORIES_FAILED,
  FETCH_SUBCATEGORIES_SUCCED,
  FETCH_SUBCATEGORIES_PROCESSING,
  FETCH_OFFER_BRAND_SUCCED,
  FETCH_OFFER_BRAND_FAILED,
  FETCH_BRAND_STORE_PROCESSING,
  FETCH_BRAND_STORE_SUCCED,
  FETCH_BRAND_STORE_FAILED,
  HIDE_ALERT,
  ADD_NEW,
  UPDATE_SEARCH_TEXT,
  UPDATE_OFFER_FORM
} from '../constants'

const initialState = {
  fetching: false,
  fetched: false,
  counting: false,
	counted: false,
  saving: false,
  saved: false,
  deleting: false,
  deleted: false,
  errorMessage: null,
  list: [],
  total: 0,
  record: null,
  stores: [],
  brands: [],
  subCategories: [],
  facilities: [],
  disable:true, 
  page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
  order: 'asc',
  loadingOffer: false,
  loadedOffer: false,
  fetchedBrands: false,
  fetchedStores: false,
  fetchedSubCategories: false,
  fetchedFacilities: false,
}

const offer = (state = initialState, action) => {
  switch (action.type) {

    case FETCH_OFFER_FAILED:
      return {
        ...state,
        fetching: false,
        fetched: false,
        errorMessage: action.payload.error,
      }
    case FETCH_OFFER_PROCESSING:
      return {
        ...state,
        fetching: true,
        fetched: false,
      }
    case FETCH_OFFER_SUCCED:
      return {
        ...state,
        fetching: false,
        fetched: true,
        list: action.payload.list,
        page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
      }
    case GET_OFFER_FAILED:
      return {
        ...state,
        loadingOffer: false,
        loadedOffer: true,
        errorMessage: action.payload.error,
      }
    case GET_OFFER_PROCESSING:
      return {
        ...state,
        loadingOffer: true,
        loadedOffer: false,
      }
    case GET_OFFER_SUCCED:
      return {
        ...state,
        loadingOffer: false,
        loadedOffer: true,
       // stores: action.payload.listStore,
        record: action.payload.record
      }
    case COUNT_OFFER_FAILED:
      return {
        ...state,
        counting: false,
	      counted: false,
        errorMessage: action.payload.error,
      }
    case COUNT_OFFER_PROCESSING:
      return {
        ...state,
        counting: true,
        counted: false,
      }
    case COUNT_OFFER_SUCCED:
      return {
        ...state,
        counting: false,
        counted: true,
        total: action.payload.total,
      }
    case SAVE_OFFER_FAILED:
      return {
        ...state,
        saving: false,
        saved: true,
        errorMessage: action.payload.error,
      }
    case SAVE_OFFER_PROCESSING:
      return {
        ...state,
        saving: true,
        saved: false,
      }
    case SAVE_OFFER_SUCCED:
      return {
        ...state,
        saving: false,
        saved: true,
      }
    case DELETE_OFFER_FAILED:
      return {
        ...state,
        deleting: false,
        deleted: false,
        errorMessage: action.payload.error,
      }
    case DELETE_OFFER_PROCESSING:
      return {
        ...state,
        deleting: true,
        deleted: false,
      }
    case DELETE_OFFER_SUCCED:
      return {
        ...state,
        deleting: false,
        deleted: true,
      }
		case FETCH_STORES_FAILED:
			return {
				...state,
				fetching: false,
				fetchedStores: false,
				errorMessage: action.payload.error
			}
		case FETCH_STORES_PROCESSING:
			return {
				...state,
				fetching: true,
				fetchedStores: false
			}
		case FETCH_STORES_SUCCED:
			return {
				...state,
				fetching: false,
        fetchedStores: true,
        disable:false,
				stores: action.payload.list
      }
		case FETCH_BRANDS_FAILED:
			return {
				...state,
				fetching: false,
				fetchedBrands: false,
				errorMessage: action.payload.error
			}
		case FETCH_BRANDS_PROCESSING:
			return {
				...state,
				fetching: true,
				fetchedBrands: false
			}
		case FETCH_BRANDS_SUCCED:
			return {
				...state,
				fetching: false,
				fetchedBrands: true,
				brands: action.payload.brands
      }            
      case FETCH_SUBCATEGORIES_FAILED:
			return {
				...state,
				fetching: false,
				fetchedSubCategories: false,
				errorMessage: action.payload.error
			}
		case FETCH_SUBCATEGORIES_PROCESSING:
			return {
				...state,
				fetching: true,
				fetchedSubCategories: false
			}
		case FETCH_SUBCATEGORIES_SUCCED:
			return {
				...state,
				fetching: false,
				fetchedSubCategories: true,
				subCategories: action.payload.list
      }  
    case FETCH_FACILITY_FAILED:
			return {
				...state,
				fetching: false,
				fetchedFacilities: false,
				errorMessage: action.payload.error
			}
		case FETCH_FACILITY_PROCESSING:
			return {
				...state,
				fetching: true,
				fetchedFacilities: false
			}
		case FETCH_FACILITY_SUCCED:
			return {
				...state,
				fetching: false,
				fetchedFacilities: true,
				facilities: action.payload.list
      }
    case FETCH_OFFER_BRAND_SUCCED:
			return {
        ...state,
        fetched: true,
      	stores: action.payload.list,
        record: action.payload.record,
        disable:false
      }
    case FETCH_OFFER_BRAND_FAILED:
      return {
        ...state,
        fetching: false,
        fetched: false,
        errorMessage: action.payload.error,
    }
    case FETCH_BRAND_STORE_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
    case FETCH_BRAND_STORE_SUCCED:
      return {
        ...state,
				fetching: false,
				fetched: true,
        stores: action.payload.list,
        disable:false
      }
    case FETCH_BRAND_STORE_FAILED:
      return {
        ...state,
        fetching: false,
        fetched: false,
        errorMessage: action.payload.error,
      } 
    case ADD_NEW:
      return {
        ...state,
        fetched: false,
        saved: false,
        errorMessage: null,
        deleted: false,
        record: null
      }     
    case HIDE_ALERT:
      return {
        ...state,
        record: null,
        fetched: false,
        fetchedBrands: false,
        fetchedStores: false,
        fetchedSubCategories: false,
        fetchedFacilities: false,
        loadingOffer: false,
        loadedOffer: false,
        saved: false,
        errorMessage: null,
        deleted: false,
      }
      case UPDATE_SEARCH_TEXT:
      return {
        ...state,
        searchText: action.payload.searchText,
      }        
      case UPDATE_OFFER_FORM:
      return {
        ...state,
        record: action.payload.record
      }      
    default:
      return state
  }
}

export default offer