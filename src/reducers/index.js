import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import auth from './auth.reducers'
import user from './user.reducers'
import childType from './childType.reducers'
import facility from './facility.reducers'
import brand from './brand.reducers'
import store from './store.reducers'
import category from './category.reducers'
import subCategory from './subCategory.reducers'
import sponsor from './sponsor.reducers'
import offer from './offer.reducers'

const rootReducer = combineReducers({
	auth,
	user,
	childType,
	facility,
	brand,
	store,
	category,
	subCategory,
  sponsor,
  offer,
	form: formReducer
})

export default rootReducer
