import {
	COUNT_CHILD_TYPE_FAILED,
	COUNT_CHILD_TYPE_SUCCED,
	COUNT_CHILD_TYPE_PROCESSING,
	FETCH_CHILD_TYPE_FAILED,
	FETCH_CHILD_TYPE_SUCCED,
	FETCH_CHILD_TYPE_PROCESSING,
	GET_CHILD_TYPE_FAILED,
	GET_CHILD_TYPE_SUCCED,
	GET_CHILD_TYPE_PROCESSING,
	SAVE_CHILD_TYPE_FAILED,
	SAVE_CHILD_TYPE_SUCCED,
	SAVE_CHILD_TYPE_PROCESSING,
	DELETE_CHILD_TYPE_FAILED,
	DELETE_CHILD_TYPE_SUCCED,
	DELETE_CHILD_TYPE_PROCESSING,
	HIDE_ALERT,
	ADD_NEW,
	TOGGLE_SIDEBAR,
	UPDATE_SEARCH_TEXT
} from '../constants'

const initialState = {
	fetching: false,
	fetched: false,
	counting: false,
	counted: false,
	saving: false,
	saved: false,
	deleting: false,
	deleted: false,
	errorMessage: null,
	list: [],
	total: 0,
	regions: [],
	record: null,
	sidebarOpen: false,
	page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
	order: 'asc'
}

const childType = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_CHILD_TYPE_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_CHILD_TYPE_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_CHILD_TYPE_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				list: action.payload.list,
				page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
			}
		case GET_CHILD_TYPE_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case GET_CHILD_TYPE_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case GET_CHILD_TYPE_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				record: action.payload.record
			}
		case COUNT_CHILD_TYPE_FAILED:
			return {
				...state,
				counting: false,
				counted: false,
				errorMessage: action.payload.error
			}
		case COUNT_CHILD_TYPE_PROCESSING:
			return {
				...state,
				counting: true,
				counted: false
			}
		case COUNT_CHILD_TYPE_SUCCED:
			return {
				...state,
				counting: false,
				counted: true,
				total: action.payload.total
			}
		case SAVE_CHILD_TYPE_FAILED:
			return {
				...state,
				saving: false,
				saved: false,
				errorMessage: action.payload.error
			}
		case SAVE_CHILD_TYPE_PROCESSING:
			return {
				...state,
				saving: true,
				saved: false
			}
		case SAVE_CHILD_TYPE_SUCCED:
			return {
				...state,
				saving: false,
				saved: true
			}
		case DELETE_CHILD_TYPE_FAILED:
			return {
				...state,
				deleting: false,
				deleted: false,
				errorMessage: action.payload.error
			}
		case DELETE_CHILD_TYPE_PROCESSING:
			return {
				...state,
				deleting: true,
				deleted: false
			}
		case DELETE_CHILD_TYPE_SUCCED:
			return {
				...state,
				deleting: false,
				deleted: true
			}
		case ADD_NEW:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false,
				record: null
			}
		case HIDE_ALERT:
			return {
				...state,
				fetched: false,
				saved: false,
				deleted: false,
				errorMessage: null
			}
		case TOGGLE_SIDEBAR:
			return {
				...state,
				sidebarOpen: !state.sidebarOpen
			}
			case UPDATE_SEARCH_TEXT:
			return {
			  ...state,
			  searchText: action.payload.searchText,
			}     
		default:
			return state
	}
}

export default childType
