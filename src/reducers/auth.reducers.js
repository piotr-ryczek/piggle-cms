import {
	LOGOUT,
	LOGIN_REQUEST,
	LOGIN_SUCCESS,
	LOGIN_FAILURE,
	SIGNUP_REQUEST,
	SIGNUP_SUCCESS,
	SIGNUP_FAILURE,
	HIDE_ALERT
} from '../constants'
import { auth as authFirebase } from '../api/firebase'
const initialState = {
	isSigningUp: false,
	isLoggingIn: false,
	isLoggedIn: authFirebase.currentUser ? true : false,
	token: null,
	userId: null,
	error: false,
	errorMessage: null
}

const auth = (state = initialState, action) => {
	switch (action.type) {
		case SIGNUP_REQUEST:
			return {
				...state,
				isSigningUp: true
			}

		case SIGNUP_SUCCESS:
			return {
				...state,
				isLoggedIn: true,
				isSigningUp: false,
				userId: action.payload.objectId,
				token: action.payload.sessionToken
			}

		case SIGNUP_FAILURE:
			return {
				...state,
				isSigningUp: false
			}

		case LOGIN_REQUEST:
			return {
				...state,
				isLoggingIn: true
			}

		case LOGIN_SUCCESS:
			return {
				...state,
				isLoggedIn: true,
				userId: action.payload.objectId,
				token: action.payload.sessionToken
			}

		case LOGIN_FAILURE:
			return {
				...state,
				error: action.payload.message ? true : false,
				errorMessage: action.payload.message,
				isLoggedIn: false,
				isLoggingIn: false
			}
		case LOGOUT:
			return {
				...state,
				isLoggedIn: false
			}
		case HIDE_ALERT:
			return {
				...state,
				error: false,
				errorMessage: null
			}
		default:
			return state
	}
}

export default auth
