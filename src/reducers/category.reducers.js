import {
	COUNT_CATEGORY_FAILED,
	COUNT_CATEGORY_SUCCED,
	COUNT_CATEGORY_PROCESSING,
	FETCH_CATEGORY_FAILED,
	FETCH_CATEGORY_SUCCED,
	FETCH_CATEGORY_PROCESSING,
	GET_CATEGORY_FAILED,
	GET_CATEGORY_SUCCED,
	GET_CATEGORY_PROCESSING,
	SAVE_CATEGORY_FAILED,
	SAVE_CATEGORY_SUCCED,
	SAVE_CATEGORY_PROCESSING,
	DELETE_CATEGORY_FAILED,
	DELETE_CATEGORY_SUCCED,
	DELETE_CATEGORY_PROCESSING,
	HIDE_ALERT,
	ADD_NEW,
	FETCH_SUBCATEGORIES_FAILED,
	FETCH_SUBCATEGORIES_SUCCED,
	FETCH_SUBCATEGORIES_PROCESSING,
	UPDATE_SEARCH_TEXT
} from '../constants'

const initialState = {
	fetching: false,
	fetched: false,
	counting: false,
	counted: false,
	saving: false,
	saved: false,
	deleting: false,
	deleted: false,
	errorMessage: null,
	list: [],
	total: 0,
	record: null,
	subCategories: [],
	page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
	order: 'asc'
}

const category = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_CATEGORY_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_CATEGORY_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_CATEGORY_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				list: action.payload.list,
				page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
			}
		case GET_CATEGORY_FAILED:
			return {
				...state,
				fetching: false,
				fetched: true,
				errorMessage: action.payload.error
			}
		case GET_CATEGORY_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case GET_CATEGORY_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				record: action.payload.record
			}
		case COUNT_CATEGORY_FAILED:
			return {
				...state,
				counting: false,
				counted: false,
				errorMessage: action.payload.error
			}
		case COUNT_CATEGORY_PROCESSING:
			return {
				...state,
				counting: true,
				counted: false
			}
		case COUNT_CATEGORY_SUCCED:
			return {
				...state,
				counting: false,
				counted: true,
				total: action.payload.total
			}
		case SAVE_CATEGORY_FAILED:
			return {
				...state,
				saving: false,
				saved: true,
				errorMessage: action.payload.error
			}
		case SAVE_CATEGORY_PROCESSING:
			return {
				...state,
				saving: true,
				saved: false
			}
		case SAVE_CATEGORY_SUCCED:
			return {
				...state,
				saving: false,
				saved: true
			}
		case DELETE_CATEGORY_FAILED:
			return {
				...state,
				deleting: false,
				deleted: false,
				errorMessage: action.payload.error
			}
		case DELETE_CATEGORY_PROCESSING:
			return {
				...state,
				deleting: true,
				deleted: false
			}
		case DELETE_CATEGORY_SUCCED:
			return {
				...state,
				deleting: false,
				deleted: true
			}
		case ADD_NEW:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false,
				record: null
			}
		case FETCH_SUBCATEGORIES_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_SUBCATEGORIES_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_SUBCATEGORIES_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				subCategories: action.payload.list
			}
		case HIDE_ALERT:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false
			}
			case UPDATE_SEARCH_TEXT:
			return {
			  ...state,
			  searchText: action.payload.searchText,
			}     
		default:
			return state
	}
}

export default category
