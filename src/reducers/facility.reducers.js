import {
	COUNT_FACILITY_FAILED,
	COUNT_FACILITY_SUCCED,
	COUNT_FACILITY_PROCESSING,
	FETCH_FACILITY_FAILED,
	FETCH_FACILITY_SUCCED,
	FETCH_FACILITY_PROCESSING,
	GET_FACILITY_FAILED,
	GET_FACILITY_SUCCED,
	GET_FACILITY_PROCESSING,
	SAVE_FACILITY_FAILED,
	SAVE_FACILITY_SUCCED,
	SAVE_FACILITY_PROCESSING,
	DELETE_FACILITY_FAILED,
	DELETE_FACILITY_SUCCED,
	DELETE_FACILITY_PROCESSING,
	ADD_NEW,
	HIDE_ALERT,
	UPDATE_SEARCH_TEXT
} from '../constants'

const initialState = {
	fetching: false,
	fetched: false,
	counting: false,
	counted: false,
	saving: false,
	saved: false,
	deleting: false,
	deleted: false,
	errorMessage: null,
	list: [],
	total: 0,
	facilities: [],
	record: null,
	page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
	order: 'asc'
}

const facility = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_FACILITY_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_FACILITY_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_FACILITY_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				list: action.payload.list,
				page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
			}
		case GET_FACILITY_FAILED:
			return {
				...state,
				fetching: false,
				fetched: true,
				errorMessage: action.payload.error
			}
		case GET_FACILITY_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case GET_FACILITY_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				record: action.payload.record
			}
		case COUNT_FACILITY_FAILED:
			return {
				...state,
				counting: false,
				counted: false,
				errorMessage: action.payload.error
			}
		case COUNT_FACILITY_PROCESSING:
			return {
				...state,
				counting: true,
				counted: false
			}
		case COUNT_FACILITY_SUCCED:
			return {
				...state,
				counting: false,
				counted: true,
				total: action.payload.total
			}
		case SAVE_FACILITY_FAILED:
			return {
				...state,
				saving: false,
				saved: true,
				errorMessage: action.payload.error
			}
		case SAVE_FACILITY_PROCESSING:
			return {
				...state,
				saving: true,
				saved: false
			}
		case SAVE_FACILITY_SUCCED:
			return {
				...state,
				saving: false,
				saved: true
			}
		case DELETE_FACILITY_FAILED:
			return {
				...state,
				deleting: false,
				deleted: false,
				errorMessage: action.payload.error
			}
		case DELETE_FACILITY_PROCESSING:
			return {
				...state,
				deleting: true,
				deleted: false
			}
		case DELETE_FACILITY_SUCCED:
			return {
				...state,
				deleting: false,
				deleted: true
			}
		case ADD_NEW:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false,
				record: null
			}
		case HIDE_ALERT:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false
			}
			case UPDATE_SEARCH_TEXT:
			return {
			  ...state,
			  searchText: action.payload.searchText,
			}     
		default:
			return state
	}
}

export default facility
