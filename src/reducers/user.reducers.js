import {
	COUNT_USER_FAILED,
	COUNT_USER_SUCCED,
	COUNT_USER_PROCESSING,
	FETCH_USER_FAILED,
	FETCH_USER_SUCCED,
	FETCH_USER_PROCESSING,
	GET_USER_FAILED,
	GET_USER_SUCCED,
	GET_USER_PROCESSING,
	FETCH_USER_REGIONS_FAILED,
	FETCH_USER_REGIONS_SUCCED,
	FETCH_USER_REGIONS_PROCESSING,
	SAVE_USER_FAILED,
	SAVE_USER_SUCCED,
	SAVE_USER_PROCESSING,
	DELETE_USER_FAILED,
	DELETE_USER_SUCCED,
	DELETE_USER_PROCESSING,
	HIDE_ALERT,
	TOGGLE_SIDEBAR,
	FETCH_CHILDTYPE_FAILED,
	FETCH_CHILDTYPE_SUCCED,
	FETCH_CHILDTYPE_PROCESSING,
	UPDATE_LOCATION,
	ADD_NEW,
	UPDATE_SEARCH_TEXT

} from '../constants'

const initialState = {
	fetching: false,
	fetched: false,
	counting: false,
	counted: false,
	saving: false,
	saved: false,
	deleting: false,
	deleted: false,
	errorMessage: null,
	list: [],
	total: 0,
	regions: [],
	record: null,
	sidebarOpen: false,
	lat: 51.5073509,
	lng: -0.12775829999998223,
	childType:[],
	page: 0,
	rowsPerPage: 10,
	searchText: '',
	orderBy: 'title',
	order: 'asc'
}

const user = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_USER_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_USER_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_USER_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				list: action.payload.list,
				page: action.payload.page,
				rowsPerPage: action.payload.rowsPerPage,
				searchText: action.payload.searchText,
				orderBy: action.payload.orderBy,
				order: action.payload.order
			}
		case GET_USER_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case GET_USER_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case GET_USER_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				record: action.payload.record,
				lat:action.payload.lat,
				lng:action.payload.lng
			}
		case COUNT_USER_FAILED:
			return {
				...state,
				counting: false,
				counted: false,
				errorMessage: action.payload.error
			}
		case COUNT_USER_PROCESSING:
			return {
				...state,
				counting: true,
				counted: false
			}
		case COUNT_USER_SUCCED:
			return {
				...state,
				counting: false,
				counted: true,
				total: action.payload.total
			}
		case FETCH_USER_REGIONS_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_USER_REGIONS_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_USER_REGIONS_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				regions: action.payload.regions
			}
		case SAVE_USER_FAILED:
			return {
				...state,
				saving: false,
				saved: false,
				errorMessage: action.payload.error
			}
		case SAVE_USER_PROCESSING:
			return {
				...state,
				saving: true,
				saved: false
			}
		case SAVE_USER_SUCCED:
			return {
				...state,
				saving: false,
				saved: true
			}
		case DELETE_USER_FAILED:
			return {
				...state,
				deleting: false,
				deleted: false,
				errorMessage: action.payload.error
			}
		case DELETE_USER_PROCESSING:
			return {
				...state,
				deleting: true,
				deleted: false
			}
		case DELETE_USER_SUCCED:
			return {
				...state,
				deleting: false,
				deleted: true
			}
		case TOGGLE_SIDEBAR:
			return {
				...state,
				sidebarOpen: !state.sidebarOpen
			}
		case FETCH_CHILDTYPE_FAILED:
			return {
				...state,
				fetching: false,
				fetched: false,
				errorMessage: action.payload.error
			}
		case FETCH_CHILDTYPE_PROCESSING:
			return {
				...state,
				fetching: true,
				fetched: false
			}
		case FETCH_CHILDTYPE_SUCCED:
			return {
				...state,
				fetching: false,
				fetched: true,
				childType: action.payload.childType
			}
		case UPDATE_LOCATION:
			return {
				...state,
				record: action.payload.record,
				lat: action.payload.lat,
				lng: action.payload.lng
		}
		case ADD_NEW:
			return {
			...state,
			fetched: false,
			saved: false,
			errorMessage: null,
			deleted: false,
			record: null
		}
		case HIDE_ALERT:
			return {
				...state,
				fetched: false,
				saved: false,
				errorMessage: null,
				deleted: false
			}
			case UPDATE_SEARCH_TEXT:
			return {
			  ...state,
			  searchText: action.payload.searchText,
			}     
		default:
			return state
	}
}

export default user
