import { db } from "../api/firebase" 
import {algolia} from "../api/algolia"
const Helper = {
    getInfoFromAddressComponent: function(address, field){
        for (var i = 0; i < address.length; i++) {
            if (address[i].types.includes(field)){
                 return address[i].short_name
            }            
       }
       return ''
    },
    isEmptyString: function(str){
        if (typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g,"") === "")
        {
            return true
        }
        else
        {
            return false
        }
    }, 
    getBase64: function(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.readAsDataURL(file)
            reader.onload = () => resolve(reader.result)
            reader.onerror = error => reject(error)
          })          
    },
    formatDataToFirebase: function(data, collection){
        var result = []
        var array_ids = []
        if(typeof data === 'string'){
        if(data !== null && data !== undefined && data !== ''){
            array_ids = data.split(',')
        }  
        }
        else{
            array_ids = data.map(s => {
            return s.value || s
        })
        }      
        for (var i = 0; i < array_ids.length; i++) {
            if(array_ids[i]){
                result.push(db.collection(collection).doc(array_ids[i]))
            }        
        }      
        result = result.filter(function (el) {
            return el != null
        })
        return result
    },
    getIdsInSelect: function(data){
        var array_ids = []
        if(typeof data === 'string'){
        if(data !== null && data !== undefined && data !== ''){
            array_ids = data.split(',')
        }  
        }
        else{
            array_ids = data.map(s => {
            return s.value || s
        })
        }     
        return array_ids
    },
    updateDatatoAlgolia:function(object,indexName){
        const index = algolia.initIndex(indexName)
        return new Promise((resolve, reject) => {
            index.saveObject(object)
            .then(res => { console.log('update  GOOD', res);resolve(res) })
            .catch(err => { console.log('update BAD', err);reject(err) })
        })
    },
    deleteDataAlgolia:function(objectID,indexName){
        const index = algolia.initIndex(indexName)
        return new Promise((resolve, reject) => {
        index.deleteObject(objectID)
            .then(res => { console.log('delete  GOOD', res);resolve(res) })
            .catch(err => { console.log('delete  Bad', err);reject(err) })
        })
    },
    addDatatoAlgolia:function(object,indexName){
        const index = algolia.initIndex(indexName)
        return new Promise((resolve, reject) => {
        index.addObject(object)
        .then(res => {console.log('add  GOOD', res); resolve(res) })
        .catch(err => {console.log('err add BAD', err); reject(err) })
        })
    }
}

export default Helper