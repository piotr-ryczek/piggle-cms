import React from 'react'
import PropTypes from 'prop-types'
import { List, ListItem, withStyles } from '@material-ui/core'

import footerStyle from '../../variables/styles/footerStyle'

function Footer({ ...props }) {
	const { classes } = props
	return (
		<footer className={classes.footer}>
			<div className={classes.container}>
				<div className={classes.left}>
					<List className={classes.list}>
						<ListItem className={classes.inlineBlock} style={{paddingTop:0,paddingBottom:0,paddingLeft:0}}>
							<a
								href="/dashboard"
								className={classes.block}
								target="_blank"
								rel="noopener noreferrer"
								style={{marginTop:'3px',fontSize:'14px'}}
							>
								Piggle CMS
							</a>
						</ListItem>
					</List>
				</div>
				<p className={classes.right}>
					<span>
					© Copyright {1900 + new Date().getYear()}{''}. All Rights Reserved.
						{/* <a href="/dashboard" className={classes.a}>
							Piggle
						</a>
						, made with ❤️ */}
					</span>
				</p>
			</div>
		</footer>
	)
}

Footer.propTypes = {
	classes: PropTypes.object.isRequired
}

export default withStyles(footerStyle)(Footer)
