import React, { Component } from 'react'
import { connect } from 'react-redux'
import { store } from '../../actions'
import CustomTable from '../Table/CustomTable'
import { Edit as EditIcon, Add as AddIcon, Delete as DeleteIcon } from '@material-ui/icons'
import { Button,  CircularProgress, Fade,  withStyles } from '@material-ui/core'
import { overlay, loadingSpinner } from '../../variables/styles'  
import SweetAlert from 'sweetalert2-react'
import debounce from 'lodash.debounce'
import { withLastLocation } from 'react-router-last-location'

class Store extends Component {
	constructor(props) {
		super(props)
		this.state = {
			fields: [
				{
					id: 'title',
					numeric: false,
					disablePadding: false,
					label: 'Title'
				},
				{
					id: 'address',
					numeric: false,
					disablePadding: false,
					label: 'Address'
				},
				{
					id: 'location',
					numeric: false,
					disablePadding: false,
					label: 'Location'
				},
				{
					id: 'brand',
					numeric: false,
					disablePadding: false,
					label: 'Brand'
				}
			]
		}
	}
	componentDidMount() {		
		var page = 0
		var searchText = ''
		if(this.props.lastLocation){
			if(this.props.lastLocation.pathname.includes('stores')){
				page = this.props.page
				searchText = this.props.searchText
			}
		}
		this.props.dispatch(store.count(searchText, this.props.orderBy, this.props.order))
		this._getStore(page, this.props.rowsPerPage, searchText, this.props.orderBy, this.props.order)
	}
	_getStore(page, rowsPerPage, searchText, orderBy, order) {
		this.props.dispatch(store.list(page, rowsPerPage, searchText, orderBy, order))
	}
	handleSort = (orderBy, order) => {
		this._getStore(this.props.page, this.props.rowsPerPage, this.props.searchText, orderBy, order)
	}
	handleChangePage = page => {
		this._getStore(page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	search = debounce(query => {
		this.props.dispatch(store.count(query))
		this._getStore(this.props.page, this.props.rowsPerPage, query, this.props.orderBy, this.props.order)
	}, 1000)
	handleSearch = query => {
		this.props.dispatch(store.updateSearchText(query))
		this.search(query)
	}
	handleChangeRowsPerPage = rowsPerPage => {
		this._getStore(this.props.page, rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	handleDeleteAction = (id) => {
		this.props.dispatch(store.delete(id))
	}
	hideAlert(event) {
		this.props.dispatch(store.hideAlert())
		this.props.dispatch(store.count())
		this.table.resetHeader()
		this._getStore(this.props.page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	render() {
		const { errorMessage, deleted, classes } = this.props
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
					<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade>
				<div style={{ textAlign: 'right' }}>
					<Button variant="raised" color="primary" onClick={() => this.props.history.push('/stores/add')}>
						<AddIcon /> Add New
					</Button>
				</div>
				<CustomTable
					onRef={(ref) => (this.table = ref)}
					total={this.props.total}
					history={this.props.history}
					tableHeaderColor="primary"
					tableHead={this.state.fields}
					searchText={this.props.searchText}
					page={this.props.page}
          			rowsPerPage={this.props.rowsPerPage}
					handleChangePage={this.handleChangePage}
					handleChangeRowsPerPage={this.handleChangeRowsPerPage}
					handleSearch={this.handleSearch}
					handleDelete={this.handleDeleteAction}
					handleSort={this.handleSort}
					data={this.props.list}
					editPath="/stores/edit/"
					actions={[
						{
							label: 'edit',
							icon: <EditIcon />,
							path: '/stores/edit/',
							has_id: true,
							color: 'primary'
						},
						{
							label: 'delete',
							icon: <DeleteIcon />,
							has_id: true,
							color: 'secondary'
						}
					]}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={deleted || errorMessage != null}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully deleted!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	fetching: state.store.fetching,
	fetched: state.store.fetched,
	total: state.store.total,
	list: state.store.list,
	page: state.store.page,
	rowsPerPage: state.store.rowsPerPage,
	searchText: state.store.searchText,
	orderBy: state.store.orderBy,
	order: state.store.order,
	deleted: state.store.deleted,
	errorMessage: state.store.errorMessage
})
const styles = theme => ({
	overlay,
	loadingSpinner
  })

export default withStyles(styles)(connect(mapStateToProps)(withLastLocation(Store)))
