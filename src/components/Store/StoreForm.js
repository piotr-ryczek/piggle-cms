import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, getFormValues } from 'redux-form'
import {
  TextField,
  Button,
  CardActions,
  CircularProgress,
  Fade,
  Grid,
  withStyles
} from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import RegularCard from '../Cards/RegularCard.js'
import { store } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import Geosuggest from 'react-geosuggest'
import Marker from '../CustomMap/Marker'
import GoogleMap from '../CustomMap/GoogleMap'
import Select from 'react-select'

import { store as storeReact } from '../../store'
import {required} from '../Validate/Validate'
export const fields = [
  'id',
  'title',
  'description',
  'place',
  'line1',
  'line2',
  'line3',
  'city',
  'postcode',
  'brand'
]
const renderInput = ({
  meta: { touched, error } = {},
  input: { ...inputProps },
  ...props
}) => (
  <TextField
    helperText={touched ? error : ''}
    error={touched ? (error === undefined ? false : true) : false}
    {...inputProps}
    {...props}
    fullWidth
  />
)

class StoreForm extends Component {

  submit(values) {
    values.id = this.props.initialValues.id
    values.location = {lat: this.props.lat || '', lng: this.props.lng || ''}
    this.props.dispatch(store.save(values))
  }
  componentDidMount() {
    this.props.dispatch(store.getBrands())
    if (this.props.match.params.id) {
      this.props.dispatch(store.get(this.props.match.params.id))
    }
    else{
      this.props.dispatch(store.addNew())
    }    
  }
  hideAlert(event) {
    this.props.dispatch(store.hideAlert())
    this.props.history.push('/stores')
  }
  onSuggestSelect(suggest) {    
    if(suggest){
      var recordData = this.props.record
      //if(!recordData){
        let values = getFormValues('store')(storeReact.getState())
        recordData = values
        recordData.address = {}
     // }
      this.props.dispatch(store.updateLocation(suggest, recordData))
    }    
  }
  render() {
    const { record, classes, handleSubmit, saving, saved, errorMessage, lat, lng, brands } = this.props
    const title =
      (this.props.match.params.id ? 'Edit ' : 'Add new') +
      (record !== null && record.title ? record.title : '')
    return (
      <div>
        <Fade in={this.props.fetching}>
          <div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
            <CircularProgress className={classes.loadingSpinner} />
          </div>
        </Fade>
        <RegularCard
          cardTitle={title}
          content={
            <form onSubmit={handleSubmit(this.submit.bind(this))}>              
              <div>
                <Field
                  name='title'
                  component={renderInput}
                  label='Title'
                  margin="normal"
                  validate={required}
                />
              </div>
              <div>
                <Field
                  multiline={true}
                  rowsMax="4"
                  name="description"
                  margin="normal"
                  component={renderInput}
                  label="Description"
                />
              </div>
              <div>
                <Field name="place"
                      label="Location"
                      validate={required}
                      component={props =>{
                        const {meta: {touched, error}} = props  
                        return(
                          <div>
                            <Geosuggest 
                              initialValue={props.input.value}
                              name="place"
                              onSuggestSelect={this.onSuggestSelect.bind(this)}
                              ref={el=>this._geoSuggest=el}
                              validate={required}
                              className = {touched && error ? 'dq-error': ''}
                            />
                             {touched && error && <p className="help is-error">{error}</p>}
                          </div>
                        )
                      }
                    }
                />
                <Grid container spacing={24}>
                  <Grid item xs={4} style={{padding:0,paddingLeft:'10px'}}>
                    <div>
                      <Field
                        name='line1'
                        component={renderInput}
                        label='Address Line1'
                        margin="normal"
                      />
                    </div>
                  </Grid>
                  <Grid item xs={4} style={{padding:0,paddingLeft:'10px'}}>
                    <div>
                      <Field
                        name='line2'
                        component={renderInput}
                        label='Address Line2'
                        margin="normal"
                      />
                    </div>
                  </Grid>
                  <Grid item xs={4} style={{padding:0,paddingLeft:'10px'}}>
                    <div>
                      <Field
                        name='line3'
                        component={renderInput}
                        label='Address Line3'
                        margin="normal"
                      />
                    </div>
                  </Grid>
                  <Grid item xs={6} style={{padding:0,paddingLeft:'10px'}}>
                    <div>
                      <Field
                        name='city'
                        component={renderInput}
                        label='City/town'
                        margin="normal"
                      />
                    </div>
                  </Grid>
                  <Grid item xs={6}  style={{padding:0,paddingLeft:'10px'}}>
                    <div>
                      <Field
                        name='postcode'
                        component={renderInput}
                        label='Postcode'
                        margin="normal"
                      />
                    </div>
                  </Grid>
                </Grid>
                <div style={{ height: '300px', width: '100%',marginTop:'15px' }}>
                  <GoogleMap
                    center={{lat: lat, lng: lng}}
                    defaultZoom={13}
                  >
                    <Marker
                      text='London'
                      lat={lat}
                      lng={lng}
                    />  
                  </GoogleMap>
                </div>
              </div>
              <div>
                <label>Brand</label>
                <Field name="brand"
                    label="Brand"
                    component={props =>
                        <Select
                            value={brands.filter(({value}) => value === props.input.value || value === props.input.value.value)}
                            onChange={props.input.onChange}
                            onBlur={() => {
                                props.input.onBlur(props.input.value)
                            }}
                            options={brands}
                            placeholder="Select a brand"
                            isMulti={false}
                        />
                    }
                />
              </div>

              <CardActions className={classes.actionSave}>
                <Button
                 className={classes.btnSave}
                  type="submit"
                  variant="raised"
                  color="primary"
                  disabled={this.props.fetching}
                >
                { saving ? (
                    <CircularProgress color="secondary" size={20}/>
                  ) : (
                    null
                  )}
                  Submit
                </Button>
                <Button
                className ={classes.btnBack}
                  variant="raised"
                 // color="secondary"
                  onClick={() => this.props.history.push('/stores')}
                >
                  Back to list
                </Button>
              </CardActions>
            </form>
          }
          footer={''}
        />
        <SweetAlert
            type={errorMessage ? 'error' : 'success'}
            show={saved || (errorMessage ? true : false)}
            title={errorMessage ? "Error" : "Notice"}
            text={errorMessage ? errorMessage : "Successfully saved!"}
            onConfirm={this.hideAlert.bind(this)}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  record: state.store.record,
  fetching: state.store.fetching,
  fetched: state.store.fetched,
  saving: state.store.saving,
  saved: state.store.saved,
  errorMessage: state.store.errorMessage,
  lat: state.store.lat,
  lng: state.store.lng,
  brands: state.store.brands,
  fields,
  initialValues: {
    id: state.store.record ? state.store.record.id : null,
    title: state.store.record ? state.store.record.title : '',
    description: state.store.record ? state.store.record.description : '',
    place: state.store.record ? state.store.record.place : (state.store.suggest ? state.store.suggest.place : ''),    
    line1: state.store.record && state.store.record.address ? state.store.record.address.line1 : (state.store.suggest ? state.store.suggest.line1 : ''),
    line2: state.store.record && state.store.record.address ? state.store.record.address.line2 : (state.store.suggest ? state.store.suggest.line2 : ''),
    line3: state.store.record && state.store.record.address ? state.store.record.address.line3 : (state.store.suggest ? state.store.suggest.line3 : ''),
    city: state.store.record && state.store.record.address ? state.store.record.address.city : (state.store.suggest ? state.store.suggest.city : ''),
    postcode: state.store.record && state.store.record.address ? state.store.record.address.postcode : (state.store.suggest ? state.store.suggest.postcode : ''),
    brand: state.store.record && state.store.record.brand ? state.store.record.brand : null,
  }
})

const styles = theme => ({
  actionSave: {
		position:'relative',
		top:'30px',
		left:'-15px'
	  },
	  btnSave:{
		order:1,
	  },
	  btnBack:{
		backgroudColor:'#e0e0e0',
		marginRight:'10px'
	  },
  overlay,
  loadingSpinner
})

export default withStyles(styles)(connect(mapStateToProps)(reduxForm({
  // a unique name for the form
  form: 'store',
  enableReinitialize: true,
  //keepDirtyOnReinitialize: true,
  destroyOnUnmount: false, 
	forceUnregisterOnUnmount: true, 
})(StoreForm)))
