import React, { Component } from 'react'
import { connect } from 'react-redux'
import { user } from '../../actions'
import CustomTable from '../Table/CustomTable'
//import { Add as AddIcon } from '@material-ui/icons'
import { Button,  CircularProgress, Fade,  withStyles } from '@material-ui/core'
import { overlay, loadingSpinner } from '../../variables/styles'  
import SweetAlert from 'sweetalert2-react'
import debounce from 'lodash.debounce'
import { withLastLocation } from 'react-router-last-location'

import {
	Edit as EditIcon,
	Add as AddIcon,
	Delete as DeleteIcon
  } from '@material-ui/icons'
class User extends Component {
	constructor(props) {
		super(props)
		this.state = {
			fields: [
				//{ id: 'email', numeric: false, disablePadding: false, label: 'Email' },
				{
					id: 'name',
					numeric: false,
					disablePadding: false,
					label: 'Name'
				},
				{
					id: 'email',
					numeric: false,
					disablePadding: false,
					label: 'Email'
				},
				{
					id: 'address',
					numeric: false,
					disablePadding: false,
					label: 'Address'
				},
				{
					id: 'role',
					numeric: false,
					disablePadding: false,
					label: 'Role'
				}
				// {
				// 	id: 'paidCredits',
				// 	numeric: false,
				// 	disablePadding: false,
				// 	label: 'Paid Credits'
				// },
				// {
				// 	id: 'regionName',
				// 	numeric: false,
				// 	disablePadding: false,
				// 	label: 'Region'
				// },
				// {
				// 	id: 'subscriptionStatus',
				// 	numeric: false,
				// 	disablePadding: false,
				// 	label: 'Subscription Status'
				// },
				// {
				// 	id: 'subscriptionCredits',
				// 	numeric: false,
				// 	disablePadding: false,
				// 	label: 'Subscription Credits'
				// }
			]
		}
	}
	componentDidMount() {		
		var page = 0
		var searchText = ''
		if(this.props.lastLocation){
			if(this.props.lastLocation.pathname.includes('user')){
				page = this.props.page
				searchText = this.props.searchText
			}
		}
		this.props.dispatch(user.count(searchText, this.props.orderBy, this.props.order))
		this._getUser(page, this.props.rowsPerPage, searchText, this.props.orderBy, this.props.order)
	}
	_getUser(page, rowsPerPage, searchText, orderBy, order) {
		this.props.dispatch(user.list(page, rowsPerPage, searchText, orderBy, order))
	}
	handleSort = (orderBy, order) => {
		this._getUser(this.props.page, this.props.rowsPerPage, this.props.searchText, orderBy, order)
	}
	handleChangePage = page => {
		this._getUser(page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}	
	search = debounce(query => {
		this.props.dispatch(user.count(query))
		this._getUser(this.props.page, this.props.rowsPerPage, query, this.props.orderBy, this.props.order)
	}, 1000)
	handleSearch = query => {
		this.props.dispatch(user.updateSearchText(query))
		this.search(query)
	}
	handleChangeRowsPerPage = rowsPerPage => {
		this._getUser(this.props.page, rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	handleDeleteAction = id => {
		this.props.dispatch(user.delete(id))
	}
	hideAlert(event) {
		this.props.dispatch(user.hideAlert())
		this.props.dispatch(user.count(this.props.searchText, this.props.orderBy, this.props.order))
		this.table.resetHeader()
		this._getUser(this.props.page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	render() {
		const { errorMessage, deleted, classes } = this.props
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
					<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade>     			
				<div style={{ textAlign: 'right' }}>
					<Button variant="raised" color="primary" onClick={() => this.props.history.push('/user/add')}>
						<AddIcon /> Add New
					</Button>
				</div>
				<CustomTable
					onRef={(ref) => (this.table = ref)}
					total={this.props.total}
					history={this.props.history}
					tableHeaderColor="primary"
					tableHead={this.state.fields}
					searchText={this.props.searchText}
					page={this.props.page}
          			rowsPerPage={this.props.rowsPerPage}
					handleChangePage={this.handleChangePage}
					handleChangeRowsPerPage={this.handleChangeRowsPerPage}
					handleSearch={this.handleSearch}
					handleDelete={this.handleDeleteAction}
					data={this.props.list}
					editPath="/user/edit/"
					actions={[
					    {label: "edit", icon: <EditIcon/>, path: "/user/edit/", has_id: true, color: "primary"},
					    {label: "delete", icon: <DeleteIcon/>, has_id: true, color: "secondary"}
					]}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={deleted || errorMessage != null}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully deleted!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	fetching: state.user.fetching,
	fetched: state.user.fetched,
	total: state.user.total,
	list: state.user.list,
	deleted: state.user.deleted,
	errorMessage: state.user.errorMessage,
	page: state.user.page,
	rowsPerPage: state.user.rowsPerPage,
	searchText: state.user.searchText,
	orderBy: state.user.orderBy,
	order: state.user.order
})

const styles = theme => ({
	overlay,
	loadingSpinner
  })
  

export default withStyles(styles)(connect(mapStateToProps)(withLastLocation(User)))
