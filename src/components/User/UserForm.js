import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, getFormValues } from 'redux-form'
//import { Field, reduxForm, } from 'redux-form'
import { TextField, Button, CardActions,withStyles,  CircularProgress} from '@material-ui/core'
import Select from 'react-select'
import SweetAlert from 'sweetalert2-react'

import RegularCard from '../Cards/RegularCard.js'
import { user } from '../../actions'
import {required,email,minLength6} from '../Validate/Validate'
import Geosuggest from 'react-geosuggest'

import GoogleMap from '../CustomMap/GoogleMap'
import Marker from '../CustomMap/Marker'
import { store as storeReact } from '../../store'
import { overlay, loadingSpinner } from '../../variables/styles'
const roles = [
	{value: 'admin', label: 'Admin'},
	{value: 'user', label: 'User'},
]
// @TODO: Add loading spinner (see FacilityForm)
const validate = values => {
	const errors = {};
	// if (!values.confirmPassword ) {
	//   errors.confirmPassword = 'Mandatory' ;
	// } else if (values.confirmPassword !== values.password) {
	//   errors.confirmPassword = 'Password mismatched' ;
	// }
	if((values.confirmPassword || values.password) && (values.confirmPassword !== values.password)) {
		  errors.confirmPassword = 'Password mismatched'
	}
	return errors;
  
  
  }
export const fields = [ 'id','uid','name','email', 'password', 'confirmPassword', 'place','role' ]
const renderInput = ({ meta: { touched, error } = {}, input: { ...inputProps }, ...props }) => (
	<TextField
		helperText={touched ? error : ''}
		error={touched ? error === undefined ? false : true : false}
		{...inputProps}
		{...props}
		fullWidth
	/>
)
class UsersForm extends Component {
	// constructor(props) {
	// 	super(props)
	// }
	handleChangeImage(e) {}
	submit(values) {
		values.id = this.props.initialValues.id
		values.location = {lat: this.props.lat || '', lng: this.props.lng || ''}
		this.props.dispatch(user.save(values))
	}
	componentDidMount() {
	//	this.props.dispatch(user.getRegions())
		this.props.dispatch(user.getChildType())
		if (this.props.match.params.id) {
			this.props.dispatch(user.get(this.props.match.params.id))
		}else {
			this.props.dispatch(user.addNew())
		}
	}
	onSuggestSelect(suggest) {    
		if(suggest){
		  var recordData = this.props.record
		  //if(!recordData){
			let values = getFormValues('user')(storeReact.getState())
			recordData = values
		  //}
		  this.props.dispatch(user.updateLocation(suggest, recordData))
		}    
	  }
	hideAlert(event) {
		this.props.dispatch(user.hideAlert())
		//this.props.record ? window.location.reload() : this.props.history.push('/user')
		this.props.history.push('/user')
	}
	render() {
		const {record, handleSubmit,classes, saving, saved, errorMessage,childType ,lat, lng } = this.props
		const title =
		(this.props.match.params.id ? 'Edit ' : 'Add new ') + (record !== null && record.name ? record.name : '')
		return (
			<div>
				<RegularCard
					cardTitle={title}
					content={
						<form onSubmit={handleSubmit(this.submit.bind(this))}>
							<div className={'dq-label'}>
								<Field validate={required} name="name" component={renderInput} label="Name" />
							</div>
							<div style={{paddingTop:'15px'}} className={'dq-label'}>
								<Field 
									   validate={this.props.match.params.id ? '':[required,email]} 
									   name="email" 
									   component={renderInput} 
									   label="Email" 
									   disabled = {this.props.match.params.id ? true : false}
								/>
							</div>
							<div className={'dq-label'}>
								<label>Role</label>
								<Field name="role"
									validate={required}
									label="Role"
									component={props => {
									const {meta: {touched, error}} = props  
										return(
										<div>
										<Select
											name="role"
											value={roles.filter(({value}) => value === props.input.value || value === props.input.value.value)}
											onChange={props.input.onChange}
											onBlur={() => {
												props.input.onBlur(props.input.value)
											}}
											options={roles}
											placeholder="Role"
											isMulti={false}
											validate={required}
										/>
										{touched && error && <p className="help is-error">{error}</p>}
										</div>
										)
									}
									}
								/>
							</div>
							<div className={'dq-label'}>
								<Field name="place"
									label="Location"
									validate={required}
									component={props =>{
										const {meta: {touched, error}} = props  
										return(
										<div>
											<Geosuggest 
											initialValue={props.input.value}
											name="place"
											onSuggestSelect={this.onSuggestSelect.bind(this)}
											ref={el=>this._geoSuggest=el}
											validate={required}
											className = {touched && error ? 'dq-error': ''}
											/>
											{touched && error && <p className="help is-error">{error}</p>}
										</div>
										)
									}
									}
								/>
							</div>
							<div style={{ height: '300px', width: '100%' }}>
								<GoogleMap
									center={{lat: lat, lng: lng}}
									defaultZoom={13}
								>
									<Marker
									text='London'
									lat={lat}
									lng={lng}
									/>  
								</GoogleMap>
								</div>
							<div className={'dq-label'}>
								<label>Child Type</label>
								<Field name="childType"
									validate={required}
									label="childType"
									component={props => {
									const {meta: {touched, error}} = props  
										return(
										<div>
										<Select
											name="childType"
											value={childType.filter(function(value){
												if(props.input.value.includes(value.value) || props.input.value.map(s => s.value).includes(value.value)){
													return value  
												}   
												return null                     
											})}
											onChange={props.input.onChange}
											onBlur={() => {
												props.input.onBlur(props.input.value)
											}}
											options={childType}
											placeholder="Child Type"
											isMulti={true}
											validate={required}
										/>
										{touched && error && <p className="help is-error">{error}</p>}
										</div>
										)
									}
									}
								/>
							</div>
							{this.props.match.params.id ? '': (
								<div>
									<Field validate={[required,minLength6]}
										name="password"
										type="password"
										component={renderInput}
										label="Password"
										style={{margin:'10px 0'}}
									/>
									<Field
								    validate={required}
									name="confirmPassword"
									type="password"
									component={renderInput}
									label="Confirm Password"
									style={{margin:'10px 0'}}
									/>
								</div>
							)}
							{/* <div>
								<label>Region</label>
								<Field
									name="region"
									label="Region"
									component={(props) => (
										<Select
											value={props.input.value}
											onChange={props.input.onChange}
											onBlur={() => {
												props.input.onBlur(props.input.value)
											}}
											simpleValue
											options={regions}
											placeholder="Select regions"
											multi={false}
										/>
									)}
								/>
							</div> */}
							{record !== null && record.is_subscription_active ?
							(<div style={{marginTop:'20px'}} className="subscriptions">
									<div className="label">Subscription</div>
									<div className="active"><span className="title">Status: </span>{!record.is_expire ? 'Active':'Expired'}</div>
									<div className="transaction"><span className="title">Transaction ID: </span> {record.original_transaction_id}</div>
								    <div className="expire"><span className="title">Expired date: </span> {record.expire}</div>
									<div className="receipt"><span className="title">Receipt No.: </span><div className="content">{record.receipt}</div></div>	
								</div>) : (
								<div style={{marginTop:'20px'}} className="subscriptions">
									<div className="label">Subscription</div>
									<div className="active"><span className="title">Status: </span>Not Active </div>
								</div>
								)
							}
							<CardActions className={classes.actionSave}>
								<Button type="submit" className={classes.btnSave} variant="raised" color="primary" disabled={this.props.saving}>
									{ saving ? (
										<CircularProgress color="secondary" size={20}/>
									) : (
										null
									)}
									Submit
								</Button>
								<Button
								    className ={classes.btnBack}
									variant="raised"
									//color="secondary"
									onClick={() => this.props.history.push('/user')}
								>
									Back to list
								</Button>
							</CardActions>
						</form>
					}
					footer={''}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={saved || (errorMessage ? true : false)}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully saved!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	record: state.user.record,
	regions: state.user.regions,
	fetching: state.user.fetching,
	fetched: state.user.fetched,
	saving: state.user.saving,
	saved: state.user.saved,
	errorMessage: state.user.errorMessage,
	childType: state.user.childType,
	lat: state.user.lat,
	lng: state.user.lng,
	fields,
	initialValues: {
		id: state.user.record ? state.user.record.id : null,
		name: state.user.record ? state.user.record.name : '',
		email: state.user.record ? state.user.record.email : '',
		place: state.user.record ? state.user.record.place : (state.user.suggest ? state.user.suggest.place : ''),
		password: '',
		role: state.user.record ? state.user.record.role : 'user',
		//region: state.user.record && state.user.record.get('region') ? state.user.record.get('region').id : null,
		childType: state.user.record ? state.user.record.childType : []
	}
})
const styles = (theme) => ({
	actionSave: {
		position:'relative',
		top:'30px',
		left:'-15px'
	  },
	  btnSave:{
		order:1,
	  },
	  btnBack:{
		backgroudColor:'#e0e0e0',
		marginRight:'10px'
	  },
	overlay,
	loadingSpinner
})
export default withStyles(styles)(
connect(mapStateToProps)(
	reduxForm({
		// a unique name for the form
		form: 'user',
		enableReinitialize: true,
		//keepDirtyOnReinitialize: true,
		destroyOnUnmount: false, 
		forceUnregisterOnUnmount: true, 
		validate
	})(UsersForm)
)
)
