import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import {
  TextField,
  Button,
  CardActions,
  CircularProgress,
  Fade,
  Grid,
  withStyles
} from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import RegularCard from '../Cards/RegularCard.js'
import { sponsor } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import CustomFieldUpload from '../CustomInput/CustomFileUpload'
import Datetime from 'react-datetime'
import {required,hyperlink} from '../Validate/Validate'
export const fields = [
  'id',
  'title',
  'description',
  'image',
  'url',
  'startDate',
  'enDate'
]
const renderInput = ({
  meta: { touched, error } = {},
  input: { ...inputProps },
  ...props
}) => (
  <TextField
    helperText={touched ? error : ''}
    error={touched ? (error === undefined ? false : true) : false}
    {...inputProps}
    {...props}
    fullWidth
  />
)
const validate = values => {
	const errors = {};
	if(values.endDate && values.startDate) {
     if(Date.parse(values.startDate) > Date.parse(values.endDate)){
         errors.startDate = 'Start date must be smaller than End date' ;
     }
	}
	return errors;
}
class SponsorForm extends Component {

  submit(values) {
    values.id = this.props.initialValues.id
    console.log(!values.image)
    if(this.props.match.params.id){
      if(this.props.record.image && !values.image ){
        values.image = this.props.record.image
      }
      this.props.dispatch(sponsor.save(values))
    }else{
      this.props.dispatch(sponsor.save(values))
    }
  }
  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.dispatch(sponsor.get(this.props.match.params.id))
    }
    else{
      this.props.dispatch(sponsor.addNew())
    }    
  }
  hideAlert(event) {
    this.props.dispatch(sponsor.hideAlert())
    this.props.history.push('/sponsors')
  }
  render() {
    const { record, classes, handleSubmit, saving, saved, errorMessage } = this.props
    const title =
      (this.props.match.params.id ? 'Edit ' : 'Add new') +
      (record !== null && record.title ? record.title : '')
    return (
      <div>
        <Fade in={this.props.fetching}>
          <div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
            <CircularProgress className={classes.loadingSpinner} />
          </div>
        </Fade>
        <RegularCard
          cardTitle={title}
          content={
            <form onSubmit={handleSubmit(this.submit.bind(this))}>              
              <div>
                <Field
                    component={CustomFieldUpload}
                    file={record && record.image ? record.image : null}
                    isImage={true}
                    name="image"
                    label="Image"
                    validate={record && record.image ? '' : required}
                />
              </div>
              <div>
                <Field
                  name='title'
                  component={renderInput}
                  label='Title (for reference only)'
                  margin="normal"
                />
              </div>
              <div>
                  <Field
                    multiline={true}
                    rowsMax="4"
                    name="description"
                    margin="normal"
                    component={renderInput}
                    label="Description"
                  />
              </div>
              <div>
                <Field
                  name='url'
                  component={renderInput}
                  label='Url'
                  margin="normal"
                  validate={[required,hyperlink]}
                />
              </div>
              <Grid container spacing={24}>
                <Grid item xs={6}>
                  <div style={{marginTop:'-20px'}}>
                    <label style={{position:'relative',top:'10px'}}>Start date</label>
                    <Field name="startDate"
                        validate={required}
                        component={props =>{
                           const {meta: {touched, error}} = props  
                            return(
                              <div>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  name="startDate"
                                  value={props.input.value}
                                  onChange={props.input.onChange}
                                  onBlur={() => {
                                      props.input.onBlur(props.input.value)
                                  }}
                                />
                                 {touched && error && <p className="help is-error">{error}</p>}
                              </div>
                            )
                          }
                        }
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div style={{marginTop:'-20px'}}>
                    <label style={{position:'relative',top:'10px'}}>End date</label>
                    <Field name="endDate"
                        validate={required}
                        component={props =>{
                          const {meta: {touched, error}} = props  
                          return(
                            <div>
                              <Datetime
                                input ={true}
                                inputProps={{readOnly: true}}
                                name="endDate"
                                value={props.input.value}
                                onChange={props.input.onChange}
                                onBlur={() => {
                                    props.input.onBlur(props.input.value)
                                }}
                              />
                              {touched && error && <p className="help is-error">{error}</p>}
                            </div>
                          )
                        }
                      }
                    />
                  </div>
                </Grid>
              </Grid>

              <CardActions className={classes.actionSave}>
                <Button
                  className={classes.btnSave}
                  type="submit"
                  variant="raised"
                  color="primary"
                  disabled={this.props.fetching}
                >
                  { saving ? (
                    <CircularProgress color="secondary" size={20}/>
                  ) : (
                    null
                  )}                  
                  Submit
                </Button>
                <Button
                  className ={classes.btnBack}
                  variant="raised"
                  //color="secondary"
                  onClick={() => this.props.history.push('/sponsors')}
                >
                  Back to list
                </Button>
              </CardActions>
            </form>
          }
          footer={''}
        />
        <SweetAlert
            type={errorMessage ? 'error' : 'success'}
            show={saved || (errorMessage ? true : false)}
            title={errorMessage ? "Error" : "Notice"}
            text={errorMessage ? errorMessage : "Successfully saved!"}
            onConfirm={this.hideAlert.bind(this)}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  record: state.sponsor.record,
  fetching: state.sponsor.fetching,
  fetched: state.sponsor.fetched,
  saving: state.sponsor.saving,
  saved: state.sponsor.saved,
  errorMessage: state.sponsor.errorMessage,
  fields,
  initialValues: {
    id: state.sponsor.record ? state.sponsor.record.id : null,
    title: state.sponsor.record ? state.sponsor.record.title : '',
    description: state.sponsor.record ? state.sponsor.record.description : '',
    url: state.sponsor.record ? state.sponsor.record.url : '',
    startDate: state.sponsor.record ? state.sponsor.record.startDate : null,
    endDate: state.sponsor.record ? state.sponsor.record.endDate : null,
  }
})

const styles = theme => ({
  actionSave: {
    position:'relative',
    paddingTop:'70px',
    left:'-15px'
  },
  btnSave:{
    order:1,
  },
  btnBack:{
    backgroudColor:'#e0e0e0',
    marginRight:'10px'
  },
  overlay,
  loadingSpinner
})

export default withStyles(styles)(connect(mapStateToProps)(reduxForm({
  // a unique name for the form
  form: 'sponsor',
  enableReinitialize: true,
 // keepDirtyOnReinitialize: true,
  destroyOnUnmount: false, 
  forceUnregisterOnUnmount: true, 
  validate
})(SponsorForm)))
