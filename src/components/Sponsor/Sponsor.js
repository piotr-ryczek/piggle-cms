import React, { Component } from 'react'
import { connect } from 'react-redux'
import { sponsor } from '../../actions'
import CustomTable from '../Table/CustomTable'
import {
  Edit as EditIcon,
  Add as AddIcon,
  Delete as DeleteIcon
} from '@material-ui/icons'
import { Button,  CircularProgress, Fade,  withStyles } from '@material-ui/core'
import { overlay, loadingSpinner } from '../../variables/styles'  
import SweetAlert from 'sweetalert2-react'
import debounce from 'lodash.debounce'
import { withLastLocation } from 'react-router-last-location'

class Sponsor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fields: [
        {
          id: 'image',
          numeric: false,
          image: true,
          disablePadding: false,
          width: 128,
          label: 'Image'
        },
        {
          id: 'title',
          numeric: false,
          disablePadding: false,
          label: 'Title'
        },
        {
          id: 'url',
          numeric: false,
          disablePadding: false,
          label: 'Url'
        },
        {
          id: 'startDate',
          numeric: false,
          disablePadding: false,
          label: 'Start Date'
        },
        {
          id: 'endDate',
          numeric: false,
          disablePadding: false,
          label: 'End Date'
        }
      ]
    }
  }
	componentDidMount() {    
    var page = 0
    var searchText = ''
		if(this.props.lastLocation){
			if(this.props.lastLocation.pathname.includes('sponsors')){
        page = this.props.page
        searchText = this.props.searchText
			}
    }
    this.props.dispatch(sponsor.count(searchText, this.props.orderBy, this.props.order))
		this._getSponsor(page, this.props.rowsPerPage, searchText, this.props.orderBy, this.props.order)
	}
	_getSponsor(page, rowsPerPage, searchText, orderBy, order) {
		this.props.dispatch(sponsor.list(page, rowsPerPage, searchText, orderBy, order))
	}
	handleSort = (orderBy, order) => {
		this._getSponsor(this.props.page, this.props.rowsPerPage, this.props.searchText, orderBy, order)
	}
	handleChangePage = page => {
		this._getSponsor(page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	search = debounce(query => {
		this.props.dispatch(sponsor.count(query))
		this._getSponsor(this.props.page, this.props.rowsPerPage, query, this.props.orderBy, this.props.order)
	}, 1000)
	handleSearch = query => {
		this.props.dispatch(sponsor.updateSearchText(query))
		this.search(query)
	}
	handleChangeRowsPerPage = rowsPerPage => {
		this._getSponsor(this.props.page, rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
  handleDeleteAction = id => {
    this.props.dispatch(sponsor.delete(id))
  }
  hideAlert(event) {
		this.props.dispatch(sponsor.hideAlert())
		this.props.dispatch(sponsor.count())
		this.table.resetHeader()
		this._getSponsor(this.props.page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
  }
  render() {
    const { errorMessage, deleted, classes } = this.props
    return (
      <div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
					<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade>      
        <div style={{ textAlign: 'right' }}>
          <Button
            variant="raised"
            color="primary"
            onClick={() => this.props.history.push('/sponsors/add')}
          >
            <AddIcon /> Add New
          </Button>
        </div>
        <CustomTable
          onRef={ref => (this.table = ref)} 
          total={this.props.total}
          history={this.props.history}
          tableHeaderColor="primary"
          tableHead={this.state.fields}
          searchText={this.props.searchText}
          page={this.props.page}
          rowsPerPage={this.props.rowsPerPage}
          handleChangePage={this.handleChangePage}
          handleChangeRowsPerPage={this.handleChangeRowsPerPage}
          handleSearch={this.handleSearch}
          handleDelete={this.handleDeleteAction}
          handleSort={this.handleSort}
          data={this.props.list}
          editPath="/sponsors/edit/"
          actions={[
            {
              label: 'edit',
              icon: <EditIcon />,
              path: '/sponsors/edit/',
              has_id: true,
              color: 'primary'
            },
            {
              label: 'delete',
              icon: <DeleteIcon />,
              has_id: true,
              color: 'secondary'
            }
          ]}
        />
        <SweetAlert
          type={errorMessage ? 'error' : 'success'}
          show={deleted || errorMessage != null}
          title={errorMessage ? 'Error' : 'Notice'}
          text={errorMessage ? errorMessage : 'Successfully deleted!'}
          onConfirm={this.hideAlert.bind(this)}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
	fetching: state.sponsor.fetching,
	fetched: state.sponsor.fetched,  
  total: state.sponsor.total,
  list: state.sponsor.list,
  page: state.sponsor.page,
  rowsPerPage: state.sponsor.rowsPerPage,
  searchText: state.sponsor.searchText,
  orderBy: state.sponsor.orderBy,
  order: state.sponsor.order,
  deleted: state.sponsor.deleted,
  errorMessage: state.sponsor.errorMessage
})
const styles = theme => ({
	overlay,
	loadingSpinner
  })
export default withStyles(styles)(connect(mapStateToProps)(withLastLocation(Sponsor)))
