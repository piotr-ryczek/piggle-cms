import React, { Component } from 'react'
import { connect } from 'react-redux'
import CustomSnackbar from './CustomSnackbar/CustomSnackbar'
import logoUrl from '../assets/img/logo.svg'
import LoginForm from './auth/LoginForm'
import { auth } from '../actions'
import RegularCard from './Cards/RegularCard.js'
import { Grid } from '@material-ui/core'

class Login extends Component {
	constructor(props) {
		super(props)
		this.state = {
			showInfo: false,
			showMessage: ''
		}
	}

	submit(values) {
		this.props.dispatch(auth.login(values))
	}

	handleCloseError() {
		this.props.dispatch(auth.hideAlert())
	}

	render() {
		const { loading, error, errorMessage } = this.props
		return (
			<div>
				<CustomSnackbar
					variant="error"
					open={error}
					message={errorMessage}
					handleClose={this.handleCloseError.bind(this)}
				/>
				<Grid container justify="center">
					<Grid xs={4} item>
						<RegularCard
							cardTitle="Piggle CMS"
							cardSubtitle="Log in"
							mediaUrl={logoUrl}
							mediaTitle="Piggle"
							content={
								<LoginForm error={errorMessage} loading={loading} onSubmit={this.submit.bind(this)} />
							}
							footer={''}
						/>
					</Grid>
				</Grid>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	isLoggedIn: state.auth.isLoggedIn,
	userId: state.auth.userId,
	token: state.auth.token,
	error: state.auth.error,
	errorMessage: state.auth.errorMessage
})

export default connect(mapStateToProps)(Login)
