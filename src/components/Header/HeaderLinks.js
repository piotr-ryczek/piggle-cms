import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import { Manager, Reference, Popper } from 'react-popper'
import { IconButton, MenuItem, MenuList, Grow, Paper, ClickAwayListener, Hidden, withStyles } from '@material-ui/core'
import { Person, Dashboard } from '@material-ui/icons'
import {firebase,db} from "../../api/firebase"
import {Link } from "react-router-dom"
import { auth } from '../../actions'
import CustomInput from '../CustomInput/CustomInput'
import headerLinksStyle from '../../variables/styles/headerLinksStyle'
class HeaderLinks extends React.Component {
	state = {
		open: false,
		linkProfile :null
	};
	handleClick = () => {
		this.setState({ open: !this.state.open })
	};
	componentDidMount(){
		let user = firebase.auth().currentUser
		if(user){
			var idDocs = []	
			let query = db.collection("users")
			query.where('uid', '==', user.uid.trim()).get().then(
			 async (collection) => {
			await collection.docs.map(doc => {
				idDocs.push(doc.id)
				})
				let linkProfile ='/user/edit/' + idDocs[0]
				this.setState({linkProfile:linkProfile})
			  })
		}
	}
	handleClose = () => {
		this.setState({ open: false })
	};
	signOut = () => {
		this.props.dispatch(auth.logout())
	};
	render() {
		const { classes } = this.props
		const { open,linkProfile } = this.state
		return (
			<div style={{marginTop:"30px"}}>
				{/* <CustomInput
					formControlProps={{
						className: classes.top + ' ' + classes.search
					}}
					inputProps={{
						placeholder: 'Search',
						inputProps: {
							'aria-label': 'Search'
						}
					}}
				/> */}
				{/* <IconButton color="inherit" aria-label="Dashboard" className={classes.buttonLink}>
					<Dashboard className={classes.links} />
					<Hidden mdUp>
						<p className={classes.linkText}>Dashboard</p>
					</Hidden>
				</IconButton> */}
				<Manager style={{ display: 'inline-block' }}>
					<Reference>
						{({ ref }) => (
							<IconButton style={{margin:'auto',width:'48px'}}
								color="inherit"
								aria-label="Person"
								aria-owns={open ? 'menu-list' : null}
								aria-haspopup="true"
								onClick={this.handleClick}
								className={classes.buttonLink}
							>
								<Person className={classes.links} style={{margin:0}}/>
								{/* <Hidden mdUp>
									<p onClick={this.handleClick} className={classes.linkText}>
										Profile
									</p>
								</Hidden> */}
							</IconButton>
						)}
					</Reference>
					<Popper placement="bottom-start" eventsEnabled={open}>
						{({ ref, style, placement, outOfBoundaries, scheduleUpdate, arrowProps }) => (
							<div
								ref={ref}
								className={
									classNames({ [classes.popperClose]: !open }) + ' ' + classes.pooperResponsive
								}
								style={{
									position: 'absolute',
									willChange: 'transform',
									right:'10px'
								}}
								data-placement={placement}
							>
								<ClickAwayListener onClickAway={this.handleClose} >
									<Grow in={open} id="menu-list" style={{ transformOrigin: '0 0 0' }}>
										<Paper className={classes.dropdown}>
											<MenuList role="menu">
												<MenuItem  onClick={this.handleClose} className={classes.dropdownItem}>
													{linkProfile ? (<Link to={linkProfile} className="editprofile">Edit Profile</Link>) : 'Edit Profile'}	
												</MenuItem>
												<MenuItem onClick={this.signOut} className={classes.dropdownItem}>
													Sign out
												</MenuItem>
											</MenuList>
										</Paper>
									</Grow>
								</ClickAwayListener>
							</div>
						)}
					</Popper>
				</Manager>
			</div>
		)
	}
}
const mapStateToProps = (state) => ({})
export default withStyles(headerLinksStyle)(connect(mapStateToProps)(HeaderLinks))
