import React, { Component } from 'react'
import { Button } from '@material-ui/core'
import Helper from '../../utils/Helper'
import {
    FileUpload
  } from '@material-ui/icons'
class CustomFileUpload extends Component {
	constructor(props, context) {
		super(props, context)

		this.state = {
            file: null,
            fileBase64: null,
			fileName: '',
        }
        this.onChange = this.onChange.bind(this)
    }
    onChange(e) {
        let that = this
        const { input: { onChange } } = this.props
        if(e.target.files.length){
            const file = e.target.files[0]
            Helper.getBase64(file).then(data => {
                that.setState({
                    file: file,
                    fileBase64: data,
                    fileName: file.name
                })
                onChange(file)
            })            
        }
        else{
            onChange(null)
        }        
      }
    render() {
        const { input: { value: omitValue, onChange, onBlur, ...inputProps }, meta: { touched, error }, file, isImage, ...props } = this.props
        return (
            <div>
                {/* <label> */}
                 <label htmlFor={inputProps.name}>
                    <p>{props.label}</p>                    
                    {this.state.fileBase64 ? (
                        <p>
                            <img src={this.state.fileBase64} alt={this.state.fileName} className='img-preview'/>
                        </p>
                    ) : ( 
                       <p>{file ? (
                           <span>{isImage ? (
                            <img src={file} alt={file} className='img-preview'/>
                           ) : (
                               {file}
                           )}</span>
                       ) : (
                           null
                       )}</p>
                    )}
                    <Button variant="raised" component="label">
                        Upload
                        <FileUpload/>
                        <input
                            accept="image/*"
                            style={{ display: 'none' }}
                            onChange={this.onChange}
                            onBlur={this.onChange}
                            id="raised-button-file"
                            type="file"
                            {...inputProps}
                            {...props}
                        />				
                    </Button>
                    <span>{this.state.fileName ? '  File: ' + this.state.fileName : ''}</span>
                    {touched && error && <p className="help is-error">{error}</p>}
                </label>
            </div>
        )
    }
}

export default CustomFileUpload