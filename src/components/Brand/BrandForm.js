import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { TextField, Button, CardActions, CircularProgress, Fade, withStyles } from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import RegularCard from '../Cards/RegularCard.js'
import CustomFieldUpload from '../CustomInput/CustomFileUpload'
import { brand } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import Select from 'react-select'
import {required} from '../Validate/Validate'

export const fields = [ 'id', 'title', 'description', 'image', 'stores' ]
const renderInput = ({ meta: { touched, error } = {}, input: { ...inputProps }, ...props }) => (
	<TextField
		helperText={touched ? error : ''}
		error={touched ? error === undefined ? false : true : false}
		{...inputProps}
		{...props}
		fullWidth
	/>
)

class BrandForm extends Component {
	submit(values) {
		values.id = this.props.initialValues.id
		if(this.props.match.params.id){
		if(this.props.record.image && !values.image ){
			values.image = this.props.record.image
		}
		this.props.dispatch(brand.save(values))
		}else{
		this.props.dispatch(brand.save(values))
		}
	}
	componentDidMount() {
		this.props.dispatch(brand.getStores())
		if (this.props.match.params.id) {
			this.props.dispatch(brand.get(this.props.match.params.id))
		} else {
			this.props.dispatch(brand.addNew())
		}
	}
	hideAlert(event) {
		this.props.dispatch(brand.hideAlert())
		this.props.history.push('/brands')
	}
	render() {
		const { record, classes, handleSubmit, saving, saved, errorMessage, stores } = this.props
		const title =
			(this.props.match.params.id ? 'Edit ' : 'Add new') + (record !== null && record.title ? record.title : '')
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{ zIndex: this.props.fetching ? 10 : 0 }}>
						<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade>
				<RegularCard
					cardTitle={title}
					content={
						<form onSubmit={handleSubmit(this.submit.bind(this))}>
							<div>
								<Field
									component={CustomFieldUpload}
									file={record && record.image ? record.image : null}
									isImage={true}
									name="image"
									label="Image"
								/>
							</div>
							<div>
								<Field validate={required} name="title" component={renderInput} label="Title" margin="normal" />
							</div>
							<div>
								<Field
									multiline={true}
									rowsMax="4"
									name="description"
									margin="normal"
									component={renderInput}
									label="Description"
								/>
							</div>
							<div style={{marginTop :'16px'}}>
								<label>Stores</label>
								<Field
									name="stores"
									label="Stores"
									component={(props) => (
										<Select
											value={stores.filter(function(value){
												if(props.input.value.includes(value.value) || props.input.value.map(s => s.value).includes(value.value)){
													return value  
												}
												return null                  
											})}
											onChange={props.input.onChange}
											onBlur={() => {
												props.input.onBlur(props.input.value)
											}}
											options={stores}
											placeholder="Select stores"
											isMulti={true}
										/>
									)}
								/>
							</div>

							<CardActions className={classes.actionSave}>
								<Button type="submit" className={classes.btnSave} variant="raised" color="primary" disabled={this.props.fetching}>
									{saving ? <CircularProgress color="secondary" size={20} /> : null}
									Submit
								</Button>
								<Button
									className ={classes.btnBack}
									variant="raised"
									//color="secondary"
									onClick={() => this.props.history.push('/brands')}
								>
									Back to list
								</Button>
							</CardActions>
						</form>
					}
					footer={''}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={saved || (errorMessage ? true : false)}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully saved!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	record: state.brand.record,
	fetching: state.brand.fetching,
	fetched: state.brand.fetched,
	saving: state.brand.saving,
	saved: state.brand.saved,
	errorMessage: state.brand.errorMessage,
	stores: state.brand.stores,
	fields,
	initialValues: {
		id: state.brand.record ? state.brand.record.id : null,
		title: state.brand.record ? state.brand.record.title : '',
		description: state.brand.record ? state.brand.record.description : '',
		stores: state.brand.record ? state.brand.record.stores : []
	}
})

const styles = (theme) => ({
	actionSave: {
		position:'relative',
		//top:'30px',
		left:'-15px',
		paddingTop:'70px'
	  },
	  btnSave:{
		order:1,
	  },
	  btnBack:{
		backgroudColor:'#e0e0e0',
		marginRight:'10px'
	  },
	overlay,
	loadingSpinner
})

export default withStyles(styles)(
	connect(mapStateToProps)(
		reduxForm({
			// a unique name for the form
			form: 'brand',
			enableReinitialize: true,
			//keepDirtyOnReinitialize: true,
			destroyOnUnmount: false, 
			forceUnregisterOnUnmount: true, 
		})(BrandForm)
	)
)
