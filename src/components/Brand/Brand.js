import React, { Component } from 'react'
import { connect } from 'react-redux'
import { brand } from '../../actions'
import CustomTable from '../Table/CustomTable'
import { Edit as EditIcon, Add as AddIcon, Delete as DeleteIcon } from '@material-ui/icons'
import { Button,  CircularProgress, Fade,  withStyles } from '@material-ui/core'
import { overlay, loadingSpinner } from '../../variables/styles' 
import SweetAlert from 'sweetalert2-react'
import debounce from 'lodash.debounce'
import { withLastLocation } from 'react-router-last-location'

class Brand extends Component {
	constructor(props) {
		super(props)
		this.state = {
			fields: [
				{
					id: 'image',
					numeric: false,
					image: true,
					disablePadding: false,
					width: 128,
					label: 'Image'
				},
				{
					id: 'title',
					numeric: false,
					disablePadding: false,
					label: 'Title'
				},
				{
					id: 'description',
					numeric: false,
					disablePadding: false,
					label: 'Description'
				}
			]
		}
	}
	componentDidMount() {		
		var page = 0
		var searchText = ''
		if(this.props.lastLocation){
			if(this.props.lastLocation.pathname.includes('brands')){
				page = this.props.page
				searchText = this.props.searchText
			}
		}
		this.props.dispatch(brand.count(searchText, this.props.orderBy, this.props.order))
		this._getBrand(page, this.props.rowsPerPage, searchText, this.props.orderBy, this.props.order)
	}
	_getBrand(page, rowsPerPage, searchText, orderBy, order) {
		this.props.dispatch(brand.list(page, rowsPerPage, searchText, orderBy, order))
	}
	handleSort = (orderBy, order) => {
		this._getBrand(this.props.page, this.props.rowsPerPage, this.props.searchText, orderBy, order)
	}
	handleChangePage = page => {
		this._getBrand(page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	search = debounce(query => {
		this.props.dispatch(brand.count(query))
		this._getBrand(this.props.page, this.props.rowsPerPage, query, this.props.orderBy, this.props.order)
	}, 1000)
	handleSearch = query => {
		this.props.dispatch(brand.updateSearchText(query))
		this.search(query)
	}
	handleChangeRowsPerPage = rowsPerPage => {
		this._getBrand(this.props.page, rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	handleDeleteAction = (id) => {
		this.props.dispatch(brand.delete(id))
	}
	hideAlert(event) {
		this.props.dispatch(brand.hideAlert())
		this.props.dispatch(brand.count(this.props.searchText, this.props.orderBy, this.props.order))
		this.table.resetHeader()
		this._getBrand(this.props.page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	render() {
		const { errorMessage, deleted, classes } = this.props
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
					<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade> 			
				<div style={{ textAlign: 'right' }}>
					<Button variant="raised" color="primary" onClick={() => this.props.history.push('/brands/add')}>
						<AddIcon /> Add New
					</Button>
				</div>
				<CustomTable
					onRef={(ref) => (this.table = ref)}
					total={this.props.total}
					history={this.props.history}
					tableHeaderColor="primary"
					tableHead={this.state.fields}
					searchText={this.props.searchText}
					page={this.props.page}
          			rowsPerPage={this.props.rowsPerPage}
					handleChangePage={this.handleChangePage}
					handleChangeRowsPerPage={this.handleChangeRowsPerPage}
					handleSearch={this.handleSearch}
					handleDelete={this.handleDeleteAction}
					handleSort={this.handleSort}
					data={this.props.list}
					editPath="/brands/edit/"
					actions={[
						{
							label: 'edit',
							icon: <EditIcon />,
							path: '/brands/edit/',
							has_id: true,
							color: 'primary'
						},
						{
							label: 'delete',
							icon: <DeleteIcon />,
							has_id: true,
							color: 'secondary'
						}
					]}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={deleted || errorMessage != null}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully deleted!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	fetching: state.brand.fetching,
	fetched: state.brand.fetched, 	
	total: state.brand.total,
	list: state.brand.list,
	page: state.brand.page,
	rowsPerPage: state.brand.rowsPerPage,
	searchText: state.brand.searchText,
	orderBy: state.brand.orderBy,
	order: state.brand.order,
	deleted: state.brand.deleted,
	errorMessage: state.brand.errorMessage
})
const styles = theme => ({
	overlay,
	loadingSpinner
  })
export default withStyles(styles)(connect(mapStateToProps)(withLastLocation(Brand)))
