import React from 'react'
import PropTypes from 'prop-types'
import styled, { keyframes } from 'styled-components'

const BounceAnimation = keyframes`
    0%{
      opacity 0;
      transform translateY(-2000px) rotate(-45deg);
    }      
    60%{
      opacity 1;
      transform translateY(30px) rotate(-45deg);
    }      
    80%{
      transform translateY(-10px) rotate(-45deg);
    }      
    100%{
      transform translateY(0) rotate(-45deg);
    }      
`
const PulseAnimation = keyframes`
    0%{
      transform scale(0.1, 0.1);
      opacity 0.0;
    }      
    50%{
      opacity 1;
    }
    100%{
      transform scale(1.2, 1.2)''
      opacity 0;
    }      
`
const Wrapper = styled.div`
	width: 30px;
	height: 30px;
	left: 50%;
	top: 50%;
	position: absolute;
	margin: -35px 0 0 -15px;
`
const Pin = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 50% 50% 50% 0;
  background: #e74c3c;
  position: absolute;
  animation: ${BounceAnimation} 1s both;
  animation-delay: .5s;  
  user-select: none;
  transform: translate(-50%, -50%) rotate(-45deg);
  cursor: ${(props) => (props.onClick ? 'pointer' : 'default')};
  &:after {
    content "";
    width 14px;
    height 14px;
    margin 8px 0 0 8px;
    background #c0392b;
    position absolute;
    border-radius 50%;
  }
`
const Pulse = styled.div`
  background rgba(0,0,0,0.2);
  border-radius 50%;
  height 14px;
  width 14px;
  position absolute;
  left 50%;
  top 50%;
  margin 14px 0px 0px -7px;
  transform rotateX(55deg);
  z-index -2;
  &:after {
    content "";
    border-radius 50%;
    height 40px;
    width 40px;
    position absolute;
    margin -12px 0 0 -13px;
    animation ${PulseAnimation} 1s ease-out;
    animation-iteration-count infinite;
    opacity 0.0;
    box-shadow 0 0 1px 2px #89849b;
    animation-delay 1.1s;
  }
`

const Marker = (props) => (
	<Wrapper alt={props.text} {...(props.onClick ? { onClick: props.onClick } : {})}>
		<Pin />
		<Pulse />
	</Wrapper>
)

Marker.defaultProps = {
	onClick: null
}

Marker.propTypes = {
	onClick: PropTypes.func,
	text: PropTypes.string.isRequired
}

export default Marker
