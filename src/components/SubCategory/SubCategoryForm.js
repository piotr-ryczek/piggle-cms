import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import {
  TextField,
  Button,
  CardActions,
  CircularProgress,
  Fade,
  withStyles
} from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import RegularCard from '../Cards/RegularCard.js'
import CustomFieldUpload from '../CustomInput/CustomFileUpload'
import { subCategory } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import Select from 'react-select'
import {required} from '../Validate/Validate'


export const fields = [
  'id',
  'title',
  'description',
  'category'
]
const renderInput = ({
  meta: { touched, error } = {},
  input: { ...inputProps },
  ...props
}) => (
  <TextField
    helperText={touched ? error : ''}
    error={touched ? (error === undefined ? false : true) : false}
    {...inputProps}
    {...props}
    fullWidth
  />
)
    
class SubCategoryForm extends Component {

  submit(values) {
    values.id = this.props.initialValues.id
    values.location = {lat: this.props.lat || '', lng: this.props.lng || ''}
		if(this.props.match.params.id){
      if(this.props.record.image && !values.image ){
        values.image = this.props.record.image
      }
	  	this.props.dispatch(subCategory.save(values))
		}else{
		  this.props.dispatch(subCategory.save(values))
		}
  }
  componentDidMount() {
    this.props.dispatch(subCategory.getCategories())
    if (this.props.match.params.id) {
      this.props.dispatch(subCategory.get(this.props.match.params.id))
    }
    else{
      this.props.dispatch(subCategory.addNew())
    }    
  }
  hideAlert(event) {
    this.props.dispatch(subCategory.hideAlert())
    this.props.history.push('/SubCategories')
  }
  render() {
    const { record, classes, handleSubmit, saving, saved, errorMessage, categories } = this.props
    const title =
      (this.props.match.params.id ? 'Edit ' : 'Add new') +
      (record !== null && record.title ? record.title : '')
    return (
      <div>
        <Fade in={this.props.fetching}>
          <div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
            <CircularProgress className={classes.loadingSpinner} />
          </div>
        </Fade>
        <RegularCard
          cardTitle={title}
          content={
            <form onSubmit={handleSubmit(this.submit.bind(this))}>
              <div>
								<Field
									component={CustomFieldUpload}
									file={record && record.image ? record.image : null}
									isImage={true}
									name="image"
									label="Image"
								/>
							</div>              
              <div>
                <Field
                  name='title'
                  component={renderInput}
                  label='Title'
                  margin="normal"
                  validate={required}
                />
              </div>
              <div>
                <Field
                  multiline={true}
                  rowsMax="4"
                  name="description"
                  margin="normal"
                  component={renderInput}
                  label="Description"
                  validate={required}
                />
              </div>              
              <div>
                <label>Category</label>
                <Field name="category"
                    validate={required}
                    label="Category"
                    component={props => {
                      const {meta: {touched, error}} = props  
                        return(
                        <div>
                        <Select
                            name="category"
                            value={categories.filter(({value}) => value === props.input.value || value === props.input.value.value)}
                            onChange={props.input.onChange}
                            onBlur={() => {
                                props.input.onBlur(props.input.value)
                            }}
                            options={categories}
                            placeholder="Select a category"
                            isMulti={false}
                            validate={required}
                        />
                        {touched && error && <p className="help is-error">{error}</p>}
                        </div>
                        )
                      }
                     }
                />
              </div>

              <CardActions className={classes.actionSave}>
                <Button
                  className={classes.btnSave}
                  type="submit"
                  variant="raised"
                  color="primary"
                  disabled={this.props.fetching}
                >
                { saving ? (
                    <CircularProgress color="secondary" size={20}/>
                  ) : (
                    null
                  )}
                  Submit
                </Button>
                <Button
                  className ={classes.btnBack}
                  variant="raised"
                 // color="secondary"
                  onClick={() => this.props.history.push('/SubCategories')}
                >
                  Back to list
                </Button>
              </CardActions>
            </form>
          }
          footer={''}
        />
        <SweetAlert
            type={errorMessage ? 'error' : 'success'}
            show={saved || (errorMessage ? true : false)}
            title={errorMessage ? "Error" : "Notice"}
            text={errorMessage ? errorMessage : "Successfully saved!"}
            onConfirm={this.hideAlert.bind(this)}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  record: state.subCategory.record,
  fetching: state.subCategory.fetching,
  fetched: state.subCategory.fetched,
  saving: state.subCategory.saving,
  saved: state.subCategory.saved,
  errorMessage: state.subCategory.errorMessage,
  lat: state.subCategory.lat,
  lng: state.subCategory.lng,
  categories: state.subCategory.categories,
  fields,
  initialValues: {
    id: state.subCategory.record ? state.subCategory.record.id : null,
    title: state.subCategory.record ? state.subCategory.record.title : '',
    description: state.subCategory.record ? state.subCategory.record.description : '',
    place: state.subCategory.record ? state.subCategory.record.place : (state.subCategory.suggest ? state.subCategory.suggest.place : ''),    
    line1: state.subCategory.record && state.subCategory.record.address ? state.subCategory.record.address.line1 : (state.subCategory.suggest ? state.subCategory.suggest.line1 : ''),
    line2: state.subCategory.record && state.subCategory.record.address ? state.subCategory.record.address.line2 : (state.subCategory.suggest ? state.subCategory.suggest.line2 : ''),
    line3: state.subCategory.record && state.subCategory.record.address ? state.subCategory.record.address.line3 : (state.subCategory.suggest ? state.subCategory.suggest.line3 : ''),
    city: state.subCategory.record && state.subCategory.record.address ? state.subCategory.record.address.city : (state.subCategory.suggest ? state.subCategory.suggest.city : ''),
    postcode: state.subCategory.record && state.subCategory.record.address ? state.subCategory.record.address.postcode : (state.subCategory.suggest ? state.subCategory.suggest.postcode : ''),
    category: (state.subCategory.record && state.subCategory.selectedCategory) ? state.subCategory.selectedCategory : null,
  }
})

const styles = theme => ({
  actionSave: {
    position:'relative',
    top:'30px',
    left:'-15px'
  },
  btnSave:{
    order:1,
  },
  btnBack:{
    backgroudColor:'#e0e0e0',
    marginRight:'10px'
  },
  overlay,
  loadingSpinner
})

export default withStyles(styles)(connect(mapStateToProps)(reduxForm({
  // a unique name for the form
  form: 'subCategory',
  enableReinitialize: true,
  //keepDirtyOnReinitialize: true,
  destroyOnUnmount: false, 
	forceUnregisterOnUnmount: true, 
})(SubCategoryForm)))
