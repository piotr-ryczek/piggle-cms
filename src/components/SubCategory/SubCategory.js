import React, { Component } from 'react'
import { connect } from 'react-redux'
import { subCategory } from '../../actions'
import CustomTable from '../Table/CustomTable'
import {
  Edit as EditIcon,
  Add as AddIcon,
  Delete as DeleteIcon
} from '@material-ui/icons'
import { Button,  CircularProgress,
  Fade,  withStyles } from '@material-ui/core'
import { overlay, loadingSpinner } from '../../variables/styles'  
import SweetAlert from 'sweetalert2-react'
import debounce from 'lodash.debounce'
import { withLastLocation } from 'react-router-last-location'

class SubCategory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fields: [
				{
					id: 'image',
					numeric: false,
					image: true,
					disablePadding: false,
					width: 128,
					label: 'Image'
				},        
        {
          id: 'title',
          numeric: false,
          disablePadding: false,
          label: 'Title'
        },  
        {
          id: 'category',
          numeric: false,
          disablePadding: false,
          label: 'Category'
        }         
      ]
    }
  }
  componentDidMount() {
    var page = 0
		var searchText = ''
		if(this.props.lastLocation){
			if(this.props.lastLocation.pathname.includes('SubCategories')){
				page = this.props.page
				searchText = this.props.searchText
			}
		}
    this.props.dispatch(subCategory.count(searchText, this.props.orderBy, this.props.order))
    this._getSubCategory(page, this.props.rowsPerPage, searchText, this.props.orderBy, this.props.order)
  }
  _getSubCategory(page, rowsPerPage, searchText, orderBy, order) {
    this.props.dispatch(subCategory.list(page, rowsPerPage, searchText, orderBy, order))
  }
  handleSort = (orderBy, order) => {
    this._getSubCategory(this.props.page, this.props.rowsPerPage, this.props.searchText, orderBy, order)
  }
  handleChangePage = page => {
    this._getSubCategory(page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
  }
	search = debounce(query => {
		this.props.dispatch(subCategory.count(query))
		this._getSubCategory(this.props.page, this.props.rowsPerPage, query, this.props.orderBy, this.props.order)
	}, 1000)
	handleSearch = query => {
		this.props.dispatch(subCategory.updateSearchText(query))
		this.search(query)
	}
  handleChangeRowsPerPage = rowsPerPage => {
    this._getSubCategory(this.props.page, rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
  }
  handleDeleteAction = id => {
    this.props.dispatch(subCategory.delete(id))
  }
  hideAlert(event) {
		this.props.dispatch(subCategory.hideAlert())
		this.props.dispatch(subCategory.count())
		this.table.resetHeader()
		this._getSubCategory(this.props.page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
  }
  render() {
    const { errorMessage, deleted, classes } = this.props
    return (
      <div>
        <Fade in={this.props.fetching}>
          <div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
            <CircularProgress className={classes.loadingSpinner} />
          </div>
        </Fade>      
        <div style={{ textAlign: 'right' }}>
          <Button
            variant="raised"
            color="primary"
            onClick={() => this.props.history.push('/SubCategories/add')}
          >
            <AddIcon /> Add New
          </Button>
        </div>
        <CustomTable
          onRef={ref => (this.table = ref)} 
          total={this.props.total}
          history={this.props.history}
          tableHeaderColor="primary"
          tableHead={this.state.fields}
          searchText={this.props.searchText}
          page={this.props.page}
          rowsPerPage={this.props.rowsPerPage}
          handleChangePage={this.handleChangePage}
          handleChangeRowsPerPage={this.handleChangeRowsPerPage}
          handleSearch={this.handleSearch}
          handleDelete={this.handleDeleteAction}
          handleSort={this.handleSort}
          data={this.props.list}
          editPath="/SubCategories/edit/"
          actions={[
            {
              label: 'edit',
              icon: <EditIcon />,
              path: '/SubCategories/edit/',
              has_id: true,
              color: 'primary'
            },
            {
              label: 'delete',
              icon: <DeleteIcon />,
              has_id: true,
              color: 'secondary'
            }
          ]}
        />
        <SweetAlert
          type={errorMessage ? 'error' : 'success'}
          show={deleted || errorMessage != null}
          title={errorMessage ? 'Error' : 'Notice'}
          text={errorMessage ? errorMessage : 'Successfully deleted!'}
          onConfirm={this.hideAlert.bind(this)}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  total: state.subCategory.total,
  fetching: state.subCategory.fetching,
  fetched: state.subCategory.fetched,
  list: state.subCategory.list,
  page: state.subCategory.page,
  rowsPerPage: state.subCategory.rowsPerPage,
  searchText: state.subCategory.searchText,
  orderBy: state.subCategory.orderBy,
  order: state.subCategory.order,
  deleted: state.subCategory.deleted,
  errorMessage: state.subCategory.errorMessage
})

const styles = theme => ({
  overlay,
  loadingSpinner
})

export default withStyles(styles)(connect(mapStateToProps)(withLastLocation(SubCategory)))
