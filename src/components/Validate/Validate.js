import React from 'react'
const isUrlValid = (userInput) => {
    let res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/g)
    if(res == null)
        return (false )
    else
        return (true)
};
const validateImage = (val) =>{
    if(val){
        let file = val
        if (file.name.endsWith('.jpg') || file.name.endsWith('.png')|| file.name.endsWith('.jpeg')) {
            return true
        }else{
            return false
        }
       
    }
    return true
}
export const renderError = ({meta: {touched, error}}) => touched && error ? <p className="help is-error">{error}</p> : false
export const required = value => value ? undefined : 'Mandatory'
export const mandatory = value => value ? undefined : 'Mandatory'
export const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)? 'Invalid email address': undefined
export const hyperlink = value =>  isUrlValid(value) ? undefined : 'Insert a valid URL'
export const confirmPass = value => value && (value.password !== value.confirmPassword) ? "Password mismatched": undefined
export const minLength = min => value => value && value.length < min ? `Must be ${min} characters or more` : undefined
export const minLength6 = minLength(6)
export const isImg = value =>  validateImage(value) ? undefined : 'Scan file must be an .jpg,.jpeg or .png file'