import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm ,getFormValues} from 'redux-form'
import {
  TextField,
  Button,
  CardActions,
  CircularProgress,
  Fade,
  Grid,
  withStyles
} from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import CustomFieldUpload from '../CustomInput/CustomFileUpload'
import RegularCard from '../Cards/RegularCard.js'
import { offer } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import Datetime from 'react-datetime'
import Select from 'react-select'
import {required,isImg} from '../Validate/Validate'
import { store as storeReact } from '../../store'

export const fields = [
  'id',
  'title',
  'subtitle',
  'image',
  'voucherTitle',
  'voucherCode',
  'voucherInstructions',
  'voucherAdditionalInstructions',
  'shareUrl',
  'startDate',
  'enDate',
  'brand',
  'stores',
  'subCategories',
  'facilities'
]
const renderInput = ({
  meta: { touched, error } = {},
  input: { ...inputProps },
  ...props
}) => (
  <TextField
    helperText={touched ? error : ''}
    error={touched ? (error === undefined ? false : true) : false}
    {...inputProps}
    {...props}
    fullWidth
  />
)
const validate = values => {
	const errors = {}
	if(values.endDate && values.startDate) {
     if(Date.parse(values.startDate) > Date.parse(values.endDate)){
         errors.startDate = 'Start date must be smaller than End date' 
     }
	}
	return errors
}
class OfferForm extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentInstance: this
    }
  }
  submit(values) {    
     values.id = this.props.initialValues.id
    // // values.brand = values.brand || this.props.record.brand
    // // values.stores = values.stores || this.props.record.stores
    // // values.subCategories = values.subCategories || this.props.record.subCategories
    // // values.facilities = values.facilities || this.props.record.facilities
    // this.props.dispatch(offer.save(values))
    if(this.props.match.params.id){
      if(this.props.record.image && !values.image ){
        values.image = this.props.record.image
      }
      if(this.props.record.voucherImage && !values.voucherImage ){
        values.voucherImage = this.props.record.voucherImage
      }
      console.log(values)
      this.props.dispatch(offer.save(values))
    }else{
      this.props.dispatch(offer.save(values))
    }
  }
  componentDidMount() {
    if(!this.props.fetchedBrands || !this.props.fetchedStores || !this.props.fetchedSubCategories || !this.props.fetchedFacilities){
      this.props.dispatch(offer.getBrands())
      this.props.dispatch(offer.getStores())
      this.props.dispatch(offer.getSubCategories())
      this.props.dispatch(offer.getFacilities())
    }
    if(!this.props.loadedOffer){
      this.getOffer()
    }
  }
  componentWillUnmount(){
    this.props.dispatch(offer.hideAlert())
  }
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (prevProps.record !== this.props.record) {
  //       if(this.props.record && this.props.record.brand){
  //         // this.changeBrand(this.props.record.brand)
  //       }
  //   }
  // }
  getOffer(){
    if (this.props.match.params.id) {
      if(this.props.record === null){
        this.props.dispatch(offer.get(this.props.match.params.id))
      }
    }
    else{
      this.props.dispatch(offer.addNew())
    }
  }
  changeBrand(brand, props) {
    props.input.onChange(brand)
    // //this.setState({ value: brand });    
    if(brand.value){
    //   // var recordData = this.props.record
    //   // if(!recordData){
    //   //   let values = getFormValues('offer')(storeReact.getState())
    //   //   recordData = values
    //   // }
      var recordData = getFormValues('offer')(storeReact.getState())
      this.props.dispatch(offer.updateOfferForm(recordData))
    //   this.props.dispatch(offer.updateStores(brand.value, recordData))
    }
  }
  hideAlert(event) {
    this.props.dispatch(offer.hideAlert())
    this.props.history.push('/offers')
  }
  render() {
    const { record , classes, handleSubmit, saving, saved, errorMessage, brands, stores, subCategories, facilities } = this.props
    const title =
      (this.props.match.params.id ? 'Edit ' : 'Add new') +
      (record !== null && record.title ? record.title : '')
    return (
      <div>
        <Fade in={this.props.fetching}>
          <div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
            <CircularProgress className={classes.loadingSpinner} />
          </div>
        </Fade>
        <RegularCard
          cardTitle={title}
          content={
            <form onSubmit={handleSubmit(this.submit.bind(this))}>              
              <div>
                <Field
                    component={CustomFieldUpload}
                    file={record && record.image ? record.image : null}
                    isImage={true}
                    name="image"
                    validate={record && record.image ? '' : isImg}
                    label="Image (width:400 - 450px Height:220px)"
                />
              </div>
              <div>
                <Field
                  name='title'
                  component={renderInput}
                  label='Title'
                  margin="normal"
                  validate={required}
                />
              </div>
              <div>
                  <Field
                    multiline={true}
                    rowsMax="4"
                    name="subtitle"
                    margin="normal"
                    component={renderInput}
                    label="Subtitle"
                  />
              </div>
              <div>
                <Field
                  name='voucherTitle'
                  component={renderInput}
                  label='Voucher Title'
                  margin="normal"
                />
              </div>
              <div style={{marginLeft:'-5px',marginTop:'-4px',marginBottom:'-5px'}}>
              <Field
                    component={CustomFieldUpload}
                    file={record && record.voucherImage ? record.voucherImage : null}
                    isImage={true}
                    name="voucherImage"
                    label="Voucher Image"
                    validate={record && record.image ? '' : isImg}
                />
              </div>
              <div>
                <Field
                  name='voucherInstructions'
                  multiline={true}
                  rowsMax="4"
                  component={renderInput}
                  label='Voucher instructions '
                  margin="normal"
                />
              </div>
              <div>
                <Field
                  name='voucherAdditionalInstructions'
                  multiline={true}
                  rowsMax="4"
                  component={renderInput}
                  label='Voucher additional instructions'
                  margin="normal"
                />
              </div>
              <div>
                <Field
                  name='shareUrl'
                  component={renderInput}
                  label='Share Url'
                  margin="normal"
                />
              </div>
              <Grid container spacing={24}>
                <Grid item xs={6}>
                  <div style={{marginTop:'-20px'}}>
                    <label style={{position:'relative',top:'10px'}}>Start date</label>
                    <Field name="startDate"
                        component={props =>{
                           const {meta: {touched, error}} = props  
                            return(
                              <div>
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  name="startDate"
                                  value={props.input.value}
                                  onChange={props.input.onChange}
                                  onBlur={() => {
                                      props.input.onBlur(props.input.value)
                                  }}
                                />
                                 {touched && error && <p className="help is-error">{error}</p>}
                              </div>
                            )
                          }
                        }
                    />
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div style={{marginTop:'-20px'}}>
                    <label style={{position:'relative',top:'10px'}}>End date</label>
                    <Field name="endDate"
                        component={props =>
                          <Datetime
                            inputProps={{readOnly: true}}
                            name="endDate"
                            value={props.input.value}
                            onChange={props.input.onChange}
                            onBlur={() => {
                                props.input.onBlur(props.input.value)
                            }}

                          />
                        }
                    />
                  </div>
                </Grid>
              </Grid>

              <div style={{marginTop:'25px'}}>
                <label>Brand</label>
                <Field name="brand"
                    label="Brand"
                    component={props =>
                        <Select
                            // value={this.state.value}           
                            // defaultValue={record ? brands.filter(({value}) => value === record.brand) : null}
                            value={brands.filter(({value}) => value === props.input.value || value === props.input.value.value)}
                            onChange={(value) => this.changeBrand(value, props)}
                            onBlur={() => props.input.onBlur(props.input.value)}
                            options={brands}
                            placeholder="Select a brand"
                            isMulti={false}
                        />
                    }
                />
              </div>
              <div style={{marginTop:'25px'}}>
								<label>{!this.props.disable ? "Stores": "Stores (Please choose brand)"}</label>
								<Field
									name="stores"
									label="Stores"
									component={(props) => (
										<Select
                      disabled={this.props.disable}                      
											// defaultValue={record ? stores.filter(({value}) => value === record.stores || record.stores.includes(value) ) : null}
											// value={stores.filter(({value}) => props.input.value.map(s => s.value).includes(value))}
                      value={stores.filter(function(value){
                        if(props.input.value.includes(value.value) || props.input.value.map(s => s.value).includes(value.value)){
                          return value  
                        }        
                        return null                
                      })}
                      onChange={(value) => props.input.onChange(value)}
                      onBlur={() => props.input.onBlur(props.input.value)}
											options={stores ? (record ? stores.filter(({brand}) => brand === record.brand || ((typeof record.brand === 'object' && record.brand) ? (brand === record.brand.value ? true :false) : false)) : []) : []}
											placeholder="Select stores"
											isMulti={true}
										/>
									)}
								/>
							</div>
              <div style={{marginTop:'25px'}}>
								<label>Sub-categories</label>
								<Field
									name="subCategories"
									label="Sub Categories"
									component={(props) => (
										<Select
                      value={subCategories.filter(function(value){
                        if(props.input.value.includes(value.value) || props.input.value.map(s => s.value).includes(value.value)){
                          return value  
                        }      
                        return null                  
                      })}
                      onChange={(value) => props.input.onChange(value)}
                      onBlur={() => props.input.onBlur(props.input.value)}
											options={subCategories}
											placeholder="Select subCategories"
											isMulti={true}
										/>
									)}
								/>
							</div>
              <div style={{marginTop:'25px'}}>
								<label>Facilities</label>
								<Field
									name="facilities"
									label="Facilities"
									component={(props) => (
										<Select
                      value={facilities.filter(function(value){
                        if(props.input.value.includes(value.value) || props.input.value.map(s => s.value).includes(value.value)){
                          return value  
                        }      
                        return null                  
                      })}
                      onChange={(value) => props.input.onChange(value)}
                      onBlur={() => props.input.onBlur(props.input.value)}
											options={facilities}
											placeholder="Select Facilities"
											isMulti={true}
										/>
									)}
								/>
							</div>

              <CardActions className={classes.actionSave}>
                <Button
                  type="submit"
                  variant="raised"
                  color="primary"
                  disabled={this.props.fetching}
                  className={classes.btnSave}
                >
                  { saving ? (
                    <CircularProgress color="secondary" size={20}/>
                  ) : (
                    null
                  )}                  
                  Submit
                </Button>
                <Button
                  className ={classes.btnBack}
                  variant="raised"
                //  color="secondary"
                  onClick={() => this.props.history.push('/offers')}
                >
                  Back to list
                </Button>
              </CardActions>
            </form>
          }
          footer={''}
        />
        <SweetAlert
            type={errorMessage ? 'error' : 'success'}
            show={saved || (errorMessage ? true : false)}
            title={errorMessage ? "Error" : "Notice"}
            text={errorMessage ? errorMessage : "Successfully saved!"}
            onConfirm={this.hideAlert.bind(this)}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  record: state.offer.record,
  fetching: state.offer.fetching,
  fetched: state.offer.fetched,
  saving: state.offer.saving,
  saved: state.offer.saved,
  errorMessage: state.offer.errorMessage,
  brands: state.offer.brands,
  stores: state.offer.stores,
  subCategories: state.offer.subCategories,
  facilities: state.offer.facilities,
  disable:state.offer.disable,
  fetchedBrands: state.offer.fetchedBrands,
  fetchedStores: state.offer.fetchedStores,
  fetchedSubCategories: state.offer.fetchedSubCategories,
  fetchedFacilities: state.offer.fetchedFacilities,  
  loadingOffer: state.offer.loadingOffer,  
  loadedOffer: state.offer.loadedOffer,  
  fields,
  initialValues: {
    id: state.offer.record ? state.offer.record.id : null,
    title: state.offer.record ? state.offer.record.title : '',
    subtitle: state.offer.record ? state.offer.record.subtitle : '',
    shareUrl: state.offer.record ? state.offer.record.shareUrl : '',
    voucherTitle: state.offer.record ? state.offer.record.voucherTitle : '',
    voucherCode: state.offer.record ? state.offer.record.voucherCode : '',
    voucherInstructions: state.offer.record ? state.offer.record.voucherInstructions : '',
    voucherAdditionalInstructions: state.offer.record ? state.offer.record.voucherAdditionalInstructions : '',
    startDate: state.offer.record ? state.offer.record.startDate : null,
    endDate: state.offer.record ? state.offer.record.endDate : null,
    brand: state.offer.record ? state.offer.record.brand : null,
    stores: state.offer.record ? state.offer.record.stores : [],
    subCategories: state.offer.record ? state.offer.record.subCategories : [],
    facilities: state.offer.record ? state.offer.record.facilities : [],
  }
})

const styles = theme => ({
  actionSave: {
    position:'relative',
   // top:'30px',
    left:'-15px',
    marginTop:'63px',
  },
  btnSave:{
    order:1,
  },
  btnBack:{
    backgroudColor:'#e0e0e0',
    marginRight:'10px'
  },
  overlay,
  loadingSpinner
})

export default withStyles(styles)(connect(mapStateToProps)(reduxForm({
  // a unique name for the form
  form: 'offer',
  enableReinitialize: true,
  //keepDirtyOnReinitialize: true,
  destroyOnUnmount: false, 
  forceUnregisterOnUnmount: true, 
  validate
})(OfferForm)))
