import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { TextField, Button, CardActions, CircularProgress, Fade, withStyles } from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import RegularCard from '../Cards/RegularCard.js'
import CustomFieldUpload from '../CustomInput/CustomFileUpload'
import { category } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import Select from 'react-select'
import {required} from '../Validate/Validate'

export const fields = [ 'id', 'title', 'description', 'image', 'SubCategories' ]
const renderInput = ({ meta: { touched, error } = {}, input: { ...inputProps }, ...props }) => (
	<TextField
		helperText={touched ? error : ''}
		error={touched ? error === undefined ? false : true : false}
		{...inputProps}
		{...props}
		fullWidth
	/>
)

class CategoryForm extends Component {
	submit(values) {
		values.id = this.props.initialValues.id
		if(this.props.match.params.id){
		if(this.props.record.image && !values.image ){
			values.image = this.props.record.image
		}
		this.props.dispatch(category.save(values))
		}else{
		this.props.dispatch(category.save(values))
		}
	}
	componentDidMount() {
		this.props.dispatch(category.getSubCategories())
		if (this.props.match.params.id) {
			this.props.dispatch(category.get(this.props.match.params.id))
		} else {
			this.props.dispatch(category.addNew())
		}
	}
	hideAlert(event) {
		this.props.dispatch(category.hideAlert())
		this.props.history.push('/categories')
	}
	render() {
		const { record, classes, handleSubmit, saving, saved, errorMessage, subCategories } = this.props
		const title =
			(this.props.match.params.id ? 'Edit ' : 'Add new') + (record !== null && record.title ? record.title : '')
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{ zIndex: this.props.fetching ? 10 : 0 }}>
						<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade>
				<RegularCard
					cardTitle={title}
					content={
						<form onSubmit={handleSubmit(this.submit.bind(this))}>
							<div>
								<Field
									component={CustomFieldUpload}
									file={record && record.image ? record.image : null}
									isImage={true}
									name="image"
									label="Image"
								/>
							</div>
							<div>
								<Field name="title" validate={required} component={renderInput} label="Title" margin="normal" />
							</div>
							<div>
								<Field
									multiline={true}
									rowsMax="4"
									name="description"
									margin="normal"
									component={renderInput}
									label="Description"
									validate={required}
								/>
							</div>
							<div style={{marginTop :'16px'}} >
								<label>Sub-categories</label>
								<Field
									name="subCategories"
									label="Sub Categories"
									component={(props) => (
										<Select
											isDisabled={true}
											value={subCategories.filter(function(value){
												if(props.input.value.includes(value.value) || props.input.value.map(s => s.value).includes(value.value)){
													return value  
												}
												return null										                          
											})}
											onChange={props.input.onChange}
											onBlur={() => {
												props.input.onBlur(props.input.value)
											}}
											options={subCategories}
											placeholder=""
											isMulti={true}
										/>
									)}
								/>
							</div>

							<CardActions className={classes.actionSave}>
								<Button type="submit"  className={classes.btnSave} variant="raised" color="primary" disabled={this.props.fetching}>
									{saving ? <CircularProgress color="secondary" size={20} /> : null}
									Submit
								</Button>
								<Button
									className ={classes.btnBack}
									variant="raised"
									//color="secondary"
									onClick={() => this.props.history.push('/categories')}
								>
									Back to list
								</Button>
							</CardActions>
						</form>
					}
					footer={''}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={saved || (errorMessage ? true : false)}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully saved!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	record: state.category.record,
	fetching: state.category.fetching,
	fetched: state.category.fetched,
	saving: state.category.saving,
	saved: state.category.saved,
	errorMessage: state.category.errorMessage,
	subCategories: state.category.subCategories,
	fields,
	initialValues: {
		id: state.category.record ? state.category.record.id : null,
		title: state.category.record ? state.category.record.title : '',
		description: state.category.record ? state.category.record.description : '',
		subCategories: state.category.record ? state.category.record.subCategories : []
	}
})

const styles = (theme) => ({
	actionSave: {
		position:'relative',
		//top:'62px',
		left:'-15px',
		paddingTop:'70px'
	  },
	  btnSave:{
		order:1,
	  },
	  btnBack:{
		backgroudColor:'#e0e0e0',
		marginRight:'10px'
	  },
	overlay,
	loadingSpinner
})

export default withStyles(styles)(
	connect(mapStateToProps)(
		reduxForm({
			// a unique name for the form
			form: 'category',
			enableReinitialize: true,
			//keepDirtyOnReinitialize: true,
			destroyOnUnmount: false, 
			forceUnregisterOnUnmount: true, 
		})(CategoryForm)
	)
)
