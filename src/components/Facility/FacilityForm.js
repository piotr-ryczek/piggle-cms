import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { TextField, Button, CardActions, CircularProgress, Fade, withStyles } from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import CustomFieldUpload from '../CustomInput/CustomFileUpload'
import RegularCard from '../Cards/RegularCard.js'
import { facility } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import {required} from '../Validate/Validate'
export const fields = [ 'id', 'title', 'icon', 'deactive_icon' ]
const renderInput = ({ meta: { touched, error } = {}, input: { ...inputProps }, ...props }) => (
	<TextField
		helperText={touched ? error : ''}
		error={touched ? error === undefined ? false : true : false}
		{...inputProps}
		{...props}
		fullWidth
	/>
)
class FacilityForm extends Component {
	submit(values) {
		values.id = this.props.initialValues.id
		//this.props.dispatch(facility.save(values))
		if(this.props.match.params.id){
		if(this.props.record.icon && !values.icon ){
			values.icon = this.props.record.icon
		}
		if(this.props.record.deactiveIcon && !values.deactiveIcon ){
			values.deactiveIcon = this.props.record.deactiveIcon
		}
		//console.log(values)
		this.props.dispatch(facility.save(values))
		}else{
		this.props.dispatch(facility.save(values))
		}
	}
	componentDidMount() {
		if (this.props.match.params.id) {
			this.props.dispatch(facility.get(this.props.match.params.id))
		}else {
			this.props.dispatch(facility.addNew())
		}
	}
	hideAlert(event) {
		this.props.dispatch(facility.hideAlert())
		this.props.history.push('/facilities')
	}
	render() {
		const { record, classes, handleSubmit, saving, saved, errorMessage } = this.props
		const title =
			(this.props.match.params.id ? 'Edit ' : 'Add new') + (record !== null && record.title ? record.title : '')
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{ zIndex: this.props.fetching ? 10 : 0 }}>
						<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade>
				<RegularCard
					cardTitle={title}
					content={
						<form onSubmit={handleSubmit(this.submit.bind(this))}>
							<div>
								<Field
									component={CustomFieldUpload}
									file={record && record.icon ? record.icon : null}
									isImage={true}
									name="icon"
									label="Image"
									validate={record && record.icon ? '' : required}
								/>
							</div>
							<div>
								<Field
									component={CustomFieldUpload}
									file={record && record.deactiveIcon ? record.deactiveIcon : null}
									isImage={true}
									name="deactiveIcon"
									label="Deactivate Icon"
									validate={record && record.deactiveIcon ? '' :required}
								/>
							</div>
							<div>
								<Field name="title" validate={required} component={renderInput} label="Title" margin="normal" />
							</div>

							<CardActions className={classes.actionSave}>
								<Button type="submit" className={classes.btnSave} variant="raised" color="primary" disabled={this.props.fetching}>
									{saving ? <CircularProgress color="secondary" size={20} /> : null}
									Submit
								</Button>
								<Button
									className ={classes.btnBack}
									variant="raised"
									//color="secondary"
									onClick={() => this.props.history.push('/facilities')}
								>
									Back to list
								</Button>
							</CardActions>
						</form>
					}
					footer={''}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={saved || (errorMessage ? true : false)}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully saved!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	record: state.facility.record,
	fetching: state.facility.fetching,
	fetched: state.facility.fetched,
	saving: state.facility.saving,
	saved: state.facility.saved,
	errorMessage: state.facility.errorMessage,
	fields,
	initialValues: {
		id: state.facility.record ? state.facility.record.id : null,
		title: state.facility.record ? state.facility.record.title : ''
	}
})

const styles = (theme) => ({
	actionSave: {
		position:'relative',
		top:'30px',
		left:'-15px'
	  },
	  btnSave:{
		order:1,
	  },
	  btnBack:{
		backgroudColor:'#e0e0e0',
		marginRight:'10px'
	  },
	overlay,
	loadingSpinner
})

export default withStyles(styles)(
	connect(mapStateToProps)(
		reduxForm({
			// a unique name for the form
			form: 'facility',
			enableReinitialize: true,
			//keepDirtyOnReinitialize: true,
			destroyOnUnmount: false, 
			forceUnregisterOnUnmount: true, 
		})(FacilityForm)
	)
)
