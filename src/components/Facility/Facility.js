import React, { Component } from 'react'
import { connect } from 'react-redux'
import { facility } from '../../actions'
import CustomTable from '../Table/CustomTable'
import { Edit as EditIcon, Add as AddIcon, Delete as DeleteIcon } from '@material-ui/icons'
import { Button,  CircularProgress, Fade,  withStyles } from '@material-ui/core'
import { overlay, loadingSpinner } from '../../variables/styles'  
import SweetAlert from 'sweetalert2-react'
import debounce from 'lodash.debounce'
import { withLastLocation } from 'react-router-last-location'

class Facility extends Component {
	constructor(props) {
		super(props)
		this.state = {
			fields: [
				{
					id: 'icon',
					numeric: false,
					image: true,
					disablePadding: false,
					width: 128,
					label: 'Icon'
				},
				{
					id: 'title',
					numeric: false,
					disablePadding: false,
					label: 'Title'
				}
			]
		}
	}
	componentDidMount() {		
		var page = 0
		var searchText = ''
		if(this.props.lastLocation){
			if(this.props.lastLocation.pathname.includes('facilities')){
				page = this.props.page
				searchText = this.props.searchText
			}
		}
		this.props.dispatch(facility.count(searchText, this.props.orderBy, this.props.order))
		this._getFacility(page, this.props.rowsPerPage, searchText, this.props.orderBy, this.props.order)
	}
	_getFacility(page, rowsPerPage, searchText, orderBy, order) {
		this.props.dispatch(facility.list(page, rowsPerPage, searchText, orderBy, order))
	}
	handleSort = (orderBy, order) => {
		this._getFacility(this.props.page, this.props.rowsPerPage, this.props.searchText, orderBy, order)
	}
	handleChangePage = page => {
		this._getFacility(page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	search = debounce(query => {
		this.props.dispatch(facility.count(query))
		this._getFacility(this.props.page, this.props.rowsPerPage, query, this.props.orderBy, this.props.order)
	}, 1000)
	handleSearch = query => {
		this.props.dispatch(facility.updateSearchText(query))
		this.search(query)
	}
	handleChangeRowsPerPage = rowsPerPage => {
		this._getFacility(this.props.page, rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	handleDeleteAction = (id) => {
		this.props.dispatch(facility.delete(id))
	}
	hideAlert(event) {
		this.props.dispatch(facility.hideAlert())
		this.props.dispatch(facility.count())
		this.table.resetHeader()
		this._getFacility(this.props.page, this.props.rowsPerPage, this.props.searchText, this.props.orderBy, this.props.order)
	}
	render() {
		const { errorMessage, deleted, classes } = this.props
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{zIndex: this.props.fetching ? 10 : 0}}>
					<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade> 			
				<div style={{ textAlign: 'right' }}>
					<Button variant="raised" color="primary" onClick={() => this.props.history.push('/facilities/add')}>
						<AddIcon /> Add New
					</Button>
				</div>
				<CustomTable
					onRef={(ref) => (this.table = ref)}
					total={this.props.total}
					history={this.props.history}
					tableHeaderColor="primary"
					tableHead={this.state.fields}
					searchText={this.props.searchText}
					page={this.props.page}
          			rowsPerPage={this.props.rowsPerPage}
					handleChangePage={this.handleChangePage}
					handleChangeRowsPerPage={this.handleChangeRowsPerPage}
					handleSearch={this.handleSearch}
					handleDelete={this.handleDeleteAction}
					handleSort={this.handleSort}
					data={this.props.list}
					editPath="/facilities/edit/"
					actions={[
						{
							label: 'edit',
							icon: <EditIcon />,
							path: '/facilities/edit/',
							has_id: true,
							color: 'primary'
						},
						{
							label: 'delete',
							icon: <DeleteIcon />,
							has_id: true,
							color: 'secondary'
						}
					]}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={deleted || errorMessage != null}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully deleted!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	fetching: state.facility.fetching,
	fetched: state.facility.fetched, 	
	total: state.facility.total,
	list: state.facility.list,
	page: state.facility.page,
	rowsPerPage: state.facility.rowsPerPage,
	searchText: state.facility.searchText,
	orderBy: state.facility.orderBy,
	order: state.facility.order,
	deleted: state.facility.deleted,
	errorMessage: state.facility.errorMessage
})
const styles = theme => ({
	overlay,
	loadingSpinner
  })
export default withStyles(styles)(connect(mapStateToProps)(withLastLocation(Facility)))
