import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { TextField, Button, CardActions, CircularProgress, Fade, withStyles } from '@material-ui/core'
import SweetAlert from 'sweetalert2-react'
import RegularCard from '../Cards/RegularCard.js'
import { childType } from '../../actions'
import { overlay, loadingSpinner } from '../../variables/styles'
import {required} from '../Validate/Validate'
export const fields = [ 'id', 'title' ]
const renderInput = ({ meta: { touched, error } = {}, input: { ...inputProps }, ...props }) => (
	<TextField
		helperText={touched ? error : ''}
		error={touched ? error === undefined ? false : true : false}
		{...inputProps}
		{...props}
		fullWidth
	/>
)
class ChildTypesForm extends Component {
	submit(values) {
		values.id = this.props.initialValues.id
		this.props.dispatch(childType.save(values))
	}
	componentDidMount() {
		if (this.props.match.params.id) {
			this.props.dispatch(childType.get(this.props.match.params.id))
		}else {
			this.props.dispatch(childType.addNew())
		}
	}
	hideAlert(event) {
		this.props.dispatch(childType.hideAlert())
		this.props.history.push('/childType')
	}
	render() {
		const { record, handleSubmit, classes, saving, saved, errorMessage } = this.props
		const title =
			(this.props.match.params.id ? 'Edit ' : 'Add new') + (record !== null && record.title ? record.title : '')
		return (
			<div>
				<Fade in={this.props.fetching}>
					<div className={classes.overlay} style={{ zIndex: this.props.fetching ? 10 : 0 }}>
						<CircularProgress className={classes.loadingSpinner} />
					</div>
				</Fade>
				<RegularCard
					cardTitle={title}
					content={
						<form onSubmit={handleSubmit(this.submit.bind(this))}>
							<div>
								<Field validate={required} name="title" component={renderInput} label="Title" />
							</div>

							<CardActions className={classes.actionSave}>
								<Button type="submit" className={classes.btnSave} variant="raised" color="primary" disabled={this.props.fetching}>
									{saving ? <CircularProgress color="secondary" size={20} /> : null}
									Submit
								</Button>
								<Button
									 className ={classes.btnBack}
									variant="raised"
									//color="secondary"
									onClick={() => this.props.history.push('/childType')}
								>
									Back to list
								</Button>
							</CardActions>
						</form>
					}
					footer={''}
				/>
				<SweetAlert
					type={errorMessage ? 'error' : 'success'}
					show={saved || (errorMessage ? true : false)}
					title={errorMessage ? 'Error' : 'Notice'}
					text={errorMessage ? errorMessage : 'Successfully saved!'}
					onConfirm={this.hideAlert.bind(this)}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	record: state.childType.record,
	regions: state.childType.regions,
	fetching: state.childType.fetching,
	fetched: state.childType.fetched,
	saving: state.childType.saving,
	saved: state.childType.saved,
	errorMessage: state.childType.errorMessage,
	fields,
	initialValues: {
		id: state.childType.record ? state.childType.record.id : null,
		title: state.childType.record ? state.childType.record.title : ''
	}
})

const styles = (theme) => ({
	actionSave: {
		position:'relative',
		top:'30px',
		left:'-15px'
	  },
	  btnSave:{
		order:1,
	  },
	  btnBack:{
		backgroudColor:'#e0e0e0',
		marginRight:'10px'
	  },
	overlay,
	loadingSpinner
})

export default withStyles(styles)(
	connect(mapStateToProps)(
		reduxForm({
			// a unique name for the form
			form: 'facility',
			enableReinitialize: true,
			//keepDirtyOnReinitialize: true,
			destroyOnUnmount: false, 
			forceUnregisterOnUnmount: true,
		})(ChildTypesForm)
	)
)
