import React from "react"
import PropTypes from "prop-types"
import { connect } from 'react-redux'
import { Switch, Route, Redirect } from "react-router-dom"
import PerfectScrollbar from "perfect-scrollbar"
import { withStyles } from '@material-ui/core'
import { Header, Footer, Sidebar } from "../components"
import appRoutes from "../routes/app"
import appStyle from "../variables/styles/appStyle.jsx"
import logo from "../assets/img/logo_white.svg"
import DashboardPage from "../components/Home"
import LoginPage from "../components/Login"
import { auth ,db } from "../api/firebase"
import CircularProgress from '@material-ui/core/CircularProgress'

import "perfect-scrollbar/css/perfect-scrollbar.css"

const RouteWithSubRoutes = route => (  
  <div>
      <Route exact path={route.path} component={route.component} />
      {route.routes.map(sub => {
        return <Route exact path={sub.path} component={sub.component} key={sub.action} />
      })}
  </div>
)

const switchRoutes = (
  <Switch>
    <Route path="/login" component={LoginPage} key="login"/>
    <Route path="/dashboard" component={DashboardPage} key="dashboard"/>   
    {appRoutes.map((prop, key) => {
      if (prop.redirect)        
        return <Redirect from={prop.path} to={prop.to} key={key} />      
      if(prop.routes !== undefined){        
        return <RouteWithSubRoutes key={key} {...prop} />
      }
      else{
        return <Route path={prop.path} component={prop.component} key={key} />
      }      
    })}
  </Switch>
)
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mobileOpen: false,
      loadingUser: true,
    }
  }

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen })
  }
  componentDidMount() {       
    this.checkCurrentUser() 
    if(this.props.isLoggedIn){
      if(navigator.platform.indexOf('Win') > -1){
        // eslint-disable-next-line
        const ps = new PerfectScrollbar(this.refs.mainPanel)
      }
    }

  }
  componentDidUpdate(){
    let user = auth.currentUser
    if(!user){
      if(this.props.location.pathname !== "/login"){
        this.props.history.push('/login')
      }
    }
  }

  checkCurrentUser(){
    const { currentUser } = auth;

    if (currentUser) {
      this.setState({ loadingUser: false });
    } else {
      auth.onAuthStateChanged(async user => {
        this.setState({ loadingUser: false });

        if (user) {
          const { uid } = user;

          const adminRef = await db.collection('administrators').doc(uid).get();
        
          if (!adminRef.exists) {
            
            this.props.dispatch({
              type: 'LOGIN_FAILURE',
              meta: {},
              payload: { message: 'This user does not have credentials to access CMS' }
            });

            if (this.props.location.pathname !== '/login') {
              this.props.history.push('/login');
            } 
          } else {
            this.props.dispatch({
              type: 'LOGIN_SUCCESS',
              meta: {},
              payload: {
                objectId: user.uid,
                sessionToken: user.refreshToken
              }
            });

            if(this.props.location.pathname === '/login'){
              this.props.history.push('/dashboard')      
            }
            if (this.refs.mainPanel) this.refs.mainPanel.scrollTop = 0;
          }
        }
        else {
          if(this.props.location.pathname !== '/login'){
            this.props.history.push('/login')
          } 
        }                    
      });
    }
  }

  render() {
    const { classes, ...rest } = this.props
    return (
      <div>
        { this.state.loadingUser ? (
          <CircularProgress className={classes.progress} size={50} />
        ) : (
          <div className={classes.wrapper}>
            { this.props.isLoggedIn ? (
              <div>
                <Sidebar
                  routes={appRoutes}
                  logoText={"Piggle CMS"}
                  logo={logo}
                  handleDrawerToggle={this.handleDrawerToggle}
                  open={this.state.mobileOpen}
                  color="blue"
                  {...rest}
                />
                <div className={classes.mainPanel} ref="mainPanel">
                  <Header
                    routes={appRoutes}
                    handleDrawerToggle={this.handleDrawerToggle}
                    {...rest}
                  />          
                  <div className={classes.content}>
                    <div className={classes.container}>{switchRoutes}</div>
                  </div>
                  <Footer />
                </div>
              </div>
            ) : (
              <Route path="/login" component={LoginPage} key="login"/>
            )}
          </div>
        )}
      </div>
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
}
function mapStateToProps(state) {
  return {
    userId: state.auth.userId,
    isLoggedIn: state.auth.isLoggedIn
  }
}


export default withStyles(appStyle)(connect(mapStateToProps)(App))
