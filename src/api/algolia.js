import * as algoliasearch from 'algoliasearch'

const algolia = algoliasearch(process.env.REACT_APP_ALOGLIA_APP_ID, process.env.REACT_APP_ALOGLIA_SEARCH_KEY)
export {
    algolia
}