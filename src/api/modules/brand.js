import {
  COUNT_BRAND_FAILED,
  COUNT_BRAND_SUCCED,
  COUNT_BRAND_PROCESSING,
  FETCH_BRAND_FAILED,
  FETCH_BRAND_SUCCED,
  FETCH_BRAND_PROCESSING,
  GET_BRAND_FAILED,
  GET_BRAND_SUCCED,
  GET_BRAND_PROCESSING,
  SAVE_BRAND_FAILED,
  SAVE_BRAND_SUCCED,
  SAVE_BRAND_PROCESSING,
  DELETE_BRAND_FAILED,
  DELETE_BRAND_SUCCED,
  DELETE_BRAND_PROCESSING,
  FETCH_STORES_FAILED,
  FETCH_STORES_SUCCED,
  FETCH_STORES_PROCESSING,
} from '../../constants'
import { db, storage } from "../firebase" 
import { store } from '../../store'
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const brand = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_BRAND_PROCESSING
      })      
      const index = algolia.initIndex('brands')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_BRAND_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_BRAND_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: FETCH_BRAND_PROCESSING
      })
      const index = algolia.initIndex('brands')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'title'
      order = order || 'asc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      search['page']= page
      search['hitsPerPage']= rowsPerPage
      index
      .search(search)
      .then(collection => {
        const list = collection.hits.map(doc => {
          return {
            [doc.objectID]: {
              title: doc.title,
              description: doc.description,
              image: doc.image
            }
          }
        })
        dispatch({
          type: FETCH_BRAND_SUCCED,
          payload: {
            list: list,
            page: page,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            orderBy: orderBy,
            order: order
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_BRAND_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getStores(searchText) {
    return dispatch => {
      dispatch({
        type: FETCH_STORES_PROCESSING
      })
      let query = db.collection("stores")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')      
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_STORES_SUCCED,
          payload: {
            list: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_STORES_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {
    return dispatch => {
      dispatch({
        type: GET_BRAND_PROCESSING
      })
      db.collection('brands').doc(id).get()
      .then(ref => {
        let data = ref.data()
        var stores = []
        if(data.stores){
          stores = data.stores.map(r => {
            return r.id
          })
        }        
        delete data['stores']
        data.stores = stores
        dispatch({
          type: GET_BRAND_SUCCED,
          payload: {
            record: Object.assign({}, {id: ref.id}, data)
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: GET_BRAND_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return dispatch => {
      dispatch({
        type: SAVE_BRAND_PROCESSING
      })
      var stores = Helper.formatDataToFirebase(values.stores, 'stores') 
      if (values.image && typeof values.image === "object") {
        // Upload Image and get url
        let ref = storage.ref().child('images/brands/'+store.getState().auth.userId+'/'+ values.image.name)
        ref.put(values.image)
          .then(function(file) {
            file.ref.getDownloadURL().then(function(downloadURL) {
              if (
                values.id !== undefined &&
                values.id !== null &&
                values.id !== ''
              ) {
                // Update
                let doc = db.collection('brands').doc(values.id)
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  stores: stores
                }
                doc.update(data)
                .then(async r => {
                  data.stores = data.stores.map(r=> {
                    return r.id
                  })
                  data.objectID = values.id
                 await  Helper.updateDatatoAlgolia(data,'brands')
                  .then(res => console.log('SUCCESS ALGOLIA equipment Add', res))
                  .catch(err => console.log('ERROR ALGOLIA equipment ADD', err))
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_BRAND_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_BRAND_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
              else{
                // New
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  stores: stores
                }
                db.collection('brands').add(data)
                .then(async ref => {
                  data.stores = data.stores.map(r=> {
                    return r.id
                  })
                  data.objectID = ref.id
                  await Helper.addDatatoAlgolia(data, 'brands')
                  .then(res => console.log('SUCCESS ALGOLIA equipment Add', res))
                  .catch(err => console.log('ERROR ALGOLIA equipment ADD', err))
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_BRAND_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_BRAND_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
            })            
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_BRAND_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
      }
      else{
        if (
          values.id !== undefined &&
          values.id !== null &&
          values.id !== ''
        ) {
          // Update
          let doc = db.collection('brands').doc(values.id)
          let data ={
            title: values.title || '',
            description: values.description || '',
            stores: stores
          }
          doc.update(data)
          .then(async r => {
            data.stores = data.stores.map(r=> {
              return r.id
            })
            if(values.image){
              data.image = values.image
            }
            data.objectID = values.id
             await Helper.updateDatatoAlgolia(data,'brands')
            .then(res => console.log('SUCCESS ALGOLIA equipment Add', res))
            .then(ref => {
              dispatch({
                type: SAVE_BRAND_SUCCED
              })
            })
            .catch(err => console.log('ERROR ALGOLIA equipment ADD', err))
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_BRAND_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
        else{
          // New
          let data = {
            title: values.title || '',
            description: values.description || '',
            stores: stores
          }
          db.collection('brands').add(data)
          .then(async r => {
            data.stores = data.stores.map(r=> {
              return r.id
            })
            data.objectID = r.id
            await Helper.addDatatoAlgolia(data, 'brands')
            .then(res => console.log('SUCCESS ALGOLIA equipment Add', res))
            .then(ref => {
              dispatch({
                type: SAVE_BRAND_SUCCED
              })
            })            
            .catch(err => console.log('ERROR ALGOLIA equipment ADD', err))
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_BRAND_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
      }
      
    }
  },
  delete(ids) {
    return  async (dispatch) => {
      dispatch({
        type: DELETE_BRAND_PROCESSING
      })
      var batch = db.batch()
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        await Helper.deleteDataAlgolia(id, 'brands')
        batch.delete(db.collection('brands').doc(id))
      }
      batch.commit()
      .then(function() {
        dispatch({
          type: DELETE_BRAND_SUCCED
        })
      })
      .catch(error => {
        dispatch({
          type: DELETE_BRAND_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
}
