import {
  COUNT_CHILD_TYPE_FAILED,
  COUNT_CHILD_TYPE_SUCCED,
  COUNT_CHILD_TYPE_PROCESSING,
  FETCH_CHILD_TYPE_FAILED,
  FETCH_CHILD_TYPE_SUCCED,
  FETCH_CHILD_TYPE_PROCESSING,
  GET_CHILD_TYPE_FAILED,
  GET_CHILD_TYPE_SUCCED,
  GET_CHILD_TYPE_PROCESSING,
  SAVE_CHILD_TYPE_FAILED,
  SAVE_CHILD_TYPE_SUCCED,
  SAVE_CHILD_TYPE_PROCESSING,
  DELETE_CHILD_TYPE_FAILED,
  DELETE_CHILD_TYPE_SUCCED,
  DELETE_CHILD_TYPE_PROCESSING
} from '../../constants'
import { db } from "../firebase" 
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const childType = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_CHILD_TYPE_PROCESSING
      })      
      const index = algolia.initIndex('child_types')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_CHILD_TYPE_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_CHILD_TYPE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: FETCH_CHILD_TYPE_PROCESSING
      })
      const index = algolia.initIndex('child_types')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'title'
      order = order || 'asc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      search['page']= page
      search['hitsPerPage']= rowsPerPage
      index
      .search(search)
      .then(collection => {
        const list = collection.hits.map(doc => {
          return {
            [doc.objectID]: {
              title: doc.title,
            }
          }
        })
        dispatch({
          type: FETCH_CHILD_TYPE_SUCCED,
          payload: {
            list: list,
            page: page,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            orderBy: orderBy,
            order: order
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_CHILD_TYPE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {
    return (dispatch) => {
      dispatch({
        type: GET_CHILD_TYPE_PROCESSING
      })
      db.collection('childTypes').doc(id).get()
      .then(ref => {
        dispatch({
          type: GET_CHILD_TYPE_SUCCED,
          payload: {
            record: Object.assign({}, {id: ref.id}, ref.data())
          }
        })

        
      })
      .catch(function(error) {
        dispatch({
          type: GET_CHILD_TYPE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return (dispatch) => {
      dispatch({
        type: SAVE_CHILD_TYPE_PROCESSING
      })
      if (
        values.id !== undefined &&
        values.id !== null &&
        values.id !== ''
      ) {
        // Update
        let doc = db.collection('childTypes').doc(values.id)
        let data = {
          title: values.title,
        }
        doc.update(data)
        .then(async r => {
          data.objectID = values.id
          await Helper.updateDatatoAlgolia(data,'child_types')
         })
        .then(ref => {
          dispatch({
            type: SAVE_CHILD_TYPE_SUCCED
          })
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_CHILD_TYPE_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }
      else{
        // New
        let data = {
          title: values.title,
        }
        db.collection('childTypes').add(data)
        .then(async r => {
          data.objectID = r.id
          await Helper.addDatatoAlgolia(data, 'child_types')
        })
        .then(ref => {
          dispatch({
            type: SAVE_CHILD_TYPE_SUCCED
          })
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_CHILD_TYPE_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_CHILD_TYPE_PROCESSING
      })
      var batch = db.batch()
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        await Helper.deleteDataAlgolia(id, 'child_types')
        batch.delete(db.collection('childTypes').doc(id))
      }
      batch.commit()
      .then(function() {
        dispatch({
          type: DELETE_CHILD_TYPE_SUCCED
        })
      })
      .catch(error => {
        dispatch({
          type: DELETE_CHILD_TYPE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
}
