import {
  COUNT_USER_FAILED,
  COUNT_USER_SUCCED,
  COUNT_USER_PROCESSING,
  FETCH_USER_FAILED,
  FETCH_USER_SUCCED,
  FETCH_USER_PROCESSING,
  GET_USER_FAILED,
  GET_USER_SUCCED,
  GET_USER_PROCESSING,
  //FETCH_USER_REGIONS_FAILED,
  //FETCH_USER_REGIONS_SUCCED,
  FETCH_USER_REGIONS_PROCESSING,
  SAVE_USER_FAILED,
  SAVE_USER_SUCCED,
  SAVE_USER_PROCESSING,
  DELETE_USER_FAILED,
  DELETE_USER_SUCCED,
  DELETE_USER_PROCESSING,
  FETCH_CHILDTYPE_FAILED,
  FETCH_CHILDTYPE_SUCCED,
  FETCH_CHILDTYPE_PROCESSING,
} from '../../constants'
import { db, firebase, auth } from "../firebase" 
import axios from 'axios'
import moment from 'moment'
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const user = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_USER_PROCESSING
      })      
      const index = algolia.initIndex('users')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_USER_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_USER_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: FETCH_USER_PROCESSING
      })
      const index = algolia.initIndex('users')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'name'
      order = order || 'asc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      search['page']= page
      search['hitsPerPage']= rowsPerPage
      index
      .search(search)
      .then(collection => {
        const list = collection.hits.map(doc => {
          let role = doc.role
          if(typeof role === 'object'){
            role = role.value
          }
          return {
            [doc.objectID]: {
              name: doc.name,
              email: doc.email,
              address: doc.place,
              role: role
            }
          }
        })
        dispatch({
          type: FETCH_USER_SUCCED,
          payload: {
            list: list,
            page: page,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            orderBy: orderBy,
            order: order
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_USER_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {
    return async dispatch => {
      dispatch({
        type: GET_USER_PROCESSING
      })
      db.collection('users').doc(id).get()
      .then(ref => {
        if(ref.data()){
          let data = ref.data()
          console.log(data)
          var childType = []
          var offers = []
          var subscriptions = [] 
          console.log(data.expire)
          if(data.childType){
            childType = data.childType.map(r => {
              return r.id
            })
          }
          if(data.offers){
            offers = data.offers.map(r => {
              return r.id
            })
          }
          if(data.subscriptions){
            subscriptions = data.subscriptions.map(r => {
              return r.id
            })
          }
          var currentDate = moment().valueOf()
          data.expire = typeof data.expire === 'object' ? data.expire.seconds  * 1000 : data.expire
          if(currentDate > data.expire){
            data.is_expire = true
          }else{
            data.is_expire = false
          }
          data.childType = childType
          data.offers = offers
          data.subscriptions = subscriptions
          data.expire = data.expire ? moment.unix(data.expire/1000).format("DD MMM YYYY hh:mm a"): ''
          dispatch({
            type: GET_USER_SUCCED,
            payload: {
              record: Object.assign({}, {id: ref.id}, data),
              lat:data.location.latitude,
              lng:data.location.longitude
            }
          })
        }
        else{
          dispatch({
            type: GET_USER_FAILED,
            payload: {
              error: 'Error: 404 Cannot find user'
            }
          })
        }
      })
      .catch(function(error) {
        dispatch({
          type: GET_USER_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getRegions(searchText) {
    return (dispatch) => {
      dispatch({
        type: FETCH_USER_REGIONS_PROCESSING
      })
      
    }
  },
  save(values) {
    return async (dispatch) => {
      dispatch({
        type: SAVE_USER_PROCESSING
      })
      var location = null
      if(values.location.lat && values.location.lng){
        location = new firebase.firestore.GeoPoint(values.location.lat, values.location.lng)
      }
      var childTypes = Helper.formatDataToFirebase(values.childType, 'childTypes')
      if (
        values.id !== undefined &&
        values.id !== null &&
        values.id !== ''
      ) {
        // Update
        let doc = db.collection('users').doc(values.id)
        // doc.get().then(ref=>{
        //   let data = ref.data()
        //   firebase.auth().getUser(data.uid)
        //     .then(function(userRecord) {
        //       userRecord.updatePassword(values.password).then(() => {
        //         // Update successful.
        //       }, (error) => {
        //         // An error happened.
        //       })
        //     })
        //     .catch(function(error) {
        //       console.log("Error fetching user data:", error);
        //     })
        // })
        let data = {
          place: values.place || '',
          location: location || '',
          name: values.name || '',
          email: values.email || '',
          role: values.role || '',
          childType:childTypes
        }
        doc.update(data)
        .then(async r => {
          data.objectID = values.id
          data.childType = data.childType.map(r => {
            return r.id
          })
          await Helper.updateDatatoAlgolia(data,'users')
        })
        .then(ref => {
          dispatch({
            type: SAVE_USER_SUCCED
          })
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_USER_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }else{
        // New
        if(values.email && values.password){
          let token = process.env.REACT_APP_FIREBASE_AUTHENTICATE_TOKEN
          let url = process.env.REACT_APP_FIREBASE_AUTHENTICATE_URL+ '/signup'
          let config = {
            headers: {'Authorization': "Bearer " + token},
          }
          let ChildTypeIds = Helper.getIdsInSelect(values.childType)
          let data = {
          "name" : values.name,
          "email" : values.email,
          "password" : values.password,
          "location":values.location,
          "cms": true,
          "place":values.place,
          "childTypesIds":ChildTypeIds,
          "role": values.role
        }
        await axios.post(url, data , config)
          .then(async response =>{
              let ref = response.data
              if(ref && ref !== ''){
                data.objectID = ref
                await Helper.addDatatoAlgolia(data,'users')
                dispatch({
                  type: SAVE_USER_SUCCED
                })
            }
          }).catch(function(error) {
            if( error.response ){
              dispatch({
                type: SAVE_USER_FAILED,
                payload: {
                  error: 'Error: ' + error.response.data.code + ' ' + error.response.data.message
                }
              })
            }
          })
          // axios({
          //   method: 'POST',
          //   url: 'https://us-central1-piggle-a4cba.cloudfunctions.net/api/signup',
          //   config: {headers: {'Authorization': "bearer " + token}},
          //   data: data,
          //   })
          //   .then(function (response) {
          //       const data = response.data;
              
          //   })
          //   .catch(function (response) {
        
          //   })
          // firebase.auth().createUserWithEmailAndPassword(
          //     values.email,
          //     values.password
          // ).then(data => {
          //   //console.log('uid',data.user.uid)
          //   // user.updateProfile({
          //   //   displayName: values.name
          //   // }).then(function() {
          //   //   // Update successful.
          //   // }, function(error) {
          //   //   // An error happened.
          //   // })    
          //   db.collection('users').add({
          //       place: values.place || '',
          //       location: location || '',
          //       name: values.name || '',
          //       email: values.email || '',
          //       role: values.role || '',
          //       childType:childTypes,
          //       uid: data.user.uid
          //     }).then(ref => {
          //       dispatch({
          //         type: SAVE_USER_SUCCED
          //       })
          //     })
          //     .catch(function(error) {
          //       dispatch({
          //         type: SAVE_USER_FAILED,
          //         payload: {
          //           error: 'Error: ' + error.code + ' ' + error.message
          //         }
          //       })
          //     })      
          // })
          // .catch(function(error) {
          //   dispatch({
          //     type: SAVE_USER_FAILED,
          //     payload: {
          //       error: 'Error: ' + error.code + ' ' + error.message
          //     }
          //   })
          // })
        }
        
      }
    }
  },
  getChildType(searchText) {
    return dispatch => {
      dispatch({
        type: FETCH_CHILDTYPE_PROCESSING
      })
      let query = db.collection("childTypes")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')      
      .get()
      .then(collection => {
        const childType = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_CHILDTYPE_SUCCED,
          payload: {
            childType: childType
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_CHILDTYPE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_USER_PROCESSING
      })
      
      var batch = db.batch()
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        await db.collection('users').doc(id).get().then(function(doc){
        if(doc){
           let token = process.env.REACT_APP_FIREBASE_AUTHENTICATE_TOKEN
           let url = process.env.REACT_APP_FIREBASE_AUTHENTICATE_URL+ '/deleteUser'
           let config = {
            headers: {'Authorization': "Bearer " + token},
            }
              let  dataUser = {
                  "id":id,
                  "uid":doc.data().uid,
                  "name":doc.data().name
              }
            axios.post(url, dataUser , config)
              .then(response =>{
                
                })
                .then(function (response) {
                   // const data = response.data
                })
                .catch(function (response) {
                  
                })
            }
        })
        await Helper.deleteDataAlgolia(id, 'users')
        batch.delete(db.collection('users').doc(id))
      }
      batch.commit()
      .then(function() {
        dispatch({
          type: DELETE_USER_SUCCED
        })
      })
      .catch(error => {
        dispatch({
          type: DELETE_USER_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
}
