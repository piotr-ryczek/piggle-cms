import { 
  auth as authFirebase,
  db
} from '../firebase'
export const auth = {
  login(values) {
    return async dispatch => {
      try {
        dispatch({
          type: 'LOGIN_REQUEST'
        });

        const { email, password } = values;
        const { user, user: { uid, refreshToken } } = await authFirebase.signInWithEmailAndPassword(email, password);
        // const token = await user.getIdToken();
        const adminRef = await db.collection('administrators').doc(uid).get();
        if (!adminRef.exists) throw new Error('This user does not have credentials to access CMS');

        dispatch({
          type: 'LOGIN_SUCCESS',
          meta: {},
          payload: {
            objectId: uid,
            sessionToken: refreshToken
            // TODO refreshToken
          }
        });

      } catch (error) {
        dispatch({
          type: 'LOGIN_FAILURE',
          meta: {},
          payload: { message: error.message }
          // payload: {message:'The password is invalid or the user does not have a password.'}
        });
      }
    }
  },
  logout() {
    return dispatch => {
      authFirebase.signOut().then(() => {
        dispatch({
          type: 'LOGOUT'
        })
      })
    }
  }
}
