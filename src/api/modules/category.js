import {
  COUNT_CATEGORY_FAILED,
  COUNT_CATEGORY_SUCCED,
  COUNT_CATEGORY_PROCESSING,
  FETCH_CATEGORY_FAILED,
  FETCH_CATEGORY_SUCCED,
  FETCH_CATEGORY_PROCESSING,
  GET_CATEGORY_FAILED,
  GET_CATEGORY_SUCCED,
  GET_CATEGORY_PROCESSING,
  SAVE_CATEGORY_FAILED,
  SAVE_CATEGORY_SUCCED,
  SAVE_CATEGORY_PROCESSING,
  DELETE_CATEGORY_FAILED,
  DELETE_CATEGORY_SUCCED,
  DELETE_CATEGORY_PROCESSING,
  FETCH_SUBCATEGORIES_FAILED,
  FETCH_SUBCATEGORIES_SUCCED,
  FETCH_SUBCATEGORIES_PROCESSING,
} from '../../constants'
import { db, storage } from "../firebase" 
import { store } from '../../store'
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const category = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_CATEGORY_PROCESSING
      })      
      const index = algolia.initIndex('categories')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_CATEGORY_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_CATEGORY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: FETCH_CATEGORY_PROCESSING
      })
      const index = algolia.initIndex('categories')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'title'
      order = order || 'asc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      console.log(page, rowsPerPage)
      search['page'] = page
      search['hitsPerPage'] = rowsPerPage
      index
      .search(search)
      .then(collection => {
        const list = collection.hits.map(doc => {
          return {
            [doc.objectID]: {
              title: doc.title,
              description: doc.description,
              image: doc.image
            }
          }
        })
        dispatch({
          type: FETCH_CATEGORY_SUCCED,
          payload: {
            list: list,
            page: page,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            orderBy: orderBy,
            order: order
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_CATEGORY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getSubCategories(searchText) {
    return dispatch => {
      dispatch({
        type: FETCH_SUBCATEGORIES_PROCESSING
      })
      let query = db.collection("subCategories")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')      
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_SUBCATEGORIES_SUCCED,
          payload: {
            list: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_SUBCATEGORIES_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {
    return dispatch => {
      dispatch({
        type: GET_CATEGORY_PROCESSING
      })
      db.collection('categories').doc(id).get()
      .then(ref => {
        let data = ref.data()
        var subCategories = []
        if(data.subCategories){
          subCategories = data.subCategories.map(r => {
            return r.id
          })
        }        
        data.subCategories = subCategories
        dispatch({
          type: GET_CATEGORY_SUCCED,
          payload: {
            record: Object.assign({}, {id: ref.id}, data)
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: GET_CATEGORY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return dispatch => {
      dispatch({
        type: SAVE_CATEGORY_PROCESSING
      })
      var subCategories = Helper.formatDataToFirebase(values.subCategories, 'subCategories') 
      if (values.image && typeof values.image === "object") {
        // Upload Image and get url
        let ref = storage.ref().child('images/categories/'+store.getState().auth.userId+'/'+ values.image.name)
        ref.put(values.image)
          .then(function(file) {
            file.ref.getDownloadURL().then(function(downloadURL) {
              if (
                values.id !== undefined &&
                values.id !== null &&
                values.id !== ''
              ) {
                // Update
                let doc = db.collection('categories').doc(values.id)
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  subCategories: subCategories
                }
                doc.update(data)
                .then(async r => {
                  data.subCategories = data.subCategories.map(r=> {
                    return r.id
                  })
                   data.objectID = values.id
                   await Helper.updateDatatoAlgolia(data,'categories')
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_CATEGORY_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_CATEGORY_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
              else{
                // New
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  subCategories: subCategories
                }
                db.collection('categories').add(data)
                .then(async r => {
                  data.subCategories = data.subCategories.map(r=> {
                    return r.id
                  })
                  data.objectID = r.id
                  await Helper.addDatatoAlgolia(data, 'categories')
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_CATEGORY_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_CATEGORY_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
            })            
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_CATEGORY_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
      }
      else{
        if (
          values.id !== undefined &&
          values.id !== null &&
          values.id !== ''
        ) {
          // Update
          let doc = db.collection('categories').doc(values.id)
          let data = {
            title: values.title || '',
            description: values.description || '',
            subCategories: subCategories
          }
          doc.update(data)
          .then(async r => {
            data.subCategories = data.subCategories.map(r=> {
              return r.id
            })
            if(values.image){
              data.image = values.image
            }
            data.objectID = values.id
            await Helper.updateDatatoAlgolia(data,'categories')
          })
          .then(ref => {
            dispatch({
              type: SAVE_CATEGORY_SUCCED
            })
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_CATEGORY_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
        else{
          // New
          let data = {
            title: values.title || '',
            description: values.description || '',
            subCategories: subCategories
          }
          db.collection('categories').add(data)
          .then(async r => {
            data.subCategories = data.subCategories.map(r=> {
              return r.id
            })
            
            data.objectID = r.id
            await Helper.addDatatoAlgolia(data, 'categories')
          })
          .then(ref => {
            dispatch({
              type: SAVE_CATEGORY_SUCCED
            })
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_CATEGORY_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
      }
      
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_CATEGORY_PROCESSING
      })
      var subCategories = []
      // var labelSucess = []
      var labelFails = []
      var batch = db.batch()
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
       await db.collection('categories').doc(id).get()
          .then(async ref => {
            let data = ref.data()
            if(data.subCategories){
              subCategories = data.subCategories.map(r => {
                return r.id
              })
            }
            if(subCategories && subCategories.length > 0){
              labelFails.push(data.title)
              // dispatch({
              //   type: DELETE_CATEGORY_FAILED,
              //   payload: {
              //     error: 'Error: Can\'t not delete category ' + data.title + ' because it is in use. In order to delete it, first remove all its references'
              //   }
              // })
            }else{
              await Helper.deleteDataAlgolia(id, 'categories')
              batch.delete(db.collection('categories').doc(id))
            }
        })
      }
    
      batch.commit()
        .then(function() {
          if(labelFails && labelFails.length > 0){
            var errorText = 'Can\'t delete category "' + labelFails.toString() + '" which is in use. Please remove all its references first.'
            if(labelFails.length > 1){
              errorText = 'Can\'t delete categories "' + labelFails.toString() + '" which are in use. Please remove all its references first.'
            }
            dispatch({
              type: DELETE_CATEGORY_FAILED,
              payload: {
                error: errorText
              }
            })
          }else{
            dispatch({
              type: DELETE_CATEGORY_SUCCED
            })
          }
        })
        .catch(error => {
          dispatch({
            type: DELETE_CATEGORY_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
    }
  },
}
