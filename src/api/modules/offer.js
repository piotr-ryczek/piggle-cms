import {
  COUNT_OFFER_FAILED,
  COUNT_OFFER_SUCCED,
  COUNT_OFFER_PROCESSING,
  FETCH_OFFER_FAILED,
  FETCH_OFFER_SUCCED,
  FETCH_OFFER_PROCESSING,
  GET_OFFER_FAILED,
  GET_OFFER_SUCCED,
  GET_OFFER_PROCESSING,
  SAVE_OFFER_FAILED,
  SAVE_OFFER_SUCCED,
  SAVE_OFFER_PROCESSING,
  DELETE_OFFER_FAILED,
  DELETE_OFFER_SUCCED,
  DELETE_OFFER_PROCESSING,
  FETCH_STORES_FAILED,
  FETCH_STORES_SUCCED,
  FETCH_STORES_PROCESSING,  
  FETCH_BRANDS_FAILED,
  FETCH_BRANDS_SUCCED,
  FETCH_BRANDS_PROCESSING,   
  FETCH_FACILITY_FAILED,
  FETCH_FACILITY_SUCCED,
  FETCH_FACILITY_PROCESSING,  
  FETCH_SUBCATEGORIES_FAILED,
  FETCH_SUBCATEGORIES_SUCCED,
  FETCH_SUBCATEGORIES_PROCESSING,
  FETCH_OFFER_BRAND_PROCESSING,
  FETCH_OFFER_BRAND_SUCCED,
  FETCH_OFFER_BRAND_FAILED,
  FETCH_BRAND_STORE_PROCESSING,
  FETCH_BRAND_STORE_SUCCED,
  FETCH_BRAND_STORE_FAILED
} from '../../constants'
import { db, storage } from "../firebase" 
import { store as storeReact } from '../../store'
import moment from 'moment'
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const offer = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_OFFER_PROCESSING
      })      
      const index = algolia.initIndex('offers')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      // index
      // .search(search)
      // .then(collection => {        
      //   dispatch({
      //     type: COUNT_OFFER_SUCCED,
      //     payload: {
      //       total: collection.hits.length
      //     }
      //   })
      // })
      // .catch(function(error) {
      //   dispatch({
      //     type: COUNT_OFFER_FAILED,
      //     payload: {
      //       error: 'Error: ' + error.code + ' ' + error.message
      //     }
      //   })
      // })
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits)
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_OFFER_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_OFFER_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText,order, orderBy) {
    return async dispatch => {
      dispatch({
        type: FETCH_OFFER_PROCESSING
      })
      const index = algolia.initIndex('offers')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'start_date'
      order = order || 'desc'
      const orderRank = order+"("+orderBy+")"
      var rankSetting = { ranking: [orderRank] }
      if(orderBy === 'title'){
        rankSetting = {customRanking: [
            orderRank,        
          ],
          ranking: [ "custom" ]
        }
      }
      
      await index.setSettings(rankSetting, function(err) {
        if (!err) {
          // console.log('success')
        }
        else{
          // console.log(err)
        }
      })
      search['page']= page
      search['facets'] = ['categories', 'subCategories']
      search['hitsPerPage']= rowsPerPage      
      index
      .search(search)
      .then(collection => {        
        const list = collection.hits.map(doc => {
          return {
            [doc.objectID]: {
              title: doc.title,
              description: doc.description,
              url: doc.share_url,
              startDate: doc.startDate ? moment.unix(doc.startDate._seconds).format('L') : '',
              endDate: doc.endDate ? moment.unix(doc.endDate._seconds).format('L') : '',
              image: doc.image,
            }
          }
        })
        dispatch({
          type: FETCH_OFFER_SUCCED,
          payload: {
            list: list,
            page: page,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            orderBy: orderBy,
            order: order
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_OFFER_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })      
    }
  },
  getStores(searchText) {
    return dispatch => {
      dispatch({
        type: FETCH_STORES_PROCESSING
      })
      let query = db.collection("stores")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')      
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title, brand: doc.data().brand ? doc.data().brand.id : null
          }
        })
        dispatch({
          type: FETCH_STORES_SUCCED,
          payload: {
            list: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_STORES_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getBrands(searchText) {
    return (dispatch) => {
      dispatch({
        type: FETCH_BRANDS_PROCESSING
      })
      let query = db.collection("brands")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')  
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_BRANDS_SUCCED,
          payload: {
            brands: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_BRANDS_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getSubCategories(searchText) {
    return dispatch => {
      dispatch({
        type: FETCH_SUBCATEGORIES_PROCESSING
      })
      let query = db.collection("subCategories")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')      
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_SUBCATEGORIES_SUCCED,
          payload: {
            list: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_SUBCATEGORIES_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getFacilities(searchText) {
    return dispatch => {
      dispatch({
        type: FETCH_FACILITY_PROCESSING
      })
      let query = db.collection("facilities")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title') 
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_FACILITY_SUCCED,
          payload: {
            list: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_FACILITY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {
    return dispatch => {
      dispatch({
        type: GET_OFFER_PROCESSING
      })
      return db.collection('offers').doc(id).get()
      .then(async ref => {
        let data = ref.data()
        data.shareUrl = data.share_url ? data.share_url :''
        data.startDate = data.startDate ? moment.unix(data.startDate.seconds) : null
        data.endDate = data.endDate ? moment.unix(data.endDate.seconds) : null
        let brand = data.brand ? data.brand.id : null
        data.brand = brand
        var stores = []
        // var storesB =[]
        if(data.stores){
          stores = data.stores.map(r => {
            return r.id
          })
        }
        // if(brand){
        // await  db.collection('brands').doc(brand).get()
        //   .then((br) => {
        //     let refBrand = br.data()
        //     refBrand.stores.map( wdf => {
        //     db.collection('stores').doc(wdf.id).get().then(rfstore => {
        //         storesB.push({ value:wdf.id , label : rfstore.data().title })
        //       })
        //    })
        //   })
        // }
        data.stores = stores
        var subCategories = []
        if(data.subCategories){
          subCategories = data.subCategories.map(r => {
            return r.id
          })
        }        
        data.subCategories = subCategories
        var facilities = []
        if(data.facilities){          
          facilities = data.facilities.map(r => {
            return r.id
          })
        }
        data.facilities = facilities
        var categories = []
        if(data.categories){          
          categories = data.categories.map(r => {
            return r.id
          })
        }
        data.categories = categories
        dispatch({
          type: GET_OFFER_SUCCED,
          payload: {
           // listStore:storesB,
            record: Object.assign({}, {id: ref.id}, data)
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: GET_OFFER_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return async dispatch => {
      dispatch({
        type: SAVE_OFFER_PROCESSING
      })
      const startDate = values.startDate ? values.startDate.toDate() : ''
      const endDate = values.endDate ? values.endDate.toDate() :''      
      var brand = null
      if(values.brand !== null && values.brand !== undefined && values.brand !== ''){
          let brandId  = values.brand.value || values.brand
          brand = db.collection('brands').doc(brandId)
      }            
      var stores = Helper.formatDataToFirebase(values.stores, 'stores')     
      var subCategories = Helper.formatDataToFirebase(values.subCategories, 'subCategories')
      var facilities = Helper.formatDataToFirebase(values.facilities, 'facilities')
      var data ={
        title: values.title || '',
        subtitle: values.subtitle || '',
        voucherTitle: values.voucherTitle || '',
        voucherInstructions: values.voucherInstructions || '',
        voucherAdditionalInstructions: values.voucherAdditionalInstructions || '',
        share_url: values.shareUrl || '', 
        startDate: startDate || null,
        endDate: endDate || null,
        brand: brand,
        stores: stores,
        subCategories: subCategories,
        facilities: facilities,
      }
      if (values.image && typeof values.image === "object") {
        let refImage = storage.ref().child('images/offers/images/'+storeReact.getState().auth.userId+'/'+ values.image.name)
        let fileImage = await refImage.put(values.image)
        if(fileImage){
          data['image'] = await fileImage.ref.getDownloadURL()
        }
      }
    
      if (values.voucherImage && typeof values.voucherImage === "object") {
        let refVoucherImage = storage.ref().child('images/offers/voucherImage/'+storeReact.getState().auth.userId+'/'+ values.voucherImage.name)
        let fileVoucherImage = await refVoucherImage.put(values.voucherImage)
        if(fileVoucherImage){
          data['voucherImage'] = await fileVoucherImage.ref.getDownloadURL()
        }
      }
      console.log('save--',data)
      if (
        values.id !== undefined &&
        values.id !== null &&
        values.id !== ''
      ) {
        // Update
        data.share_url = values.shareUrl
        let doc = db.collection('offers').doc(values.id)
        doc.update(data)
        .then(async r => {
          if(data.brand && data.brand!==null ){
             data.brand = data.brand.id
          }
          data.stores = data.stores.map(r=> {
            return r.id
          })
          data.facilities = data.facilities.map(r=> {
            return r.id
          })
          data.subCategories = data.subCategories.map(r=> {
            return r.id
          })
          data.objectID = values.id
          if(data.startDate !== '' && typeof data.startDate === 'object' && data.startDate!==null){
            data.startDate = {_seconds :data.startDate.getTime() / 1000, _nanoseconds: data.startDate.getTime()}
          }
          if(data.endDate !== '' && typeof data.endDate === 'object' && data.endDate!==null){
            data.endDate = {_seconds :data.endDate.getTime() / 1000, _nanoseconds: data.endDate.getTime()}
          }
          if(values.image && data['image'] === undefined ){
            data.image = values.image
          }
          if(values.voucherImage && data['voucherImage'] === undefined ){
            data.voucherImage = values.voucherImage
          }
          await Helper.updateDatatoAlgolia(data,'offers')
        })
        .then(ref => {
          dispatch({
            type: SAVE_OFFER_SUCCED
          })
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_OFFER_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }
      else{
        // New
        db.collection('offers').add(data)
        .then(async r => {
          if(data.brand && data.brand!==null ){
              data.brand = data.brand.id
          }
          data.stores = data.stores.map(r=> {
            return r.id
          })
          data.facilities = data.facilities.map(r=> {
            return r.id
          })
          data.subCategories = data.subCategories.map(r=> {
            return r.id
          })
          data.objectID = r.id
          if(data.startDate !== '' && typeof data.startDate === 'object' && data.startDate!==null){
            data.startDate = {_seconds :data.startDate.getTime() / 1000, _nanoseconds: data.startDate.getTime()}
          }
          if(data.endDate !== '' && typeof data.endDate === 'object' && data.endDate!==null){
            data.endDate = {_seconds :data.endDate.getTime() / 1000, _nanoseconds: data.endDate.getTime()}
          }
          await Helper.addDatatoAlgolia(data, 'offers')
        })
        .then(ref => {
          dispatch({
            type: SAVE_OFFER_SUCCED
          })
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_OFFER_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }    
      
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_OFFER_PROCESSING
      })
      var batch = db.batch()
      // var labelFails = []
      var offerDelete = []
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        var checkExits = false
        var userId = []
        let refOffer = db.collection('offers').doc(id)
        let offerTitle = await db.collection('offers').doc(id).get().then(doc=>{
              var data  = doc.data()
              return data.title
        })
        await db.collection("users").where("offers", "array-contains", refOffer).get().then( async collection =>
        {
          if(collection.docs.length > 0){
            checkExits = true
          }
          collection.forEach(c => {
            // let data = c.data()
            if(c){
              //console.log(c.id)
              userId.push(c.id)
            }
          })
        })
        //console.log(checkExits) 
        if(checkExits){
          offerDelete.push(offerTitle)
        }else{
           await Helper.deleteDataAlgolia(id, 'offers')
           batch.delete(db.collection('offers').doc(id))
        }
      }
      batch.commit()
        .then(function() {
          if(offerDelete && offerDelete.length > 0){
            var errorText = 'Can not delete offer "' + offerDelete.toString() + '" which is in use "My offers"'
            if(offerDelete.length>1){
              errorText = 'Can not delete offers "' + offerDelete.toString() + '" which are in use "My offers"'
            }
            dispatch({
              type: DELETE_OFFER_FAILED,
              payload: {
                error: errorText
              }
            })
          }else{
            dispatch({
              type: DELETE_OFFER_SUCCED
            })
          }
        })
        .catch(error => {
          dispatch({
            type: DELETE_OFFER_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
    }
    // return async (dispatch) => {
    //   dispatch({
    //     type: DELETE_OFFER_PROCESSING
    //   })
    //   var batch = db.batch()
    //   for (let index = 0; index < ids.length; index++) {
    //     const id = ids[index]
    //     await Helper.deleteDataAlgolia(id, 'offers')
    //     batch.delete(db.collection('offers').doc(id))
    //   }
    //   batch.commit()
    //   .then(function() {
    //     dispatch({
    //       type: DELETE_OFFER_SUCCED
    //     })
    //   })
    //   .catch(error => {
    //     dispatch({
    //       type: DELETE_OFFER_FAILED,
    //       payload: {
    //         error: 'Error: ' + error.code + ' ' + error.message
    //       }
    //     })
    //   })
    // }
  },
  updateStores(brand,record){
    return (dispatch) => {
      dispatch({
        type: FETCH_OFFER_BRAND_PROCESSING
      })
      db.collection('brands').doc(brand).get()
      .then(async (ref) => {
        let data = ref.data()
        var list = []
       //if(data.stores){
        await data.stores.map(async r => {
          await db.collection('stores').doc(r.id).get().then(storeRf => {
                if(storeRf.data()){
                  list.push({ value:r.id , label : storeRf.data().title })
                }
              })
          })
        //record.stores = list
        record.brand = brand
         dispatch({
          type: FETCH_OFFER_BRAND_SUCCED,
          payload: {
           list : list,
           record: record
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_OFFER_BRAND_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
      }
  },
  getStoreBrand(id){
    return async dispatch => {
      dispatch({
        type: FETCH_BRAND_STORE_PROCESSING
      })
      await db.collection('offers').doc(id).get().then(async ref => {
          let dataOffer = ref.data()
          console.log(dataOffer)
          if(dataOffer){
            let brandId = dataOffer.brand.id
            await  db.collection('brands').doc(brandId).get()
              .then(async (ref) => {
                let data = ref.data()
                var list = []
              //if(data.stores){
                await data.stores.map( async r => {
                  await db.collection('stores').doc(r.id).get().then( async store => {
                    await  list.push({ value:r.id , label : store.data().title })
                      })
                  })
               dispatch({
                  type: FETCH_BRAND_STORE_SUCCED,
                  payload: {
                    list : list
                  }
                })
              })
              .catch(function(error) {
                dispatch({
                  type: FETCH_BRAND_STORE_FAILED,
                  payload: {
                    error: 'Error: ' + error.code + ' ' + error.message
                  }
                })
              })
          }
          else{
            dispatch({
              type: FETCH_BRAND_STORE_FAILED,
              payload: {
                error: 'Error: 404 Not found offer'
              }
            })            
          }

      })
    }
  }
}

