import {
  COUNT_STORE_FAILED,
  COUNT_STORE_SUCCED,
  COUNT_STORE_PROCESSING,
  FETCH_STORE_FAILED,
  FETCH_STORE_SUCCED,
  FETCH_STORE_PROCESSING,
  GET_STORE_FAILED,
  GET_STORE_SUCCED,
  GET_STORE_PROCESSING,
  SAVE_STORE_FAILED,
  SAVE_STORE_SUCCED,
  SAVE_STORE_PROCESSING,
  DELETE_STORE_FAILED,
  DELETE_STORE_SUCCED,
  DELETE_STORE_PROCESSING,
  FETCH_BRANDS_FAILED,
  FETCH_BRANDS_SUCCED,
  FETCH_BRANDS_PROCESSING,  
} from '../../constants'
import { firebase, db } from "../firebase" 
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const store = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_STORE_PROCESSING
      })      
      const index = algolia.initIndex('stores')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_STORE_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_STORE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return async dispatch => {
      dispatch({
        type: FETCH_STORE_PROCESSING
      })
      const index = algolia.initIndex('stores')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'title'
      order = order || 'asc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      search['page']= page
      search['hitsPerPage']= rowsPerPage
      index
      .search(search)
      .then(async(collection) => {
        var list = []
        var hasError = null
        await Promise.all(collection.hits.map(async(doc) => {
          try{
            var brand = ''
            if(doc.brand){
              const brandId = doc.brand
              if(brandId && brandId !== undefined){
                const indexBrand = algolia.initIndex('brands')
                try {
                  brand = await indexBrand.getObject(brandId)
                }
                catch(err) {
                  console.log(err)
                }
              }              
            }
            console.log(doc)
            list.push({
              [doc.objectID]: {
                title: doc.title,
                address: doc.place,
                location: doc.location._lat + "," + doc.location._long,
                brand: brand ? brand.title : ""
              }
            })
          } catch (error) {
            hasError =  error
          }                    
        }))
        if(hasError){
          dispatch({
            type: FETCH_STORE_FAILED,
            payload: {
              error: 'Error: ' + hasError.code + ' ' + hasError.message
            }
          })
        }
        else{
          dispatch({
            type: FETCH_STORE_SUCCED,
            payload: {
              list: list,
              page: page,
              rowsPerPage: rowsPerPage,
              searchText: searchText,
              orderBy: orderBy,
              order: order
            }
          })
        }        
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_STORE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getBrands(searchText) {
    return (dispatch) => {
      dispatch({
        type: FETCH_BRANDS_PROCESSING
      })
      let query = db.collection("brands")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')  
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_BRANDS_SUCCED,
          payload: {
            brands: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_BRANDS_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {  
    return dispatch => {
      dispatch({
        type: GET_STORE_PROCESSING
      })
      db.collection('stores').doc(id).get()
      .then(ref => {
        let data = ref.data()
        let brand = data.brand ? data.brand.id : null
        data.brand = brand
        dispatch({
          type: GET_STORE_SUCCED,
          payload: {
            record: Object.assign({}, {id: ref.id}, data),
            selectedBrand: brand,
            lat:data.location.latitude,
            lng:data.location.longitude
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: GET_STORE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return async dispatch => {
      dispatch({
        type: SAVE_STORE_PROCESSING
      })
      var brand = null
      if(values.brand !== null && values.brand !== undefined && values.brand !== ''){
          let brandId  = values.brand.value || values.brand
          brand = db.collection('brands').doc(brandId)
      }
      var location = null
      if(values.location.lat && values.location.lng){
        location = new firebase.firestore.GeoPoint(values.location.lat, values.location.lng)
      }
      if (
        values.id !== undefined &&
        values.id !== null &&
        values.id !== ''
      ) {
        // Update
        let doc = db.collection('stores').doc(values.id)
        let data = {
          title: values.title || '',
          description: values.description || '',
          place: values.place || '',
          location: location || '',
          address: {
            line1: values.line1 || '',
            line2: values.line2 || '',
            line3: values.line3 || '',
            city: values.city || '',
            postcode: values.postcode || '',
          },
          brand: brand
        }
        doc.update(data)
        .then(async r => {
          if(data.brand && data.brand!==null ){
             data.brand = data.brand.id
          }
          data.objectID = values.id
          await Helper.updateDatatoAlgolia(data,'stores')
        })
        .then(ref => {
          dispatch({
            type: SAVE_STORE_SUCCED
          })
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_STORE_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }
      else{
        // New
        let data = {
          title: values.title || '',
          description: values.description || '',
          place: values.place || '',
          location: location || '',
          address: {
            line1: values.line1 || '',
            line2: values.line2 || '',
            line3: values.line3 || '',
            city: values.city || '',
            postcode: values.postcode || '',
          },
          brand: brand
        }
       await db.collection('stores').add(data)
        .then(async r => {
          if(data.brand && data.brand!==null ){
             data.brand = data.brand.id
          }
          data.objectID = r.id
          await Helper.addDatatoAlgolia(data, 'stores')
        })
        .then(ref => {
          dispatch({
            type: SAVE_STORE_SUCCED
          })
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_STORE_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }
      
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_STORE_PROCESSING
      })
      var batch = db.batch()
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        await Helper.deleteDataAlgolia(id, 'stores')
        batch.delete(db.collection('stores').doc(id))
      }
      batch.commit()
      .then(function() {
        dispatch({
          type: DELETE_STORE_SUCCED
        })
      })
      .catch(error => {
        dispatch({
          type: DELETE_STORE_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
}
