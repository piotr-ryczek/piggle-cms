import {
  COUNT_FACILITY_FAILED,
  COUNT_FACILITY_SUCCED,
  COUNT_FACILITY_PROCESSING,
  FETCH_FACILITY_FAILED,
  FETCH_FACILITY_SUCCED,
  FETCH_FACILITY_PROCESSING,
  GET_FACILITY_FAILED,
  GET_FACILITY_SUCCED,
  GET_FACILITY_PROCESSING,
  SAVE_FACILITY_FAILED,
  SAVE_FACILITY_SUCCED,
  SAVE_FACILITY_PROCESSING,
  DELETE_FACILITY_FAILED,
  DELETE_FACILITY_SUCCED,
  DELETE_FACILITY_PROCESSING,
} from '../../constants'
import { db, storage } from "../firebase" 
import { store } from '../../store'
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const facility = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_FACILITY_PROCESSING
      })      
      const index = algolia.initIndex('facilities')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_FACILITY_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_FACILITY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: FETCH_FACILITY_PROCESSING
      })
      const index = algolia.initIndex('facilities')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'title'
      order = order || 'asc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      search['page']= page
      search['hitsPerPage']= rowsPerPage
      index
      .search(search)
      .then(collection => {
        const list = collection.hits.map(doc => {
          return {
            [doc.objectID]: {
              title: doc.title,
              icon: doc.icon
            }
          }
        })
        dispatch({
          type: FETCH_FACILITY_SUCCED,
          payload: {
            list: list,
            page: page,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            orderBy: orderBy,
            order: order
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_FACILITY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {
    return dispatch => {
      dispatch({
        type: GET_FACILITY_PROCESSING
      })
      db.collection('facilities').doc(id).get()
      .then(ref => {
        dispatch({
          type: GET_FACILITY_SUCCED,
          payload: {
            record: Object.assign({}, {id: ref.id}, ref.data())
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: GET_FACILITY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return async dispatch => {
      dispatch({
        type: SAVE_FACILITY_PROCESSING
      })
      var data ={
        title: values.title,
      }
      if (values.icon && typeof values.icon === "object") {
        let refImage = storage.ref().child('images/facilities/icons/'+store.getState().auth.userId+'/'+ values.icon.name)
        let fileImage = await refImage.put(values.icon)
        if(fileImage){
          data['icon'] = await fileImage.ref.getDownloadURL()
        }
      }    
      if (values.deactiveIcon && typeof values.deactiveIcon === "object") {
        let refDeactiveIcon = storage.ref().child('images/facilities/deactiveIcons/'+store.getState().auth.userId+'/'+ values.deactiveIcon.name)
        let fileDeactiveIcon = await refDeactiveIcon.put(values.deactiveIcon)
        if(fileDeactiveIcon){
          data['deactiveIcon'] = await fileDeactiveIcon.ref.getDownloadURL()
        }
      }

      if (
        values.id !== undefined &&
        values.id !== null &&
        values.id !== ''
      ) {
        // Update
        let doc = db.collection('facilities').doc(values.id)
        doc.update(data)
        .then(async ref => {
          data.objectID = values.id
          if(values.icon && data['icon']===undefined ){
            data.icon = values.icon
          }
          if(values.deactiveIcon && data['deactiveIcon']===undefined){
            data.deactiveIcon = values.deactiveIcon
          }
         await  Helper.updateDatatoAlgolia(data, 'facilities')
          .then(res => console.log('SUCCESS ALGOLIA equipment Add', res))
          .then(ref => {
            dispatch({
              type: SAVE_FACILITY_SUCCED
            })
          })
          .catch(err => console.log('ERROR ALGOLIA equipment ADD', err))
        })        
        .catch(function(error) {
          dispatch({
            type: SAVE_FACILITY_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }
      else{
        // New
        db.collection('facilities').add(data)
        .then(async ref => {
          data.objectID = ref.id
          await Helper.addDatatoAlgolia(data, 'facilities')
          .then(res => console.log('SUCCESS ALGOLIA equipment Add', res))
          .then(ref => {
            dispatch({
              type: SAVE_FACILITY_SUCCED
            })
          })
          .catch(err => console.log('ERROR ALGOLIA equipment ADD', err))
        })
        .catch(function(error) {
          dispatch({
            type: SAVE_FACILITY_FAILED,
            payload: {
              error: 'Error: ' + error.code + ' ' + error.message
            }
          })
        })
      }      
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_FACILITY_PROCESSING
      })
      var batch = db.batch()
      let promises = []
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        // await Helper.deleteDataAlgolia(id, 'facilities')
        promises.push(deleteFacilityAlgolia(id))
        batch.delete(db.collection('facilities').doc(id))
      }
      await Promise.all(promises)
      batch.commit()
      .then(function() {
        dispatch({
          type: DELETE_FACILITY_SUCCED
        })
      })
      .catch(error => {
        dispatch({
          type: DELETE_FACILITY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
}
const deleteFacilityAlgolia = async (id) => {    
  return await Helper.deleteDataAlgolia(id, 'facilities')
}