import {
  COUNT_SPONSOR_FAILED,
  COUNT_SPONSOR_SUCCED,
  COUNT_SPONSOR_PROCESSING,
  FETCH_SPONSOR_FAILED,
  FETCH_SPONSOR_SUCCED,
  FETCH_SPONSOR_PROCESSING,
  GET_SPONSOR_FAILED,
  GET_SPONSOR_SUCCED,
  GET_SPONSOR_PROCESSING,
  SAVE_SPONSOR_FAILED,
  SAVE_SPONSOR_SUCCED,
  SAVE_SPONSOR_PROCESSING,
  DELETE_SPONSOR_FAILED,
  DELETE_SPONSOR_SUCCED,
  DELETE_SPONSOR_PROCESSING,
} from '../../constants'
import { db, storage } from "../firebase" 
import { store } from '../../store'
import moment from 'moment'
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const sponsor = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_SPONSOR_PROCESSING
      })      
      const index = algolia.initIndex('sponsors')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_SPONSOR_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_SPONSOR_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: FETCH_SPONSOR_PROCESSING
      })
      const index = algolia.initIndex('sponsors')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'start_date'
      order = order || 'desc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      search['page']= page
      search['hitsPerPage']= rowsPerPage
      index
      .search(search)
      .then(collection => {
        const list = collection.hits.map(doc => {
          return {
            [doc.objectID]: {
              title: doc.title,
              description: doc.description,
              url: doc.url,
              startDate: doc.startDate ? moment.unix(doc.startDate._seconds).format('L') : '',
              endDate: doc.endDate ? moment.unix(doc.endDate._seconds).format('L') : '',
              image: doc.image,
            }
          }
        })
        dispatch({
          type: FETCH_SPONSOR_SUCCED,
          payload: {
            list: list,
            page: page,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            orderBy: orderBy,
            order: order
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_SPONSOR_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {
    return dispatch => {
      dispatch({
        type: GET_SPONSOR_PROCESSING
      })
      db.collection('sponsors').doc(id).get()
      .then(ref => {
        let data = ref.data()
        data.startDate = data.startDate ? moment.unix(data.startDate.seconds) : null
        data.endDate = data.endDate ? moment.unix(data.endDate.seconds) : null
        dispatch({
          type: GET_SPONSOR_SUCCED,
          payload: {
            record: Object.assign({}, {id: ref.id}, data)
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: GET_SPONSOR_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return dispatch => {
      dispatch({
        type: SAVE_SPONSOR_PROCESSING
      })
      const startDate = values.startDate ? values.startDate.toDate() : ''
      const endDate = values.endDate ? values.endDate.toDate() : ''            
      if (values.image && typeof values.image === "object") {
        // Upload Image and get url
        let ref = storage.ref().child('images/sponsors/'+store.getState().auth.userId+'/'+ values.image.name)
        ref.put(values.image)
          .then(function(file) {
            file.ref.getDownloadURL().then(function(downloadURL) {
              if (
                values.id !== undefined &&
                values.id !== null &&
                values.id !== ''
              ) {
                // Update
                let doc = db.collection('sponsors').doc(values.id)
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  url: values.url || '',
                  startDate: startDate || '',
                  endDate: endDate || '',
                }
                doc.update(data)
                .then(async r => {
                  data.objectID = values.id
                  await Helper.updateDatatoAlgolia(data,'sponsors')
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_SPONSOR_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_SPONSOR_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
              else{
                // New
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  url: values.url || '',
                  startDate: startDate || '',
                  endDate: endDate || '',
                }
                db.collection('sponsors').add(data)
                .then(async r => {
                  data.objectID = r.id
                  await Helper.addDatatoAlgolia(data, 'sponsors')
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_SPONSOR_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_SPONSOR_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
            })            
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_SPONSOR_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
      }
      else{
        if (
          values.id !== undefined &&
          values.id !== null &&
          values.id !== ''
        ) {
          //console.log('chagne-ssdad',values)
          // Update
          let doc = db.collection('sponsors').doc(values.id)
          let data = {
            title: values.title || '',
            description: values.description || '',
            url: values.url || '',
            startDate: startDate || '',
            endDate: endDate || '',
          }
          doc.update(data)
          .then(async r => {
            data.objectID = values.id            
            if(data.startDate !== '' && typeof data.startDate === 'object' && data.startDate!==null){
              data.startDate = {_seconds :data.startDate.getTime() / 1000, _nanoseconds: data.startDate.getTime()}
            }
            if(data.endDate !== '' && typeof data.endDate === 'object' && data.endDate!==null){
              data.endDate = {_seconds :data.endDate.getTime() / 1000, _nanoseconds: data.endDate.getTime()}
            }
            if(values.image){
              data.image = values.image
            }
            await Helper.updateDatatoAlgolia(data,'sponsors')
          })
          .then(ref => {
            dispatch({
              type: SAVE_SPONSOR_SUCCED
            })
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_SPONSOR_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
        else{
          // New
          let data = {
            title: values.title || '',
            description: values.description || '',
            url: values.url || '',
            startDate: startDate || '',
            endDate: endDate || '',
          }
          db.collection('sponsors').add(data)
          .then(async r => {
            data.objectID = r.id
            if(data.startDate !== '' && typeof data.startDate === 'object' && data.startDate!==null){
              data.startDate = {_seconds :data.startDate.getTime() / 1000, _nanoseconds: data.startDate.getTime()}
            }
            if(data.endDate !== '' && typeof data.endDate === 'object' && data.endDate!==null){
              data.endDate = {_seconds :data.endDate.getTime() / 1000, _nanoseconds: data.endDate.getTime()}
            }
            await Helper.addDatatoAlgolia(data, 'sponsors')
          })
          .then(ref => {
            dispatch({
              type: SAVE_SPONSOR_SUCCED
            })
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_SPONSOR_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
      }
      
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_SPONSOR_PROCESSING
      })
      var batch = db.batch()
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        await Helper.deleteDataAlgolia(id, 'sponsors')
        batch.delete(db.collection('sponsors').doc(id))
      }
      batch.commit()
      .then(function() {
        dispatch({
          type: DELETE_SPONSOR_SUCCED
        })
      })
      .catch(error => {
        dispatch({
          type: DELETE_SPONSOR_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
}
