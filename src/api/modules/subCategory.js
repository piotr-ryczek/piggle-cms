import {
  COUNT_SUBCATEGORY_FAILED,
  COUNT_SUBCATEGORY_SUCCED,
  COUNT_SUBCATEGORY_PROCESSING,
  FETCH_SUBCATEGORY_FAILED,
  FETCH_SUBCATEGORY_SUCCED,
  FETCH_SUBCATEGORY_PROCESSING,
  GET_SUBCATEGORY_FAILED,
  GET_SUBCATEGORY_SUCCED,
  GET_SUBCATEGORY_PROCESSING,
  SAVE_SUBCATEGORY_FAILED,
  SAVE_SUBCATEGORY_SUCCED,
  SAVE_SUBCATEGORY_PROCESSING,
  DELETE_SUBCATEGORY_FAILED,
  DELETE_SUBCATEGORY_SUCCED,
  DELETE_SUBCATEGORY_PROCESSING,
  FETCH_CATEGORIES_FAILED,
  FETCH_CATEGORIES_SUCCED,
  FETCH_CATEGORIES_PROCESSING,  
} from '../../constants'
import { db, storage } from "../firebase" 
import { store } from '../../store'
import {algolia} from '../algolia'
import Helper from '../../utils/Helper'
export const subCategory = {
  count(searchText, orderBy, order) {
    return dispatch => {
      dispatch({
        type: COUNT_SUBCATEGORY_PROCESSING
      })      
      const index = algolia.initIndex('subCategories')
      var search = {}
      searchText = searchText || ''
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      let browser = index.browseAll(search)
      var hits = []
      browser.on('result', function onResult(content) {
        hits = hits.concat(content.hits);
      })
      browser.on('end', function onEnd() {
        dispatch({
          type: COUNT_SUBCATEGORY_SUCCED,
          payload: {
            total: hits.length
          }          
        })
      })
      browser.on('error', function onError(error) {
        dispatch({
          type: COUNT_SUBCATEGORY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  list(page, rowsPerPage, searchText, orderBy, order) {
    return async dispatch => {
      dispatch({
        type: FETCH_SUBCATEGORY_PROCESSING
      })
      const index = algolia.initIndex('subCategories')
      searchText = searchText || ''
      var search = {}
      if (searchText !== '') {
        search['query'] = searchText.trim()
      }
      orderBy = orderBy || 'title'
      order = order || 'asc'
      index.setSettings({
        ranking: [
          order+"("+orderBy+")",
        ]
      })
      search['page']= page
      search['hitsPerPage']= rowsPerPage
      index
      .search(search)
      .then(async(collection) => {
        var list = []
        var hasError = null
        await Promise.all(collection.hits.map(async(doc) => {
          try{
            var category = ''
            if(doc.category){
              const categoryId = doc.category
              if(categoryId && categoryId !== undefined){
                const indexCategory = algolia.initIndex('categories')
                try {
                  category = await indexCategory.getObject(categoryId)
                }
                catch(err) {
                  console.log(err)
                }
              }              
            }
            list.push({
              [doc.objectID]: {
                title: doc.title,
                image: doc.image,
                category: category ? category.title : ""
              }
            })
          } catch (error) {
            hasError =  error
          }
        }))
        if(hasError){
          dispatch({
            type: FETCH_SUBCATEGORY_FAILED,
            payload: {
              error: 'Error: ' + hasError.code + ' ' + hasError.message
            }
          })
        }
        else{
          dispatch({
            type: FETCH_SUBCATEGORY_SUCCED,
            payload: {
              list: list,
              page: page,
              rowsPerPage: rowsPerPage,
              searchText: searchText,
              orderBy: orderBy,
              order: order
            }
          })
        }        
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_SUBCATEGORY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  getCategories(searchText) {
    return (dispatch) => {
      dispatch({
        type: FETCH_CATEGORIES_PROCESSING
      })
      let query = db.collection("categories")
      searchText = searchText || ''
      if (searchText !== '') {
        query.where('title', '==', searchText.trim())
      }
      query
      .orderBy('title')  
      .get()
      .then(collection => {
        const list = collection.docs.map(doc => {
          return {
            value: doc.id, label: doc.data().title
          }
        })
        dispatch({
          type: FETCH_CATEGORIES_SUCCED,
          payload: {
            categories: list
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: FETCH_CATEGORIES_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  get(id) {  
    return dispatch => {
      dispatch({
        type: GET_SUBCATEGORY_PROCESSING
      })
      db.collection('subCategories').doc(id).get()
      .then(ref => {
        let data = ref.data()
        let category = data.category ? data.category.id : null
        delete data['category']
        dispatch({
          type: GET_SUBCATEGORY_SUCCED,
          payload: {
            record: Object.assign({}, {id: ref.id}, data),
            selectedCategory: category
          }
        })
      })
      .catch(function(error) {
        dispatch({
          type: GET_SUBCATEGORY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
  save(values) {
    return dispatch => {
      dispatch({
        type: SAVE_SUBCATEGORY_PROCESSING
      })
      var category = null
      if(values.category !== null && values.category !== undefined && values.category !== ''){
          let categoryId  = values.category.value || values.category
          category = db.collection('categories').doc(categoryId)
      }
      if (values.image && typeof values.image === "object") {
        // Upload Image and get url
        let ref = storage.ref().child('images/categories/'+store.getState().auth.userId+'/'+ values.image.name)
        ref.put(values.image)
          .then(function(file) {
            file.ref.getDownloadURL().then(function(downloadURL) {
              if (
                values.id !== undefined &&
                values.id !== null &&
                values.id !== ''
              ) {
                // Update
                let doc = db.collection('subCategories').doc(values.id)
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  category: category
                }
                doc.update(data)
                .then(async r => {
                  data.category = data.category.id
                  data.objectID = values.id
                  await Helper.updateDatatoAlgolia(data,'subCategories')
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_SUBCATEGORY_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_SUBCATEGORY_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
              else{
                // New
                let data = {
                  title: values.title || '',
                  description: values.description || '',
                  image: downloadURL || '',
                  category: category
                }
                db.collection('subCategories').add(data)
                .then(async r => {
                  data.category = data.category.id
                  data.objectID = r.id
                  await Helper.addDatatoAlgolia(data, 'subCategories')
                })
                .then(ref => {
                  dispatch({
                    type: SAVE_SUBCATEGORY_SUCCED
                  })
                })
                .catch(function(error) {
                  dispatch({
                    type: SAVE_SUBCATEGORY_FAILED,
                    payload: {
                      error: 'Error: ' + error.code + ' ' + error.message
                    }
                  })
                })
              }
            })            
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_SUBCATEGORY_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
      }
      else{
        if (
          values.id !== undefined &&
          values.id !== null &&
          values.id !== ''
        ) {
        //  console.log('save',values.image)
          // Update
          let doc = db.collection('subCategories').doc(values.id)
          let data = {
            title: values.title || '',
            description: values.description || '',
            category: category
          }
          doc.update(data)
          .then(async r => {
            data.category = data.category.id
            data.objectID = values.id
            if(values.image){
              data.image = values.image
            }
            await Helper.updateDatatoAlgolia(data,'subCategories')
          })
          .then(ref => {
            dispatch({
              type: SAVE_SUBCATEGORY_SUCCED
            })
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_SUBCATEGORY_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
        else{
          // New
          let data = {
            title: values.title || '',
            description: values.description || '',
            category: category
          } 
          db.collection('subCategories').add(data)
          .then(async r => {
            data.category = data.category.id
            data.objectID = r.id
            await Helper.addDatatoAlgolia(data, 'subCategories')
          })
          .then(ref => {
            dispatch({
              type: SAVE_SUBCATEGORY_SUCCED
            })
          })
          .catch(function(error) {
            dispatch({
              type: SAVE_SUBCATEGORY_FAILED,
              payload: {
                error: 'Error: ' + error.code + ' ' + error.message
              }
            })
          })
        }
      }
      
      
    }
  },
  delete(ids) {
    return async (dispatch) => {
      dispatch({
        type: DELETE_SUBCATEGORY_PROCESSING
      })
      var batch = db.batch()
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index]
        await Helper.deleteDataAlgolia(id, 'subCategories')
        batch.delete(db.collection('subCategories').doc(id))
      }
      batch.commit()
      .then(function() {
        dispatch({
          type: DELETE_SUBCATEGORY_SUCCED
        })
      })
      .catch(error => {
        dispatch({
          type: DELETE_SUBCATEGORY_FAILED,
          payload: {
            error: 'Error: ' + error.code + ' ' + error.message
          }
        })
      })
    }
  },
}
