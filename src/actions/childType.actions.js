import { childType as childTypeAPI } from '../api'
import { ADD_NEW, HIDE_ALERT, TOGGLE_SIDEBAR,
  UPDATE_SEARCH_TEXT } from '../constants'
export const childType = {
  list(page, rowsPerPage, query, orderBy, order) {
    return (dispatch) => {
      dispatch(childTypeAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return dispatch => {
      dispatch(childTypeAPI.get(id))
    }
  },
  getRegions(query) {
    return dispatch => {
      dispatch(childTypeAPI.getRegions(query))
    }
  },
  count(query, orderBy, order) {
    return dispatch => {
      dispatch(childTypeAPI.count(query, orderBy, order))
    }
  },
  save(values) {
    return dispatch => {
      dispatch(childTypeAPI.save(values))
    }
  },
  delete(id) {
    return dispatch => {
      dispatch(childTypeAPI.delete(id))
    }
  },
  hideAlert() {
    return dispatch => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  toggleSidebar() {
    return dispatch => {
      dispatch({
        type: TOGGLE_SIDEBAR
      })
    }
  },
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}
