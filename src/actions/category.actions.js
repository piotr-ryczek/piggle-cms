import {
  category as categoryAPI
} from '../api'
import {
  ADD_NEW,
  HIDE_ALERT,
  UPDATE_SEARCH_TEXT
} from '../constants'
export const category = {
  list(page, rowsPerPage, query, orderBy, order) {
    return (dispatch) => {
      dispatch(categoryAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return (dispatch) => {
      dispatch(categoryAPI.get(id))
    }
  },
  getFacilities() {
    return (dispatch) => {
      dispatch(categoryAPI.getFacilities())
    }
  },
  getSubCategories(query) {
    return (dispatch) => {
      dispatch(categoryAPI.getSubCategories(query))
    }
  },
  count(query, orderBy, order) {
    return (dispatch) => {
      dispatch(categoryAPI.count(query, orderBy, order))
    }
  },
  save(values) {
    return (dispatch) => {
      dispatch(categoryAPI.save(values))
    }
  },
  delete(id) {
    return (dispatch) => {
      dispatch(categoryAPI.delete(id))
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  hideAlert() {
    return (dispatch) => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}