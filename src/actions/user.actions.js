import { user as userAPI } from '../api'
import { ADD_NEW,HIDE_ALERT, TOGGLE_SIDEBAR,UPDATE_LOCATION,
  UPDATE_SEARCH_TEXT} from '../constants'
export const user = {
  list(page, rowsPerPage, query, orderBy, order) {
    return dispatch => {
      dispatch(userAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return dispatch => {
      dispatch(userAPI.get(id))
    }
  },
  getRegions(query) {
    return dispatch => {
      dispatch(userAPI.getRegions(query))
    }
  },
  getChildType(query){
     return dispatch => {
       dispatch(userAPI.getChildType(query))
     }
  },
  count(query, orderBy, order) {
    return dispatch => {
      dispatch(userAPI.count(query, orderBy, order))
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  save(values) {
    return dispatch => {
      dispatch(userAPI.save(values))
    }
  },
  delete(id) {
    return dispatch => {
      dispatch(userAPI.delete(id))
    }
  },
  updateLocation(suggest, record) {
    let place = "" || suggest.label
    record.place = place
    return (dispatch) => {
      dispatch({
        type: UPDATE_LOCATION,
        payload: {
          record: record,
          lat: suggest.location.lat,
          lng: suggest.location.lng
        }
      })
    }
  },
  hideAlert() {
    return dispatch => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  toggleSidebar() {
    return dispatch => {
      dispatch({
        type: TOGGLE_SIDEBAR
      })
    }
  },
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}
