import {
  offer as offerAPI
} from '../api'
import {
  ADD_NEW,
  HIDE_ALERT,
  UPDATE_SEARCH_TEXT,
  UPDATE_OFFER_FORM
} from '../constants'
export const offer = {
  list(page, rowsPerPage, query, orderBy, order) {
    return (dispatch) => {
      dispatch(offerAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return (dispatch) => {
      dispatch(offerAPI.get(id))
    }
  },
  getSubCategories(query) {
    return (dispatch) => {
      dispatch(offerAPI.getSubCategories(query))
    }
  },
  getFacilities() {
    return (dispatch) => {
      dispatch(offerAPI.getFacilities())
    }
  },
  getBrands(query) {
    return (dispatch) => {
      dispatch(offerAPI.getBrands(query))
    }
  },
  getStores(query) {
    return (dispatch) => {
      dispatch(offerAPI.getStores(query))
    }
  }, 
  getStoreBrand(id) {
    return (dispatch) => {
      dispatch(offerAPI.getStoreBrand(id))
    }
  }, 
  count(query, orderBy, order) {
    return (dispatch) => {
      dispatch(offerAPI.count(query, orderBy, order))
    }
  },
  save(values) {
    return (dispatch) => {
      dispatch(offerAPI.save(values))
    }
  },
  delete(id) {
    return (dispatch) => {
      dispatch(offerAPI.delete(id))
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  hideAlert() {
    return (dispatch) => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  updateStores(brand,recordData){
    return (dispatch) => {
      dispatch(offerAPI.updateStores(brand,recordData))
    }
  },
  updateOfferForm(recordData){
    return (dispatch) => {
      dispatch({
        type: UPDATE_OFFER_FORM,
        payload: {
          record: recordData
        }              
      })
    }
  },  
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}