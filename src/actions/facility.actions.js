import {
  facility as facilityAPI
} from '../api'
import {
  ADD_NEW,
  HIDE_ALERT,
  UPDATE_SEARCH_TEXT
} from '../constants'
export const facility = {
  list(page, rowsPerPage, query, orderBy, order) {
    return (dispatch) => {
      dispatch(facilityAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return (dispatch) => {
      dispatch(facilityAPI.get(id))
    }
  },
  getFacilities() {
    return (dispatch) => {
      dispatch(facilityAPI.getFacilities())
    }
  },
  getProducts(query) {
    return (dispatch) => {
      dispatch(facilityAPI.getProducts(query))
    }
  },
  count(query, orderBy, order) {
    return (dispatch) => {
      dispatch(facilityAPI.count(query, orderBy, order))
    }
  },
  save(values) {
    return (dispatch) => {
      dispatch(facilityAPI.save(values))
    }
  },
  delete(id) {
    return (dispatch) => {
      dispatch(facilityAPI.delete(id))
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  hideAlert() {
    return (dispatch) => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}