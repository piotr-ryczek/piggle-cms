import {
  sponsor as sponsorAPI
} from '../api'
import {
  ADD_NEW,
  HIDE_ALERT,
  UPDATE_SEARCH_TEXT
} from '../constants'
export const sponsor = {
  list(page, rowsPerPage, query, orderBy, order) {
    return (dispatch) => {
      dispatch(sponsorAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return (dispatch) => {
      dispatch(sponsorAPI.get(id))
    }
  },
  getFacilities() {
    return (dispatch) => {
      dispatch(sponsorAPI.getFacilities())
    }
  },
  count(query, orderBy, order) {
    return (dispatch) => {
      dispatch(sponsorAPI.count(query, orderBy, order))
    }
  },
  save(values) {
    return (dispatch) => {
      dispatch(sponsorAPI.save(values))
    }
  },
  delete(id) {
    return (dispatch) => {
      dispatch(sponsorAPI.delete(id))
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  hideAlert() {
    return (dispatch) => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}