import {
  brand as brandAPI
} from '../api'
import {
  ADD_NEW,
  HIDE_ALERT,
  UPDATE_SEARCH_TEXT
} from '../constants'
export const brand = {
  list(page, rowsPerPage, query, orderBy, order) {
    return (dispatch) => {
      dispatch(brandAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return (dispatch) => {
      dispatch(brandAPI.get(id))
    }
  },
  getFacilities() {
    return (dispatch) => {
      dispatch(brandAPI.getFacilities())
    }
  },
  getStores(query) {
    return (dispatch) => {
      dispatch(brandAPI.getStores(query))
    }
  },
  count(query, orderBy, order) {
    return (dispatch) => {
      dispatch(brandAPI.count(query, orderBy, order))
    }
  },
  save(values) {
    return (dispatch) => {
      dispatch(brandAPI.save(values))
    }
  },
  delete(id) {
    return (dispatch) => {
      dispatch(brandAPI.delete(id))
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  hideAlert() {
    return (dispatch) => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}