import {
  store as storeAPI
} from '../api'
import {
  ADD_NEW,
  UPDATE_LOCATION,
  HIDE_ALERT,
  UPDATE_SEARCH_TEXT
} from '../constants'
import Helper from '../utils/Helper'
export const store = {
  list(page, rowsPerPage, query, orderBy, order) {
    return (dispatch) => {
      dispatch(storeAPI.list(page, rowsPerPage, query, orderBy, order))
    }
  },
  get(id) {
    return (dispatch) => {
      dispatch(storeAPI.get(id))
    }
  },
  getFacilities() {
    return (dispatch) => {
      dispatch(storeAPI.getFacilities())
    }
  },
  getBrands(query) {
    return (dispatch) => {
      dispatch(storeAPI.getBrands(query))
    }
  },
  count(query, orderBy, order) {
    return (dispatch) => {
      dispatch(storeAPI.count(query, orderBy, order))
    }
  },
  save(values) {
    return (dispatch) => {
      dispatch(storeAPI.save(values))
    }
  },
  delete(id) {
    return (dispatch) => {
      dispatch(storeAPI.delete(id))
    }
  },
  addNew() {
    return (dispatch) => {
      dispatch({
        type: ADD_NEW
      })
    }
  },
  updateLocation(suggest, record) {
    let line1 = "" || Helper.getInfoFromAddressComponent(suggest.gmaps.address_components, 'street_number')
    let line2 = "" || Helper.getInfoFromAddressComponent(suggest.gmaps.address_components, 'route')
    let line3 = "" || Helper.getInfoFromAddressComponent(suggest.gmaps.address_components, 'administrative_area_level_2')
    let city = "" || Helper.getInfoFromAddressComponent(suggest.gmaps.address_components, 'administrative_area_level_1')
    let postcode = "" || Helper.getInfoFromAddressComponent(suggest.gmaps.address_components, 'postal_code')
    let place = "" || suggest.label
    record.address.line1 = line1
    record.address.line2 = line2
    record.address.line3 = line3
    record.address.city = city
    record.address.postcode = postcode    
    record.place = place
    return (dispatch) => {
      dispatch({
        type: UPDATE_LOCATION,
        payload: {
          record: record,
          lat: suggest.location.lat,
          lng: suggest.location.lng
        }
      })
    }
  },
  hideAlert() {
    return (dispatch) => {
      dispatch({
        type: HIDE_ALERT
      })
    }
  },
  updateSearchText(query){
    return (dispatch) => {
      dispatch({
        type: UPDATE_SEARCH_TEXT,
        payload: {
          searchText: query
        }              
      })
    }
  }
}