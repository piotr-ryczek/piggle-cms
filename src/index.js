import React from 'react'
import ReactDOM from 'react-dom'
import { createBrowserHistory } from 'history'
import { Router, Route, Switch } from 'react-router-dom'
import { LastLocationProvider } from 'react-router-last-location';
import './assets/css/material-dashboard-react.css'
import { Provider } from 'react-redux'
import { store } from './store'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import registerServiceWorker from './registerServiceWorker'
import indexRoutes from './routes/index.jsx'

const hist = createBrowserHistory()

const theme = createMuiTheme()

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <Router basename={process.env.REACT_APP_ROUTER_BASENAME} history={hist}>
        <LastLocationProvider>
          <Switch>
            {indexRoutes.map((prop, key) => {
              return (
                <Route path={prop.path} component={prop.component} key={key} />
              )
            })}
          </Switch>        
        </LastLocationProvider>        
      </Router>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root')
)

registerServiceWorker()
