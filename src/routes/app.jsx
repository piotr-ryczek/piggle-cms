// import DashboardPage from '../components/Home'
// import User from '../components/User/User'
// import UserForm from '../components/User/UserForm'
import ChildType from '../components/ChildType/ChildType'
import ChildTypeForm from '../components/ChildType/ChildTypeForm'
import Facility from '../components/Facility/Facility'
import FacilityForm from '../components/Facility/FacilityForm'
import Brand from '../components/Brand/Brand'
import BrandForm from '../components/Brand/BrandForm'
import Store from '../components/Store/Store'
import StoreForm from '../components/Store/StoreForm'
import Category from '../components/Category/Category'
import CategoryForm from '../components/Category/CategoryForm'
import SubCategory from '../components/SubCategory/SubCategory'
import SubCategoryForm from '../components/SubCategory/SubCategoryForm'
import Sponsor from '../components/Sponsor/Sponsor'
import SponsorForm from '../components/Sponsor/SponsorForm'
import Offer from '../components/Offer/Offer'
import OfferForm from '../components/Offer/OfferForm'
import User from '../components/User/User'
import UserForm from '../components/User/UserForm'
import {
  LocalOffer,
  BurstMode,
  Book,
  CollectionsBookmark,
  BrandingWatermark,
  Business,
  MergeType,
  Face,
  Store as StoreIcon,
} from '@material-ui/icons'

const appRoutes = [
  {
    path: '/sponsors',
    sidebarName: 'Sponsors / ads',
    navbarName: 'Sponsors / ads',
    icon: BurstMode,
    component: Sponsor,
    routes: [
      {
        action: 'add',
        path: '/sponsors/add',
        component: SponsorForm
      },
      {
        action: 'edit',
        path: '/sponsors/edit/:id',
        component: SponsorForm
      },
      {
        action: 'delete',
        path: '/sponsors/delete/:id',
        component: Sponsor
      }
    ]
  },
  {
    path: '/offers',
    sidebarName: 'Offers',
    navbarName: 'Offers',
    icon: LocalOffer,
    component: Offer,
    routes: [
      {
        action: 'add',
        path: '/offers/add',
        component: OfferForm
      },
      {
        action: 'edit',
        path: '/offers/edit/:id',
        component: OfferForm
      },
      {
        action: 'delete',
        path: '/offers/delete/:id',
        component: Offer
      }
    ],
    breakLine: true
  },
  {
    path: '/categories',
    sidebarName: 'Categories',
    navbarName: 'Categories',
    icon: Book,
    component: Category,
    routes: [
      {
        action: 'add',
        path: '/categories/add',
        component: CategoryForm
      },
      {
        action: 'edit',
        path: '/categories/edit/:id',
        component: CategoryForm
      },
      {
        action: 'delete',
        path: '/categories/delete/:id',
        component: Category
      }
    ]
  },
  {
    path: '/SubCategories',
    sidebarName: 'Sub-categories',
    navbarName: 'Sub-categories',
    icon: CollectionsBookmark,
    component: SubCategory,
    routes: [
      {
        action: 'add',
        path: '/SubCategories/add',
        component: SubCategoryForm
      },
      {
        action: 'edit',
        path: '/SubCategories/edit/:id',
        component: SubCategoryForm
      },
      {
        action: 'delete',
        path: '/SubCategories/delete/:id',
        component: SubCategory
      }
    ],
    breakLine: true
  },  
  {
    path: '/brands',
    sidebarName: 'Brands',
    navbarName: 'Brands',
    icon: BrandingWatermark,
    component: Brand,
    routes: [
      {
        action: 'add',
        path: '/brands/add',
        component: BrandForm
      },
      {
        action: 'edit',
        path: '/brands/edit/:id',
        component: BrandForm
      },
      {
        action: 'delete',
        path: '/brands/delete/:id',
        component: Brand
      }
    ]
  },
  {
    path: '/stores',
    sidebarName: 'Stores',
    navbarName: 'Stores',
    icon: StoreIcon,
    component: Store,
    routes: [
      {
        action: 'add',
        path: '/stores/add',
        component: StoreForm
      },
      {
        action: 'edit',
        path: '/stores/edit/:id',
        component: StoreForm
      },
      {
        action: 'delete',
        path: '/stores/delete/:id',
        component: Store
      }
    ],
    breakLine: true
  },  
  {
    path: '/childType',
    sidebarName: 'Child type',
    navbarName: 'Child type',
    icon: MergeType,
    component: ChildType,
    routes: [
      {
        action: 'add',
        path: '/childType/add',
        component: ChildTypeForm
      },
      {
        action: 'edit',
        path: '/childType/edit/:id',
        component: ChildTypeForm
      },
      {
        action: 'delete',
        path: '/childType/delete/:id',
        component: ChildType
      }
    ]
  },
  {
    path: '/facilities',
    sidebarName: 'Facilities',
    navbarName: 'Facilities',
    icon: Business,
    component: Facility,
    routes: [
      {
        action: 'add',
        path: '/facilities/add',
        component: FacilityForm
      },
      {
        action: 'edit',
        path: '/facilities/edit/:id',
        component: FacilityForm
      },
      {
        action: 'delete',
        path: '/facilities/delete/:id',
        component: Facility
      }
    ]
  },  
  {
    path: '/user',
    sidebarName: 'User',
    navbarName: 'User',
    icon: Face,
    component: User,
    routes: [
      {
        action: 'add',
        path: '/user/add',
        component: UserForm
      },
      {
        action: 'edit',
        path: '/user/edit/:id',
        component: UserForm
      },
      {
        action: 'delete',
        path: '/user/delete/:id',
        component: User
      },
      // {
      //   action: 'changepassword',
      //   path: '/user/changepassword/:id',
      //   component: ChangePasswordForm
      // }
    ],
    breakLine: true
  },  
]
export default appRoutes
