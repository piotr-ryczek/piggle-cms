// ##############################
// // // Dashboard styles
// #############################

import { successColor } from "../styles";

const dashboardStyle = {
  successText: {
    color: successColor
  },
  upArrowCardFacility: {
    width: 14,
    height: 14
  },
  appLogo: {
    maxWidth: '200px'
  }
};

export default dashboardStyle;
