// ##############################
// // // App styles
// #############################

import { drawerWidth, transition, container } from '../styles.jsx'

const appStyle = theme => ({
  wrapper: {
    position: 'relative',
    top: '0'
  },
  mainPanel: {
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`
    },
    overflow: 'auto',
    position: 'relative',
    float: 'right',
    ...transition,
    maxHeight: '100%',
    width: '100%',
    overflowScrolling: 'touch'
  },
  content: {
    marginTop: '70px',
    padding: '30px 15px',
    minHeight: 'calc(100% - 123px)'
  },
  container,
  map: {
    marginTop: '70px'
  },
  progress: {
    position: 'fixed',
    zIndex: 999,
    overflow: 'show',
    margin: 'auto',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,   
  }
})

export default appStyle
