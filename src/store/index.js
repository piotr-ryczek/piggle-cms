import { applyMiddleware, createStore, compose } from 'redux'
import reducers from '../reducers'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import persistState from 'redux-localstorage'
import adapter from 'redux-localstorage/lib/adapters/localStorage'

let applyMiddlewares = applyMiddleware(
  thunk, promise()
)

const storage = adapter(window.localStorage)

const createStoreWithMiddleware = compose(
  applyMiddlewares,
  persistState(storage, 'state')
)(createStore)

export const store = createStoreWithMiddleware(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__({trace: true})
)
